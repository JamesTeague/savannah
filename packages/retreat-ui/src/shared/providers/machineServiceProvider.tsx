import React, { createContext } from 'react';
import { Interpreter, State } from 'xstate';
import { RetreatContext, RetreatEvent } from '../machines';

type MachineServiceContext = {
  state: State<RetreatContext, RetreatEvent>;
  send: Interpreter<RetreatContext, any, RetreatEvent>['send'];
  service: Interpreter<RetreatContext, any, RetreatEvent>;
};

export const machineServiceContext = createContext<MachineServiceContext | undefined>(undefined);
