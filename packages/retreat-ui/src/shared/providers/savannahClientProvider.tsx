import React, { createContext } from 'react';
import { SavannahClient } from '../services/api/apolloClient';

export const savannahClientContext = createContext<SavannahClient | undefined>(undefined);

export const createSavannahClientProvider = (client: SavannahClient) => ({
  children,
}: {
  children: React.ReactNode;
}) => <savannahClientContext.Provider value={client}>{children}</savannahClientContext.Provider>;
