import React, { createContext } from 'react';
import { LocationService } from '../services';

export const apiClientContext = createContext<LocationService | undefined>(undefined);

export const createApiClientProvider = (apiClient: LocationService) => ({
  children,
}: {
  children: React.ReactNode;
}) => <apiClientContext.Provider value={apiClient}>{children}</apiClientContext.Provider>;
