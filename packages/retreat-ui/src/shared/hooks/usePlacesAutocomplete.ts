import { useEffect, useMemo, useState } from 'react';
import { from, NEVER as RxNever, Subject } from 'rxjs';
import { catchError, filter, switchMap, throttleTime } from 'rxjs/operators';
import useApiClient from './useApiClient';
import { PlaceResult } from '../services';

const usePlacesAutocomplete = () => {
  const {
    location: { places },
  } = useApiClient();
  const [searchText, setSearchText] = useState<string>('');
  const [results, setResults] = useState<{
    suggestions: PlaceResult[];
    error: Error | undefined;
  }>({ suggestions: [], error: undefined });

  const [autoCompleteSubject, resultObservable] = useMemo(() => {
    const subject: Subject<string> = new Subject();

    const result$ = subject.pipe(
      filter(text => text !== ''),
      throttleTime(300, undefined, { leading: true, trailing: true }),
      switchMap(input =>
        from(
          places.placesAutoComplete({
            textInput: input,
            language: 'en',
            restrictions: ['us'],
            types: ['place'],
          })
        ).pipe(
          catchError((error: Error) => {
            setResults({ suggestions: [], error });
            return RxNever;
          })
        )
      )
    );

    const subscription = result$.subscribe(results => {
      setResults({ suggestions: results, error: undefined });
    });

    return [subject, subscription];
  }, [places]);

  useEffect(() => {
    return () => resultObservable.unsubscribe();
  }, [resultObservable]);

  useEffect(() => {
    autoCompleteSubject.next(searchText);
  }, [autoCompleteSubject, searchText]);

  return { results, setSearchText };
};

export default usePlacesAutocomplete;
