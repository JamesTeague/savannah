import { useContext } from 'react';
import { apiClientContext } from '../providers';

const useApiClient = () => {
  const apiClient = useContext(apiClientContext);

  if (!apiClient) {
    throw new Error('The client was not found in the context. Was it set?');
  }

  return apiClient;
};

export default useApiClient;
