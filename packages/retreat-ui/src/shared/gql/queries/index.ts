import gql from 'graphql-tag';

const PROPOSALS_BY_RETREAT = gql`
  query getProposals($retreatId: ID!) {
    getProposalsByRetreatId(retreatId: $retreatId) {
      id
      city {
        id
        name
        address
        state {
          name
          abbreviation
        }
        country {
          name
          abbreviation
        }
        center
      }
      memberId
    }
  }
`;

const RETREAT_BY_ID = gql`
  query getRetreat($retreatId: ID!) {
    getRetreatByRetreatId(retreatId: $retreatId) {
      id
      name
      phase
    }
  }
`;

const MEMBER_BY_ID = gql`
  query getMember($memberId: ID!) {
    getMemberByMemberId(memberId: $memberId) {
      id
      displayName
      email
      points
      proposals {
        id
        city {
          id
          name
          address
          state {
            name
            abbreviation
          }
          country {
            name
            abbreviation
          }
          center
        }
      }
    }
  }
`;

export { MEMBER_BY_ID, PROPOSALS_BY_RETREAT, RETREAT_BY_ID };
