import gql from 'graphql-tag';

const MAKE_PROPOSAL = gql`
  mutation($data: JSON!) {
    executeCommand(input: { action: "make-proposal", data: $data }) {
      action
      data
    }
  }
`;

export { MAKE_PROPOSAL };
