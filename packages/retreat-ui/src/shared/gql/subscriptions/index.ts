import gql from 'graphql-tag';

const PROPOSAL_SUBSCRIPTION = gql`
  subscription onProposalAdded($retreatId: ID!) {
    proposalAdded(retreatId: $retreatId) {
      id
      city {
        id
        name
        address
        state {
          name
          abbreviation
        }
        country {
          name
          abbreviation
        }
        center
      }
      memberId
    }
  }
`;

export { PROPOSAL_SUBSCRIPTION };
