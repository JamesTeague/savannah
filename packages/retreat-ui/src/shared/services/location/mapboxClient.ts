import MapiClient from '@mapbox/mapbox-sdk';
import Bluebird from 'bluebird';
import uuid from 'uuid/v4';
import createMapboxGeocodingService from '@mapbox/mapbox-sdk/services/geocoding';
import { PlaceRequest, PlaceResult, PlacesService } from './types';
import {
  GeocodeFeature,
  GeocodeProperties,
  GeocodeQueryType,
} from '@mapbox/mapbox-sdk/services/geocoding';

const toPlaceResult = (place: GeocodeFeature): PlaceResult => {
  const state = place.context[0] as GeocodeProperties;
  const country = place.context[1] as GeocodeProperties;

  return {
    name: place.text,
    state: {
      name: state.text,
      abbreviation: state.short_code.replace(/(US-)/, ''),
    },
    country: {
      name: country.text,
      abbreviation: country.short_code.toUpperCase(),
    },
    address: place.place_name,
    url: `https://www.wikidata.org/wiki/${place.properties.wikidata}`,
    id: uuid(),
    center: place.center,
  };
};

const createMapboxClient = (key: string): PlacesService => {
  const baseClient = new MapiClient({ accessToken: key });
  const client = createMapboxGeocodingService(baseClient);

  return {
    placesAutoComplete: (options: PlaceRequest) => {
      return Bluebird.resolve(
        client
          .forwardGeocode({
            query: options.textInput,
            language: [options.language],
            mode: 'mapbox.places',
            countries: options.restrictions,
            autocomplete: true,
            types: options.types as GeocodeQueryType[],
          })
          .send()
          .then(({ body }) =>
            body.features.map((feature: GeocodeFeature) => toPlaceResult(feature))
          )
      );
    },
    getPlace: (options: PlaceRequest) => {
      return Bluebird.resolve(
        client
          .forwardGeocode({
            query: options.textInput,
            language: [options.language],
            mode: 'mapbox.places',
            countries: options.restrictions,
            autocomplete: false,
            types: options.types as GeocodeQueryType[],
          })
          .send()
          .then(({ body }) => toPlaceResult(body.features[0]))
      );
    },
  };
};

export { createMapboxClient };
