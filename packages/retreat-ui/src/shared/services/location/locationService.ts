import { PlacesService } from './types';
import { createMapboxClient } from './mapboxClient';

export interface LocationClient {
  readonly places: PlacesService;
}

export interface LocationService {
  readonly location: LocationClient;
}

export interface LocationClientOptions {
  placesKey: string;
}

const createLocationService = (options: LocationClientOptions): LocationService => {
  return {
    location: {
      places: createMapboxClient(options.placesKey),
    },
  };
};

export { createLocationService };

export default createLocationService;
