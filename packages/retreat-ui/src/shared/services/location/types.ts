import Bluebird from 'bluebird';
import { GeocodeQueryType } from '@mapbox/mapbox-sdk/services/geocoding';

type NameAndAbbreviation = {
  name: string;
  abbreviation: string;
};

export type LocationType = GeocodeQueryType;

export type PlaceRequest = {
  textInput: string;
  sessionToken?: string;
  language: string;
  restrictions?: string[];
  types: LocationType[];
};

export type PlaceResult = {
  name: string;
  state: NameAndAbbreviation;
  country: NameAndAbbreviation;
  address: string;
  url: string;
  id: string;
  center: number[];
};

export type PlacesService = {
  placesAutoComplete: (options: PlaceRequest) => Bluebird<PlaceResult[]>;
  getPlace: (options: PlaceRequest) => Bluebird<PlaceResult>;
};
