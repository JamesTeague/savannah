import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory';
import { ApolloClient, SubscriptionOptions } from 'apollo-client';
import { from, DocumentNode, split } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';
import Constants from 'expo-constants';
import { OperationDefinitionNode } from 'graphql';
import gql from 'graphql-tag';
import { SubscriptionClient } from 'subscriptions-transport-ws';

export type CommandInput = {
  action: string;
  key?: string;
  data: object;
};

export const SendCommand = gql`
  mutation executeCommand($commandInput: CommandInput!) {
    executeCommand(input: $commandInput) {
      id
      timestamp
      action
      data
    }
  }
`;

const { manifest } = Constants;

const isSubscriptionOperation = ({ query }: { query: DocumentNode }) => {
  const { kind, operation } = getMainDefinition(query) as OperationDefinitionNode;
  return kind === 'OperationDefinition' && operation === 'subscription';
};

const isMutationOperation = ({ query }: { query: DocumentNode }) => {
  const { kind, operation } = getMainDefinition(query) as OperationDefinitionNode;
  return kind === 'OperationDefinition' && operation === 'mutation';
};

const createHttpLink = (options: HttpLink.Options) => new HttpLink(options);
const createWsLink = (paramsOrClient: WebSocketLink.Configuration | SubscriptionClient) =>
  new WebSocketLink(paramsOrClient);

const readLink = createHttpLink({
  uri: `http://${manifest.debuggerHost.split(':').shift()}:4002`,
});

const writeLink = createHttpLink({
  uri: `http://${manifest.debuggerHost.split(':').shift()}:4000/graphql`,
});

const wsLink = createWsLink({
  uri: `ws://${manifest.debuggerHost.split(':').shift()}:4002/graphql`,
  options: {
    reconnect: true,
  },
});

const createApolloClient = (): ApolloClient<NormalizedCacheObject> =>
  new ApolloClient({
    link: from([
      split(isMutationOperation, writeLink, split(isSubscriptionOperation, wsLink, readLink)),
    ]),
    cache: new InMemoryCache(),
  });

export type SavannahClient = Pick<ApolloClient<any>, 'mutate' | 'query' | 'subscribe'> & {
  executeCommand: (command: CommandInput) => Promise<any>;
};

const createSavannahClient = (
  client: ApolloClient<NormalizedCacheObject> = createApolloClient()
): SavannahClient => ({
  query: client.query,
  mutate: client.mutate,
  subscribe: (options: SubscriptionOptions) => client.subscribe(options),
  executeCommand: (command: CommandInput) =>
    client.mutate({
      mutation: SendCommand,
      variables: { commandInput: command },
    }),
});

export { createApolloClient, createSavannahClient };
