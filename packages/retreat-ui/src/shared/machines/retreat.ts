import { IOTypes } from '@penguinhouse/savannah-io';
import { SavannahClient } from '../services/api/apolloClient';
import { assign, Machine } from 'xstate';
import { MEMBER_BY_ID, PROPOSALS_BY_RETREAT, RETREAT_BY_ID } from '../gql/queries';
import Bluebird from 'bluebird';
import { PROPOSAL_SUBSCRIPTION } from '../gql/subscriptions';

export interface RetreatStateSchema {
  states: {
    unknown: {};
    proposing: {};
    voting: {
      initial: string;
      states: {
        round1: {};
        round2: {};
      };
    };
    retreating: {};
  };
}

export type RetreatEvent =
  | { type: 'NEXT' }
  | { type: 'COMPLETE' }
  | { type: 'RESET' }
  | { type: 'ADD_PROPOSAL' }
  | { type: '_ADD_PROPOSAL'; proposal: IOTypes.Proposal };

export interface RetreatContext {
  proposals: IOTypes.Proposal[];
  votes: IOTypes.Vote[];
  retreat: IOTypes.Retreat | undefined;
  member: IOTypes.Member | undefined;
  client: SavannahClient | undefined;
}

const fetchInfoForUser = ({ client }) => {
  return Bluebird.all([
    client.query({
      query: MEMBER_BY_ID,
      variables: { memberId: '773c6fd3-f5f5-485c-94b1-2eb24de0eb41' },
    }),
    client.query({
      query: PROPOSALS_BY_RETREAT,
      variables: { retreatId: 'b6ab508e-40ef-49d8-9610-3e68e9df5d73' },
    }),
    client.query({
      query: RETREAT_BY_ID,
      variables: { retreatId: 'b6ab508e-40ef-49d8-9610-3e68e9df5d73' },
    }),
  ]).then(([member, proposals, retreat]) => ({
    member: member.data.getMemberByMemberId,
    proposals: proposals.data.getProposalsByRetreatId,
    retreat: retreat.data.getRetreatByRetreatId,
  }));
};

const subscribeToMoreProposals = ({ client }) =>
  client
    .subscribe({
      query: PROPOSAL_SUBSCRIPTION,
      variables: { retreatId: 'b6ab508e-40ef-49d8-9610-3e68e9df5d73' },
    })
    .map(value => ({ type: '_ADD_PROPOSAL', proposal: value.data.proposalAdded }));

const addProposalToMember = (member: IOTypes.Member, proposal: IOTypes.Proposal) => {
  if (proposal.memberId === member.id) {
    member.proposals.push(proposal);
  }

  return member;
};

const deadlineReached = () => false;
const allVotesCast = ({ votes }: { votes: IOTypes.Vote[] }) =>
  votes.map(vote => vote.points).reduce((acc, current) => acc + current, 0) === 60;
const proposalLimitReached = ({ proposals }) => proposals.length === 20;
const shouldBeProposing = ({ retreat, proposals }) =>
  retreat?.phase === 'PROPOSAL' && proposals.length < 20;

const addProposal = (context, event) => {
  return context.client.executeCommand({
    action: event.action,
    data: event.data,
  });
};

const retreatMachine = Machine<RetreatContext, RetreatStateSchema, RetreatEvent>(
  {
    id: 'retreat',
    initial: 'unknown',
    context: {
      proposals: [],
      votes: [],
      retreat: undefined,
      member: undefined,
      client: undefined,
    },
    states: {
      unknown: {
        invoke: {
          id: 'fetchInfo',
          src: 'fetchInfoForUser',
          onDone: {
            actions: assign({
              member: (_, event) => event.data.member,
              proposals: (_, event) => event.data.proposals,
              retreat: (_, event) => event.data.retreat,
            }),
          },
        },
        on: {
          '': {
            target: 'proposing',
            cond: 'shouldBeProposing',
          },
        },
      },
      proposing: {
        invoke: {
          id: 'subscribeToMore',
          src: 'subscribeToMoreProposals',
        },
        on: {
          '': [
            {
              target: 'voting',
              cond: 'deadlineReached',
            },
            {
              target: 'voting',
              cond: 'proposalLimitReached',
            },
          ],
          NEXT: 'voting',
          ADD_PROPOSAL: { actions: 'addProposal' },
          _ADD_PROPOSAL: {
            actions: [
              assign({
                proposals: ({ proposals }, { proposal }) => [...proposals, proposal],
              }),
              assign({
                member: ({ member }, { proposal }) => addProposalToMember(member, proposal),
              }),
            ],
          },
        },
      },
      voting: {
        initial: 'round1',
        states: {
          round1: {
            on: {
              '': {
                target: 'round2',
                cond: 'allVotesCast',
              },
              NEXT: 'round2',
            },
          },
          round2: {
            on: {
              '': {
                target: '#retreat.retreating',
                cond: 'allVotesCast',
              },
            },
          },
        },
        on: {
          COMPLETE: 'retreating',
        },
      },
      retreating: {
        on: {
          RESET: 'proposing',
        },
      },
    },
  },
  {
    guards: {
      allVotesCast,
      deadlineReached,
      proposalLimitReached,
      shouldBeProposing,
    },
    actions: {
      addProposal,
    },
    services: {
      fetchInfoForUser,
      subscribeToMoreProposals,
    },
  }
);

export { retreatMachine };
