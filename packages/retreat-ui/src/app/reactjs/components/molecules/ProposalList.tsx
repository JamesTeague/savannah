import React from 'react';
import { List } from '@material-ui/core';
import Proposal from './Proposal';

type ProposalProps = {
  id: string;
  city: {
    id: string;
    name: string;
    address: string;
    state: {
      name: string;
      abbreviation: string;
    };
    country: {
      name: string;
      abbreviation: string;
    };
    center: number[];
  };
  vote?: {
    id: string;
    points: number;
  };
};

type ProposalListProps = {
  proposals: ProposalProps[];
};

const sortProposalAlphabetically = (a: ProposalProps, b: ProposalProps) => {
  if (a.city.name < b.city.name) {
    return -1;
  }
  if (a.city.name > b.city.name) {
    return 1;
  }
  return 0;
};

export default function ProposalList(props: ProposalListProps) {
  return (
    <List>
      {props.proposals.sort(sortProposalAlphabetically).map(proposal => (
        <Proposal key={proposal.id} proposalProps={{ ...proposal }} />
      ))}
    </List>
  );
}
