import React from 'react';
import { ListItem, ListItemText, ListItemIcon } from '@material-ui/core';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { green } from '@material-ui/core/colors';
import VoteBadge from '../atoms/VoteBadge';

type ProposalProps = {
  proposalProps: {
    id: string;
    city: {
      id: string;
      name: string;
      address: string;
      state: {
        name: string;
        abbreviation: string;
      };
      country: {
        name: string;
        abbreviation: string;
      };
    };
    vote?: {
      id: string;
      points: number;
    };
  };
};

export default function Proposal(props: ProposalProps) {
  const {
    proposalProps: { city, vote },
  } = props;

  return (
    <ListItem button>
      <ListItemText
        primary={city.name}
        secondary={`${city.state.name}, ${city.country.abbreviation}`}
      />
      {vote ? (
        <VoteBadge points={vote?.points} style={{ marginRight: '44px' }} />
      ) : (
        <ListItemIcon>
          <AddCircleIcon style={{ color: green[500] }} />
        </ListItemIcon>
      )}
    </ListItem>
  );
}
