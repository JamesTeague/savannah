import React, { useState } from 'react';
import {
  Button,
  FormControl,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
} from '@material-ui/core';
import FloatingAddButton from '../atoms/FloatingAddButton';
import PlaceAutoCompleteInput from '../atoms/PlaceAutoCompleteInput';
import { PlaceResult } from '../../../../shared/services';
import ButtonWithLoader from '../atoms/ButtonWithLoader';

export default function ProposalDialog(props: any) {
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [location, setLocation] = useState({});
  const [buttonDisabled, setButtonDisabled] = useState(true);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleSelection = (selection: PlaceResult) => {
    setLocation(selection);
    setButtonDisabled(false);
  };

  const isDisabled = buttonDisabled || loading;

  const handleSubmit = () => {
    setLoading(true);
    props.completeTransaction(location).then(() => {
      setLoading(false);
      setOpen(false);
    });
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <div style={{ padding: '0px 10px 5px 0px' }}>
        <FloatingAddButton id={'open-proposal-dialog'} onClick={handleClickOpen} />
      </div>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Make Proposal</DialogTitle>
        <DialogContent>
          <FormControl>
            <PlaceAutoCompleteInput onSelection={handleSelection} />
          </FormControl>
        </DialogContent>
        <DialogActions>
          <Button variant={'contained'} onClick={handleClose} color={'secondary'}>
            Cancel
          </Button>
          <ButtonWithLoader
            isDisabled={isDisabled}
            onClick={handleSubmit}
            color={'primary'}
            loading={loading}
            text={'Proposal'}
          />
        </DialogActions>
      </Dialog>
    </>
  );
}
