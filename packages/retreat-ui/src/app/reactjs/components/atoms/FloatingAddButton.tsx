import React from 'react';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

type FloatingAddButtonProps = {
  onClick: any;
  id: string;
};

export default function FloatingAddButton(props: FloatingAddButtonProps) {
  return (
    <Fab color="primary" onClick={props.onClick} id={props.id}>
      <AddIcon />
    </Fab>
  );
}
