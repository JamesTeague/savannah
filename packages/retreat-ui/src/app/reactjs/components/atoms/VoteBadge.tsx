import React from 'react';
import { Badge } from '@material-ui/core';

export default function VoteBadge(props: { points: number; style?: any }) {
  return (
    <Badge
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'left',
      }}
      badgeContent={props.points}
      color="primary"
      style={props.style}
    >
      <div />
    </Badge>
  );
}
