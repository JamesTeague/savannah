import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import usePlacesAutocomplete from '../../../../shared/hooks/usePlacesAutocomplete';

const useStyles = makeStyles(theme => ({
  icon: {
    color: theme.palette.text.secondary,
    marginRight: theme.spacing(2),
  },
}));

export default function PlaceAutoCompleteInput(props: any) {
  const classes = useStyles();
  const [inputValue, setInputValue] = useState('');
  const { results, setSearchText } = usePlacesAutocomplete();

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value);
    setSearchText(event.target.value);
  };

  return (
    <Autocomplete
      id="google-places-autocomplete"
      style={{ width: '70vw', maxWidth: 300 }}
      getOptionLabel={option => option.address}
      options={results.suggestions}
      autoComplete
      includeInputInList
      freeSolo
      disableOpenOnFocus
      renderInput={params => (
        <TextField
          {...params}
          label="Select a city"
          variant="outlined"
          fullWidth
          onChange={handleChange}
        />
      )}
      renderOption={option => {
        const matches = match(option.address, inputValue);
        const parts = parse(option.name, matches);

        return (
          <div onClick={() => props.onSelection(option)}>
            <Grid container alignItems="center">
              <Grid item>
                <LocationOnIcon className={classes.icon} />
              </Grid>
              <Grid item xs>
                {parts.map((part: any, index: any) => (
                  <span key={index} style={{ fontWeight: part.highlight ? 700 : 400 }}>
                    {part.text}
                  </span>
                ))}
                <Typography variant="body2" color="textSecondary">
                  {`${option.state.abbreviation}, ${option.country.abbreviation}`}
                </Typography>
              </Grid>
            </Grid>
          </div>
        );
      }}
    />
  );
}
