import React from 'react';
import { Button, createStyles, Theme, CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    wrapper: {
      margin: theme.spacing(1),
      position: 'relative',
    },
    buttonProgress: {
      color: green[500],
      position: 'absolute',
      top: '50%',
      left: '50%',
      marginTop: -12,
      marginLeft: -12,
    },
  })
);

export default function ButtonWithLoader(props: any) {
  const classes = useStyles();
  return (
    <div className={classes.wrapper}>
      <Button
        variant={'contained'}
        disabled={props.isDisabled}
        onClick={props.onClick}
        color={props.color}
      >
        {props.text}
      </Button>
      {props.loading && <CircularProgress size={24} className={classes.buttonProgress} />}
    </div>
  );
}
