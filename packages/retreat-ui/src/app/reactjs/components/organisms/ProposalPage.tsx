import React, { useContext, useEffect } from 'react';
import { Container, Grid } from '@material-ui/core';
import ProposalList from '../molecules/ProposalList';
import ProposalDialog from '../molecules/ProposalDialog';
import { useMutation, useQuery } from '@apollo/react-hooks';
import { dispatchClientContext } from '../../../../shared/providers';
import { PROPOSALS_BY_RETREAT } from '../../../../shared/gql/queries';
import { MAKE_PROPOSAL } from '../../../../shared/gql/mutations';
import { PROPOSAL_SUBSCRIPTION } from '../../../../shared/gql/subscriptions';

export default function ProposalPage(props: any) {
  const { subscribeToMore, loading, data } = useQuery(PROPOSALS_BY_RETREAT, {
    variables: { retreatId: props.retreatId },
  });

  const dispatchClient = useContext(dispatchClientContext);
  const [addProposal] = useMutation(MAKE_PROPOSAL, { client: dispatchClient });

  useEffect(() => {
    // tslint:disable-next-line:variable-name
    const _subscribeToMore = () =>
      subscribeToMore({
        document: PROPOSAL_SUBSCRIPTION,
        variables: { retreatId: props.retreatId },
        updateQuery: (previous, { subscriptionData }) => {
          if (!subscriptionData.data) {
            return previous;
          }

          const newProposal = subscriptionData.data.proposalAdded;

          return Object.assign({}, previous, {
            getProposalsByRetreatId: [...previous.getProposalsByRetreatId, newProposal],
          });
        },
      });

    _subscribeToMore();
  }, [props.retreatId, subscribeToMore]);

  const handleSubmission = (city: any) => {
    return addProposal({
      variables: {
        data: {
          retreatId: props.retreatId,
          memberId: props.memberId,
          city,
        },
      },
    });
  };

  if (loading) {
    return <h4>Loading...</h4>;
  }

  return (
    <>
      <Container>
        <ProposalList proposals={data.getProposalsByRetreatId} />
      </Container>
      <Grid
        container
        direction={'row-reverse'}
        id={'proposal-dialog'}
        style={{ width: '100%', position: 'fixed', bottom: '0px' }}
      >
        <Grid item>
          <ProposalDialog completeTransaction={handleSubmission} />
        </Grid>
      </Grid>
    </>
  );
}
