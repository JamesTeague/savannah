import React from 'react';
import { Container, Content } from 'native-base';
import { PhoneHeader } from '../components';

type PhoneLayoutProps = {
  title: string;
  subtitle?: string;
};

const PhoneLayout: React.FC<PhoneLayoutProps> = ({ title, subtitle, children }) => {
  return (
    <Container>
      <PhoneHeader title={title} subtitle={subtitle} />
      <Content>{children}</Content>
    </Container>
  );
};

export { PhoneLayout };
