import { MaterialCommunityIcons } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React from 'react';
import { Text, View } from 'react-native';
import { createApiClientProvider, machineServiceContext } from '../../shared/providers';
import { createLocationService } from '../../shared/services';
import { ProposalScreen, RetreatScreen } from './screens';
import { createSavannahClient } from '../../shared/services/api/apolloClient';
import { useMachine } from '@xstate/react/lib';
import {
  RetreatContext,
  RetreatEvent,
  retreatMachine,
  RetreatStateSchema,
} from '../../shared/machines';
import { Machine } from 'xstate';

function VoteScreen() {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Vote Screen</Text>
    </View>
  );
}

function AccountScreen() {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Account Screen</Text>
    </View>
  );
}

const ApiClientProvider = createApiClientProvider(
  createLocationService({
    // key: process.env.REACT_APP_LOCATION_KEY as string,
    placesKey:
      'pk.eyJ1IjoianRlYWd1ZWlpIiwiYSI6ImNrNmZ2MWowbjAxa2gza3BtYXdvb2ZmaHoifQ.CCSeQd6zTb4OXVKiag1n6g',
  })
);

const client = createSavannahClient();

const Tab = createBottomTabNavigator();

function NativeApp() {
  const [state, send, service] = useMachine<RetreatContext, RetreatEvent>(retreatMachine, {
    context: { client },
  });

  return (
    <ApiClientProvider>
      <machineServiceContext.Provider value={{ state, send, service }}>
        <NavigationContainer>
          <Tab.Navigator
            screenOptions={({ route }) => ({
              tabBarIcon: ({ focused, color, size }) => {
                let iconName;

                if (route.name === 'Retreat') {
                  iconName = 'city-variant';
                } else if (route.name === 'Votes') {
                  iconName = 'vote';
                } else if (route.name === 'Account') {
                  iconName = 'account';
                } else if (route.name === 'Proposals') {
                  iconName = 'magnify';
                }

                // You can return any component that you like here!
                return <MaterialCommunityIcons name={iconName} size={size} color={color} />;
              },
            })}
          >
            <Tab.Screen name="Retreat" component={RetreatScreen} />
            <Tab.Screen name="Proposals" component={ProposalScreen} />
            <Tab.Screen name="Votes" component={VoteScreen} />
            <Tab.Screen name="Account" component={AccountScreen} />
          </Tab.Navigator>
        </NavigationContainer>
      </machineServiceContext.Provider>
    </ApiClientProvider>
  );
}

export default NativeApp;
