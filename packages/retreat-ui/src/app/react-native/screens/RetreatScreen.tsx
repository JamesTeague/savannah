import { List } from 'native-base';
import React, { useContext } from 'react';
import { PhoneLayout } from '../layouts';
import { LoadingListSkeleton, Proposal } from '../components';
import { machineServiceContext } from '../../../shared/providers';

const RetreatScreen = () => {
  const { state } = useContext(machineServiceContext);

  return (
    <PhoneLayout
      title={'Retreat'}
      children={
        <LoadingListSkeleton
          isLoading={false}
          children={
            <List
              dataArray={state.context.proposals}
              renderItem={proposal => <Proposal proposal={proposal.item} />}
            />
          }
        />
      }
    />
  );
};

export { RetreatScreen };
