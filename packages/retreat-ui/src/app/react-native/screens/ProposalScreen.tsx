import { IOTypes } from '@penguinhouse/savannah-io';
import { List } from 'native-base';
import React, { useContext, useState } from 'react';
import { PhoneLayout } from '../layouts';
import { LoadingListSkeleton, PlacesAutocomplete, Proposal } from '../components';
import { machineServiceContext } from '../../../shared/providers';

const ProposalScreen = () => {
  const [showResults, setShowResults] = useState(false);
  const {
    state: { context },
    send,
  } = useContext(machineServiceContext);

  const onSelection = (city: any) => {
    send('ADD_PROPOSAL', {
      action: IOTypes.CommandActionEnum.MakeProposal,
      data: {
        retreatId: context.retreat.id,
        memberId: context.member.id,
        city,
      },
    });
  };

  const shouldShowProposals = !showResults;
  return (
    <PhoneLayout title={'Proposals'}>
      <PlacesAutocomplete
        showResults={showResults}
        setShowResults={setShowResults}
        onSelection={onSelection}
      />
      <LoadingListSkeleton
        isLoading={false}
        children={
          <List
            dataArray={shouldShowProposals ? context.member.proposals : []}
            renderItem={proposal => <Proposal proposal={proposal.item} />}
          />
        }
      />
    </PhoneLayout>
  );
};

export { ProposalScreen };
