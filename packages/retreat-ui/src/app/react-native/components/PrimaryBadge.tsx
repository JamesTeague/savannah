import React from 'react';
import { Badge, Text } from 'native-base';

type PrimaryBadgeProps = {
  number: number;
};

const PrimaryBadge = (props: PrimaryBadgeProps) => (
  <Badge primary>
    <Text>{props.number}</Text>
  </Badge>
);

export { PrimaryBadge };
