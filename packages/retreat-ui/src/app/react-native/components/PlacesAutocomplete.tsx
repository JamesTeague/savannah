import { MaterialIcons } from '@expo/vector-icons';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import { ListItem } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import Autocomplete from 'react-native-autocomplete-input';
import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import usePlacesAutocomplete from '../../../shared/hooks/usePlacesAutocomplete';

type PlacesAutocompleteProps = {
  onSelection: (option: any) => void;
  setShowResults: (bool: boolean) => void;
  showResults: boolean;
};

const TextGrid = ({ parts, state, country }) => (
  <Grid>
    <Col style={{ width: 30 }}>
      <MaterialIcons name={'place'} size={20} />
    </Col>
    <Col>
      <Row>
        {parts.map((part: any, index: any) => (
          <Text key={index} style={{ fontWeight: part.highlight ? '700' : '400' }}>
            {part.text}
          </Text>
        ))}
      </Row>
      <Row>
        <Text>{`${state.abbreviation}, ${country.abbreviation}`}</Text>
      </Row>
    </Col>
  </Grid>
);

const PlacesAutocomplete = (props: PlacesAutocompleteProps) => {
  const [inputValue, setInputValue] = useState('');
  const { results, setSearchText } = usePlacesAutocomplete();

  const inputIsEmpty = inputValue === '';

  if (inputIsEmpty) {
    props.setShowResults(false);
  }
  const onPress = (city: any) => {
    setInputValue('');
    props.onSelection(city);
    props.setShowResults(false);
  };

  return (
    <View style={styles.container}>
      <Autocomplete
        autoCapitalize="none"
        autoCorrect={false}
        containerStyle={styles.autocompleteContainer}
        data={inputIsEmpty ? [] : results.suggestions}
        defaultValue={inputValue}
        onChangeText={text => {
          props.setShowResults(true);
          setInputValue(text);
          setSearchText(text);
        }}
        placeholder="Enter City"
        hideResults={!props.showResults}
        renderItem={({ item }) => {
          const { address, name, state, country } = item;
          const matches = match(address, inputValue);
          const parts = parse(name, matches);

          return (
            <ListItem onPress={() => onPress(item)}>
              <TextGrid parts={parts} state={state} country={country} />
            </ListItem>
          );
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
    flex: 1,
    paddingTop: 25,
  },
  autocompleteContainer: {
    marginLeft: 10,
    marginRight: 10,
  },
  itemText: {
    fontSize: 15,
    margin: 2,
  },
  descriptionContainer: {
    // `backgroundColor` needs to be set otherwise the
    // autocomplete input will disappear on text input.
    backgroundColor: '#F5FCFF',
    marginTop: 8,
  },
  infoText: {
    textAlign: 'center',
  },
  titleText: {
    fontSize: 18,
    fontWeight: '500',
    marginBottom: 10,
    marginTop: 10,
    textAlign: 'center',
  },
  directorText: {
    color: 'grey',
    fontSize: 12,
    marginBottom: 10,
    textAlign: 'center',
  },
  openingText: {
    textAlign: 'center',
  },
});

export { PlacesAutocomplete };
