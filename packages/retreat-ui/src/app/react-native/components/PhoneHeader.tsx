import React from 'react';
import { Body, Header, Left, Right, Subtitle, Title } from 'native-base';

type PhoneHeaderProps = {
  title: string;
  subtitle?: string;
};

const PhoneHeader = ({ title, subtitle }: PhoneHeaderProps) => (
  <Header>
    <Left />
    <Body>
      <Title>{title}</Title>
      {subtitle && <Subtitle>{subtitle}</Subtitle>}
    </Body>
    <Right />
  </Header>
);

export { PhoneHeader };
