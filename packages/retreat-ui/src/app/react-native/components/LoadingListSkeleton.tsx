import React from 'react';
import SkeletonContent from 'react-native-skeleton-content';

type LoadingListSkeletonProps = {
  isLoading: boolean;
};

const LoadingListSkeleton: React.FC<LoadingListSkeletonProps> = ({ isLoading, children }) => (
  <SkeletonContent
    containerStyle={{ flex: 1, width: 400 }}
    isLoading={isLoading}
    layout={[
      { width: 300, height: 40, marginBottom: 10, marginLeft: 30, marginTop: 10 },
      { width: 300, height: 40, marginBottom: 10, marginLeft: 30, marginTop: 10 },
      { width: 300, height: 40, marginBottom: 10, marginLeft: 30, marginTop: 10 },
      { width: 300, height: 40, marginBottom: 10, marginLeft: 30, marginTop: 10 },
      { width: 300, height: 40, marginBottom: 10, marginLeft: 30, marginTop: 10 },
    ]}
  >
    {children}
  </SkeletonContent>
);

export { LoadingListSkeleton };
