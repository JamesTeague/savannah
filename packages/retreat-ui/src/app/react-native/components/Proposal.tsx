import React from 'react';
import { Body, ListItem, Right, Text } from 'native-base';
import { PrimaryBadge } from './PrimaryBadge';

type ProposalProps = {
  proposal: {
    id: string;
    city: {
      id: string;
      name: string;
      address: string;
      state: {
        name: string;
        abbreviation: string;
      };
      country: {
        name: string;
        abbreviation: string;
      };
    };
    vote?: {
      id: string;
      points: number;
    };
  };
};

const Proposal = ({ proposal }: ProposalProps) => (
  <ListItem onPress={() => alert(proposal.city.name)}>
    <Body>
      <Text>{proposal.city.name}</Text>
      <Text note>{`${proposal.city.state.name}, ${proposal.city.country.abbreviation}`}</Text>
    </Body>
    {proposal.vote && (
      <Right>
        <PrimaryBadge number={proposal.vote.points} />
      </Right>
    )}
  </ListItem>
);

export { Proposal };
