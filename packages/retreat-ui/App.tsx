import React from 'react';
import { StyleSheet } from 'react-native';
import NativeApp from './src/app/react-native/NativeApp';

export default function App() {
  return <NativeApp />;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
