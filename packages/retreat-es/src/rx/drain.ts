import * as Rx from 'rxjs';
import * as RxOp from 'rxjs/operators';

/**
 * Asynchronously drains a given observable of elements, returning an array of collected resources
 * or throwing an Error if the stream is aborted due to an error.
 *
 * @param {Rx.Observable<T>} stream The input stream to drain.
 * @returns {Promise<Array<T>>} An array representing each element received, in order.
 */
const drain = <T>(stream: Rx.Observable<T>): Promise<Array<T>> =>
  stream.pipe(RxOp.reduce((a, b) => [...a, b], [] as T[])).toPromise();

export default drain;
export { drain };
