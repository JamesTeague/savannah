import * as Rx from 'rxjs';
import drain from './drain';

describe('drain utility', () => {
  it('drains a stream', () => {
    return drain(Rx.from([1, 2, 3])).then(result => {
      expect(result).toEqual([1, 2, 3]);
    });
  });
});
