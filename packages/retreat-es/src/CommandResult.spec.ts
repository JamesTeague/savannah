import CommandResult from './CommandResult';
import uuid from 'uuid/v4';
import moment from 'moment';
import User from './User';

describe('CommandResult', () => {
  let commandResult;
  let resultId = uuid();

  beforeEach(() => {
    commandResult = new CommandResult({
      id: resultId,
      timestamp: moment(),
      success: true,
      data: {},
      user: User.dummy(),
    });
  });

  it('serializes correctly', () => {
    const result = commandResult.toJSON();

    expect(result).toMatchObject({
      id: resultId,
      success: true,
      data: {},
      user: User.dummy().toJSON(),
    });
  });

  it('creates a result from dto', () => {
    const result = CommandResult.fromJSON(commandResult.toJSON());

    expect(result.id).toBe(resultId);
    expect(result.user).toBeInstanceOf(User);
    expect(result.success).toBeTruthy();
    expect(result.data).toEqual({});
  });
});
