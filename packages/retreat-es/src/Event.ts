import moment from 'moment';
import { Base, IOTypes } from '@penguinhouse/savannah-io';
import { createEventFactory } from './util';
import { EventShape, ICreateChildEvents } from './interfaces';

export default class Event<T> implements ICreateChildEvents, Base.ISerializable<IOTypes.Event<T>> {
  id: string;
  timestamp: moment.Moment;
  action: string;
  data: T;
  eventFactory: Function;

  constructor(args: EventShape<T>) {
    this.id = args.id;
    this.timestamp = args.timestamp;
    this.action = args.action;
    this.data = args.data;
    this.eventFactory = createEventFactory();
  }

  createChildEvent<R>(action: string, data: R): Event<R> {
    return this.eventFactory(action, data);
  }

  toJSON(): IOTypes.Event<T> {
    return {
      id: this.id,
      timestamp: this.timestamp.utc(),
      action: this.action,
      data: this.data,
    };
  }

  static fromJSON<V>(ioEvent: IOTypes.Event<V>): Event<V> {
    const timestamp = moment(ioEvent.timestamp).utc();

    return new Event({
      id: ioEvent.id,
      timestamp,
      action: ioEvent.action,
      data: ioEvent.data,
    });
  }
}
