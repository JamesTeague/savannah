import { Base } from '@penguinhouse/savannah-io';
import Command from './Command';
import CommandResult from './CommandResult';
import ExecutionContextBase from './ExecutionContextBase';
import { ICreateCommandResults, ISavannahPlatform } from './interfaces';

export default class CommandExecutionContext extends ExecutionContextBase
  implements ICreateCommandResults {
  command: Command<any>;
  fallback: Base.Nullable<CommandResult<any>>;
  result: Base.Nullable<CommandResult<any>>;

  constructor(cmd: Command<any>) {
    super(cmd);

    this.command = cmd;
    this.fallback = null;
    this.result = null;
  }

  setFallbackResult(success: boolean, data: Object): void {
    this.fallback = this.command.createCommandResult(success, data);
  }

  createCommandResult(success: boolean, data: Object): CommandResult<any> {
    if (this.result) {
      throw new Error('Attempted to set multiple command results.');
    }

    this.result = this.command.createCommandResult(success, data);

    return this.result;
  }

  drain(savannah: ISavannahPlatform): Promise<void> {
    return super.drain(savannah).then(() => {
      // Check in the continuation. Command results are critically important, but NOT moreso than
      // propagation of events after a clean execution. I'd rather a user refresh a page to see
      // the result of a command than mutate data and never tell anyone about it because I wasn't
      // able to tell someone I don't know and probably wouldn't like that something bad happened.
      const result = this.result || this.fallback;

      if (!result) {
        throw new Error('No result set for command!');
      }

      return savannah.notifyResult(result);
    });
  }

  validate(): boolean {
    return !!(this.fallback || this.result);
  }
}
