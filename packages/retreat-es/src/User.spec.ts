import uuid from 'uuid/v4';
import User from './User';

describe('User', () => {
  let user;
  const userId = uuid();
  const retreatIds = [uuid(), uuid()];
  const email = 'test@example.com';
  const groups = ['admin', 'voter'];
  const roles = ['voter', 'guest'];
  const permissions = ['read:vote', 'cast:vote'];
  const name = {
    first: 'test',
    last: 'user',
  };

  beforeEach(() => {
    user = new User({
      id: userId,
      retreatIds,
      email,
      groups,
      roles,
      permissions,
      name,
    });
  });

  it('partOf functions properly', () => {
    expect(user.partOf('admin')).toBeTruthy();
    expect(user.partOf('member')).toBeFalsy();
  });

  it('partOfAny functions properly', () => {
    expect(user.partOfAny(['admin', 'test'])).toBeTruthy();
    expect(user.partOfAny(['test', 'test2'])).toBeFalsy();
  });

  it('partOfAll functions properly', () => {
    expect(user.partOfAll(['admin', 'voter'])).toBeTruthy();
    expect(user.partOfAll(['admin', 'test'])).toBeFalsy();
  });

  it('is functions properly', () => {
    expect(user.is('voter')).toBeTruthy();
    expect(user.is('admin')).toBeFalsy();
  });

  it('isAny functions properly', () => {
    expect(user.isAny(['voter', 'test'])).toBeTruthy();
    expect(user.isAny(['admin', 'test'])).toBeFalsy();
  });

  it('isAll functions properly', () => {
    expect(user.isAll(['guest', 'voter'])).toBeTruthy();
    expect(user.isAll(['admin', 'voter'])).toBeFalsy();
  });

  it('can functions properly', () => {
    expect(user.can('read:vote')).toBeTruthy();
    expect(user.can('read:proposal')).toBeFalsy();
  });

  it('canAny functions properly', () => {
    expect(user.canAny(['read:proposal', 'read:vote'])).toBeTruthy();
    expect(user.canAny(['read:proposal', 'make:proposal'])).toBeFalsy();
  });

  it('canAll functions properly', () => {
    expect(user.canAll(['read:vote', 'cast:vote'])).toBeTruthy();
    expect(user.canAll(['read:vote', 'make:proposal'])).toBeFalsy();
  });

  it('serializes properly', () => {
    const json = user.toJSON();

    expect(json).toEqual({
      id: userId,
      retreatIds,
      email,
      name,
      groups,
      roles,
      permissions,
    });
  });

  it('getClaimLookup templatizes a string', () => {
    expect(User.getClaimLookup('test')).toEqual({
      groups: 'test/groups',
      roles: 'test/roles',
      permissions: 'test/permissions',
      appMetadata: 'test/app_metadata',
      userMetadata: 'test/user_metadata',
      userInfo: 'test/user_info',
    });
  });

  it('creates a dummy user', () => {
    const dummy = User.dummy();

    expect(dummy).toMatchObject({
      id: '0',
      name: {
        first: 'Test',
        last: 'User',
      },
      email: 'testuser@gmail.com',
      retreatIds: [],
      groups: [],
      roles: [],
      permissions: ['create:command'],
    });
  });
});
