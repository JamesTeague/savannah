import EventExecutionContext from './EventExecutionContext';
import Event from './Event';
import moment from 'moment';
import uuid from 'uuid/v4';
import SavannahPlatform from './SavannahPlatform';
jest.mock('./SavannahPlatform');
// jest.mock('./Event');

describe('EventExecutionContext and ExecutionContextBase', () => {
  let eventExecutionContext;
  let event = new Event({
    id: uuid(),
    timestamp: moment(),
    action: 'ExecutionContextBase.spec.ts',
    data: {},
  });

  beforeEach(() => {
    (SavannahPlatform as jest.Mock).mockClear();
    // (Event as any as jest.Mock).mockClear();

    eventExecutionContext = new EventExecutionContext(event);
  });

  it('creates a child event', () => {
    // const eventInstance = (event as any as jest.Mock).mock.instances[0];
    // eventInstance.createChildEvent.mockReturnValue(Even)
    const spy = jest.spyOn(event, 'createChildEvent');
    const childEvent = eventExecutionContext.createChildEvent('test-ran', {});

    expect(spy).toHaveBeenCalled();
    expect(childEvent.data).toEqual({});
    expect(childEvent.action).toBe('test-ran');
    expect(eventExecutionContext.events).toEqual([childEvent]);
  });

  it('drains events', async () => {
    const savannahPlatform = new SavannahPlatform({
      appName: 'EventExecutionContext.spec.ts',
    });

    eventExecutionContext.createChildEvent('test-ran', {});
    eventExecutionContext.createChildEvent('test-ran', {});

    await eventExecutionContext.drain(savannahPlatform);

    expect(savannahPlatform.notifyEvent).toHaveBeenCalledTimes(2);
  });

  it('discards events', () => {
    eventExecutionContext.createChildEvent('test-ran', {});
    eventExecutionContext.discard();

    expect(eventExecutionContext.events).toEqual([]);
  });

  it('validates itself', () => {
    expect(eventExecutionContext.validate()).toBeTruthy();
  });
});
