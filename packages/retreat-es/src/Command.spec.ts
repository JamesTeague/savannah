import uuid from 'uuid/v4';
import Command from './Command';
import moment from 'moment';
import User from './User';
import Event from './Event';
import CommandResult from './CommandResult';

describe('Command', () => {
  let command;
  const commandId = uuid();

  beforeEach(() => {
    command = new Command({
      id: commandId,
      timestamp: moment(),
      action: 'test-action',
      user: User.dummy(),
      data: { testField: 'test' },
    });
  });

  it('creates a child event', () => {
    const childEvent = command.createChildEvent('create-child-event', {
      payload: 'test',
    });

    expect(childEvent).toBeInstanceOf(Event);
    expect(childEvent.action).toBe('create-child-event');
    expect(childEvent.data).toStrictEqual({ payload: 'test' });
  });

  it('creates a command result', () => {
    const successfulResult = command.createCommandResult(true, {
      payload: 'test',
    });

    expect(successfulResult).toBeInstanceOf(CommandResult);
    expect(successfulResult.timestamp).toBeDefined();
    expect(successfulResult.data).toStrictEqual({ payload: 'test' });
  });

  it('serializes correctly', () => {
    const json = command.toJSON();

    expect(json).toMatchObject({
      id: commandId,
      action: 'test-action',
      user: User.dummy().toJSON(),
      data: {
        testField: 'test',
      },
    });
  });

  it('creates a command from dto', () => {
    const cmd = Command.fromJSON(command.toJSON());

    expect(cmd.id).toBe(commandId);
    expect(cmd.user).toBeInstanceOf(User);
    expect(cmd.action).toBe('test-action');
    expect(cmd.data).toEqual({ testField: 'test' });
  });
});
