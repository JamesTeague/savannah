import { Base, IOTypes } from '@penguinhouse/savannah-io';
import { IHasGroupMembership, IHasPermissions, IHasRoles } from './interfaces';

type CreateUserArgs = {
  id: string;
  retreatIds: string[];
  email: string;
  name: {
    first: string;
    last: string;
  };
  groups: string[];
  roles: string[];
  permissions: string[];
};

export default class User
  implements IHasGroupMembership, IHasPermissions, IHasRoles, Base.ISerializable<IOTypes.User> {
  id: string;
  retreatIds: string[];
  email: string;
  groups: Array<string>;
  roles: string[];
  permissions: string[];
  name: {
    first: string;
    last: string;
  };

  constructor(args: CreateUserArgs) {
    this.id = args.id;
    this.retreatIds = args.retreatIds;
    this.email = args.email;
    this.name = args.name;
    this.groups = args.groups;
    this.roles = args.roles;
    this.permissions = args.permissions;
  }

  partOf(group: string) {
    return this.groups.includes(group);
  }

  partOfAny(authGroups: string[]) {
    return authGroups.some(group => this.partOf(group));
  }

  partOfAll(authGroups: string[]) {
    return authGroups.every(group => this.partOf(group));
  }

  is(role: string) {
    return this.roles.includes(role);
  }

  isAny(authRoles: string[]) {
    return authRoles.some(role => this.is(role));
  }

  isAll(authRoles: string[]) {
    return authRoles.every(role => this.is(role));
  }

  can(permission: string) {
    return this.permissions.includes(permission);
  }

  canAny(authPermissions: string[]) {
    return authPermissions.some(permission => this.can(permission));
  }

  canAll(authPermissions: string[]) {
    return authPermissions.every(permission => this.can(permission));
  }

  toJSON(): IOTypes.User {
    return {
      id: this.id,
      retreatIds: this.retreatIds,
      email: this.email,
      name: this.name,
      groups: this.groups,
      roles: this.roles,
      permissions: this.permissions,
    };
  }

  static fromJwt(authNamespace: string) {
    const claimLookup = this.getClaimLookup(authNamespace);

    return (decodedJwt: any): User => {
      const groups = decodedJwt[claimLookup.groups];
      const roles = decodedJwt[claimLookup.roles];
      const permissions = decodedJwt[claimLookup.permissions];
      const userInfo = decodedJwt[claimLookup.userInfo];

      return new User({
        id: userInfo.id,
        name: {
          first: userInfo.fullname.first,
          last: userInfo.fullname.last,
        },
        email: userInfo.email,
        retreatIds: userInfo.retreatIds,
        groups,
        roles,
        permissions,
      });
    };
  }

  static getClaimLookup(authNamespace: string) {
    return {
      groups: `${authNamespace}/groups`,
      roles: `${authNamespace}/roles`,
      permissions: `${authNamespace}/permissions`,
      appMetadata: `${authNamespace}/app_metadata`,
      userMetadata: `${authNamespace}/user_metadata`,
      userInfo: `${authNamespace}/user_info`,
    };
  }

  static dummy() {
    return new User({
      id: '0',
      name: {
        first: 'Test',
        last: 'User',
      },
      email: 'testuser@gmail.com',
      retreatIds: [],
      groups: [],
      roles: [],
      permissions: ['create:command'],
    });
  }
}
