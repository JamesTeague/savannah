import moment from 'moment';
import { IOTypes, Base } from '@penguinhouse/savannah-io';
import { createCommandResultFactory, createEventFactory } from './util';
import User from './User';
import Event from './Event';
import CommandResult from './CommandResult';
import { CommandShape, ICreateChildEvents, ICreateCommandResults } from './interfaces';

export default class Command<T>
  implements
    ICreateChildEvents,
    ICreateCommandResults,
    Base.ISerializable<IOTypes.Command<T>>,
    CommandShape<T> {
  id: string;
  timestamp: moment.Moment;
  action: string;
  user: User;
  data: T;
  eventFactory: Function;
  commandResultFactory: Function;

  constructor(commandShape: CommandShape<T>) {
    this.id = commandShape.id;
    this.timestamp = commandShape.timestamp;
    this.action = commandShape.action;
    this.user = commandShape.user;
    this.data = commandShape.data;
    this.eventFactory = createEventFactory();
    this.commandResultFactory = createCommandResultFactory(commandShape.user);
  }

  createChildEvent<R>(action: string, data: R): Event<R> {
    return this.eventFactory(action, data);
  }

  createCommandResult<R>(success: boolean, data: R): CommandResult<R> {
    return this.commandResultFactory(success, data);
  }

  toJSON(): IOTypes.Command<T> {
    return {
      id: this.id,
      timestamp: this.timestamp.utc(),
      action: this.action,
      data: this.data,
      user: this.user.toJSON(),
    };
  }

  static fromJSON<V>(ioCommand: IOTypes.Command<V>): Command<V> {
    const user = new User(ioCommand.user);

    return new Command({
      id: ioCommand.id,
      timestamp: moment(ioCommand.timestamp).utc(),
      action: ioCommand.action,
      user,
      data: ioCommand.data,
    });
  }
}
