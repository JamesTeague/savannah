import * as Rx from 'rxjs';
import * as RxOp from 'rxjs/operators';
import cyrus, { IRxNotifier } from 'cyrus-rx';
import stoolie, { ILogger, LogLevel } from 'stoolie';
import { ConsumeCommandArgs, ConsumeEventArgs, IDrainable, ISavannahPlatform } from './interfaces';
import Event from './Event';
import Command from './Command';
import CommandResult from './CommandResult';
import CommandExecutionContext from './CommandExecutionContext';
import EventExecutionContext from './EventExecutionContext';

type PlatformConfig = {
  appName: string;
  user?: string;
  password?: string;
  host?: string;

  port?: number;
  database?: string;
  logLevel?: LogLevel;
};

type ConsumerMap = {
  [key: string]: Rx.ConnectableObservable<any>[];
};

export default class SavannahPlatform implements ISavannahPlatform {
  private consumers: ConsumerMap;
  private notifier: IRxNotifier;
  private subscription: Rx.Subscription;
  private logger: ILogger;

  constructor(args: PlatformConfig) {
    this.notifier = cyrus(args);
    this.consumers = {
      commands: [],
      events: [],
    };
    this.subscription = new Rx.Subscription();
    this.logger = stoolie(args.appName, args.logLevel).withCategory('savannah-platform');
  }

  onCommand(args: ConsumeCommandArgs<any, any>) {
    const {
      action,
      // payloadType,
      handler,
    } = args;

    // const CommandPayloadType = payloadType;

    const observable = this.notifier.on('commands');

    observable
      .pipe(
        RxOp.filter((cmd: string) => cmd !== 'ready'),
        RxOp.map((cmd: string) => {
          const shape = JSON.parse(cmd);
          return Command.fromJSON(shape);
        }),
        RxOp.filter((command: Command<any>) => command.action === action),
        RxOp.mergeMap(
          (command: Command<any>) => {
            const execEntry = this.logger.withFields({ ...command.toJSON() });

            execEntry.info('Processing command in execution context.');
            const context = new CommandExecutionContext(command);
            context.setFallbackResult(false, {
              code: 'G500',
              message: 'An unknown error has occurred.',
            });

            const { data } = command;

            try {
              // CommandPayloadType.assert(data);

              return Rx.from(handler(context, command.user, data)).pipe(
                RxOp.mapTo(context),
                RxOp.catchError((error: Error) => {
                  execEntry.withError(error).error('Command handler reported an error');

                  context.discard();

                  return Rx.of(context);
                })
              );
            } catch (error) {
              // execEntry.withError(error)
              //   .error('Command payload did not pass validation');

              context.discard();
              context.createCommandResult(false, {
                code: 'V400',
                message: 'Command payload did not pass validation.',
              });

              return Rx.of(context);
            }
          },
          undefined,
          1
        ),
        RxOp.mergeMap((drainable: IDrainable) => drainable.drain(this))
      )
      .subscribe();

    this.consumers.commands.push(observable);
  }

  onEvent(args: ConsumeEventArgs<any, any>) {
    const {
      action,
      // payloadType,
      handler,
    } = args;
    const observable = this.notifier.on('events');

    observable
      .pipe(
        RxOp.filter((event: string) => event !== 'ready'),
        RxOp.map((evt: string) => {
          const shape = JSON.parse(evt);
          return Event.fromJSON(shape);
        }),
        RxOp.filter((evt: Event<any>) => evt.action === action),
        RxOp.mergeMap(
          (evt: Event<any>) => {
            const execEntry = this.logger.withFields({ ...evt.toJSON() });
            execEntry.info('Processing event in execution context.');

            const context = new EventExecutionContext(evt);
            const { data } = evt;

            try {
              // payloadType.assert(data);

              return Rx.from(handler(context, data)).pipe(
                RxOp.mapTo(context),
                RxOp.catchError((error: Error) => {
                  execEntry.withError(error).error('Event handler reported an error');

                  context.discard();

                  return Rx.of(context);
                })
              );
            } catch (error) {
              // execEntry.withError(error)
              //   .error('Event payload did not pass validation.');

              context.discard();

              return Rx.of(context);
            }
          },
          undefined,
          1
        ),
        RxOp.mergeMap((drainable: IDrainable) => drainable.drain(this))
      )
      .subscribe();

    this.consumers.events.push(observable);
  }

  notifyEvent(event: Event<any>) {
    this.logger.withFields({ type: 'event', event }).info('Notifying event.');

    return this.notifier.notify('events', JSON.stringify(event.toJSON()));
  }

  notifyCommand(cmd: Command<any>) {
    this.logger.withFields({ type: 'command', cmd }).info('Notifying command.');

    return this.notifier.notify('commands', JSON.stringify(cmd.toJSON()));
  }

  notifyResult(result: CommandResult<any>) {
    this.logger.withFields({ type: 'result', result }).info('Notifying result.');

    return this.notifier.notify('results', JSON.stringify(result.toJSON()));
  }

  connectNotifier() {
    return this.notifier.connect();
  }

  connect() {
    Object.values(this.consumers).forEach((consumerList: Rx.ConnectableObservable<any>[]) => {
      consumerList.forEach((consumer: Rx.ConnectableObservable<any>) => {
        this.subscription.add(consumer.connect());
      });
    });

    this.logger.verbose('Platform connected.');
  }

  async disconnect() {
    await this.notifier.disconnect();
    this.subscription.unsubscribe();

    return Promise.resolve();
  }
}
