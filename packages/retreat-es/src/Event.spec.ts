import Event from './Event';
import uuid from 'uuid/v4';
import moment from 'moment';

describe('Event', () => {
  let event;
  const eventId = uuid();

  beforeEach(() => {
    event = new Event({
      id: eventId,
      timestamp: moment(),
      action: 'Event.spec.ts',
      data: {},
    });
  });

  it('creates a child event', () => {
    const childEvent = event.createChildEvent('test-ran', {});

    expect(childEvent).toBeInstanceOf(Event);
    expect(childEvent.action).toBe('test-ran');
    expect(childEvent.data).toEqual({});
  });

  it('serializes correctly', () => {
    const json = event.toJSON();

    expect(json).toMatchObject({
      id: eventId,
      action: 'Event.spec.ts',
      data: {},
    });
  });

  it('creates event from dto', () => {
    const json = Event.fromJSON(event.toJSON());

    expect(json).toBeInstanceOf(Event);
    expect(json.id).toBe(eventId);
    expect(json.action).toBe('Event.spec.ts');
    expect(json.data).toEqual({});
  });
});
