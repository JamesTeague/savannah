import Bluebird from 'bluebird';
import { ICreateChildEvents, IDrainable, ISavannahPlatform } from './interfaces';
import Event from './Event';

export default abstract class ExecutionContextBase implements ICreateChildEvents, IDrainable {
  private base: ICreateChildEvents;

  events: Event<any>[];

  constructor(base: ICreateChildEvents) {
    this.events = [];
    this.base = base;
  }

  createChildEvent(action: string, data: Object): Event<any> {
    const event = this.base.createChildEvent(action, data);

    this.events.push(event);

    return event;
  }

  drain(savannah: ISavannahPlatform): Promise<void> {
    return Bluebird.all(this.events.map(event => savannah.notifyEvent(event))).then(
      () => undefined
    );
  }

  discard(): void {
    this.events = [];
  }

  abstract validate(): boolean;
}
