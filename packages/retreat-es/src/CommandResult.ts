import moment from 'moment';
import { Base, IOTypes } from '@penguinhouse/savannah-io';
import User from './User';
import { CommandResultShape } from './interfaces';

export default class CommandResult<T> implements Base.ISerializable<IOTypes.CommandResult<T>> {
  id: string;
  timestamp: moment.Moment;
  success: boolean;
  data: T;
  user: User;

  constructor(args: CommandResultShape<T>) {
    this.id = args.id;
    this.timestamp = args.timestamp;
    this.success = args.success;
    this.data = args.data;
    this.user = args.user;
  }

  toJSON(): IOTypes.CommandResult<T> {
    return {
      id: this.id,
      timestamp: this.timestamp.utc(),
      success: this.success,
      data: this.data,
      user: this.user.toJSON(),
    };
  }

  static fromJSON<T>(ioCommandResult: IOTypes.CommandResult<T>): CommandResult<T> {
    const user = new User(ioCommandResult.user);
    const timestamp = moment(ioCommandResult.timestamp).utc();

    return new CommandResult({
      id: ioCommandResult.id,
      timestamp,
      success: ioCommandResult.success,
      data: ioCommandResult.data,
      user,
    });
  }
}
