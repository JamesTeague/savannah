import CommandExecutionContext from './CommandExecutionContext';
import Command from './Command';
import moment from 'moment';
import User from './User';
import uuid from 'uuid/v4';
import SavannahPlatform from './SavannahPlatform';
jest.mock('./Command');
jest.mock('./SavannahPlatform');

describe('CommandExecutionContext', () => {
  let context;
  let commandInstance;
  const commandId = uuid();

  beforeEach(() => {
    ((Command as any) as jest.Mock).mockClear();
    ((SavannahPlatform as any) as jest.Mock).mockClear();

    context = new CommandExecutionContext(
      new Command<any>({
        id: commandId,
        timestamp: moment(),
        action: 'test-action',
        user: User.dummy(),
        data: { testField: 'test' },
      })
    );
  });

  it('sets a fallback result', () => {
    commandInstance = ((Command as any) as jest.Mock).mock.instances[0];

    context.setFallbackResult(false, { error: 'bad request' });

    expect(commandInstance.createCommandResult).toHaveBeenCalledWith(false, {
      error: 'bad request',
    });
  });

  it('creates a command result', () => {
    commandInstance = ((Command as any) as jest.Mock).mock.instances[0];

    context.createCommandResult(true, { id: commandId });

    expect(commandInstance.createCommandResult).toHaveBeenCalledWith(true, {
      id: commandId,
    });
  });

  it('throws an error when setting multiple command results', () => {
    commandInstance = ((Command as any) as jest.Mock).mock.instances[0];
    commandInstance.createCommandResult.mockImplementationOnce(() => ({}));

    context.createCommandResult(true, { id: commandId });

    expect(() => {
      context.createCommandResult(true, { id: commandId });
    }).toThrowError('Attempted to set multiple command results.');
  });

  it('drains context', async done => {
    const savannahPlatform = new SavannahPlatform({
      appName: 'CommandExecutionContext.spec.ts',
    });
    commandInstance = ((Command as any) as jest.Mock).mock.instances[0];
    commandInstance.createCommandResult.mockImplementationOnce(() => ({}));

    context.createCommandResult(true, {});
    await context.drain(savannahPlatform).catch(() => {});

    expect(savannahPlatform.notifyResult).toHaveBeenCalled();

    done();
  });

  it('does not drain context without a result', () => {
    const savannahPlatform = new SavannahPlatform({
      appName: 'CommandExecutionContext.spec.ts',
    });

    expect(context.drain(savannahPlatform)).rejects.toThrowError('No result set for command!');
  });

  it('validates the context', () => {
    commandInstance = ((Command as any) as jest.Mock).mock.instances[0];
    commandInstance.createCommandResult.mockImplementationOnce(() => ({}));

    context.createCommandResult(true, {});

    expect(context.validate()).toBeTruthy();
  });

  it('validates the context to be false', () => {
    expect(context.validate()).toBeFalsy();
  });
});
