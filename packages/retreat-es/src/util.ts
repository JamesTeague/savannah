import moment from 'moment';
import uuid from 'uuid/v4';

import User from './User';
import Event from './Event';
import CommandResult from './CommandResult';

/**
 * Light functional helper that can be used to trivialize event creation and centralize usage of the
 * underlying timestamp and ID system that we use.
 *
 * Creates a factory function that, when invoked with required data, fills out the remainder of the
 * event structure from internal dependencies.
 */
function createEventFactory() {
  return <T>(action: string, data: T): Event<T> =>
    new Event({
      action,
      data,
      id: uuid(),
      timestamp: moment().utc(),
    });
}

/**
 * Light functional helper that can be used to trivialize command result creation and centralize
 * usage of the underlying timestamp and ID system that we use.
 *
 * Creates a factory function that, when invoked with required data, fills out the remainder of the
 * command result structure from internal dependencies.
 */
function createCommandResultFactory(user: User) {
  return <T>(success: boolean, data: T): CommandResult<T> =>
    new CommandResult({
      success,
      data,
      user,
      id: uuid(),
      timestamp: moment().utc(),
    });
}

export { createEventFactory, createCommandResultFactory };
