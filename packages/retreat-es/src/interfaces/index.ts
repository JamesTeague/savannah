import Event from '../Event';
import CommandResult from '../CommandResult';
import * as moment from 'moment';
import Command from '../Command';
import User from '../User';

export interface ICreateChildEvents {
  createChildEvent<T>(action: string, data: T): Event<T>;
}

export interface ICreateCommandResults {
  createCommandResult<T>(success: boolean, data: T): CommandResult<T>;
}

export interface IDrainable {
  drain(savannah: ISavannahPlatform): Promise<any>;
}

export interface IExecutionResults {
  event: [string, Event<any>][];
  results: CommandResult<any>[];
}

export interface IHasGroupMembership {
  partOf(group: string): boolean;
  partOfAny(groups: string[]): boolean;
  partOfAll(groups: string[]): boolean;
}

export interface IHasPermissions {
  can(permission: string): boolean;
  canAll(permissions: string[]): boolean;
  canAny(permissions: string[]): boolean;
}

export interface IHasRoles {
  is(role: string): boolean;
  isAll(roles: string[]): boolean;
  isAny(roles: string[]): boolean;
}

export interface ICommandExecutionContext extends ICreateCommandResults, ICreateChildEvents {}

export interface IEventExecutionContext extends ICreateChildEvents {}

export type ConsumeCommandHandler<T, R> = (
  context: ICommandExecutionContext,
  user: User,
  data: T
) => Promise<R>;

export type ConsumeCommandArgs<T, R> = {
  action: string;
  handler: ConsumeCommandHandler<T, R>;
};

export type ConsumeEventHandler<T, R> = (context: IEventExecutionContext, data: T) => Promise<R>;

export type ConsumeEventArgs<T, R> = {
  action: string;
  handler: ConsumeEventHandler<T, R>;
};

export interface ISavannahPlatform {
  onCommand(args: ConsumeCommandArgs<any, any>): void;
  onEvent(args: ConsumeEventArgs<any, any>): void;
  notifyEvent(event: Event<any>): Promise<any>;
  notifyCommand(command: Command<any>): Promise<any>;
  notifyResult(result: CommandResult<any>): Promise<any>;
  connectNotifier(): Promise<boolean>;
  connect(): void;
  disconnect(): Promise<void>;
}

export interface CommandShape<T> {
  id: string;
  timestamp: moment.Moment;
  action: string;
  user: User;
  data: T;
}

export interface CommandResultShape<T> {
  id: string;
  timestamp: moment.Moment;
  success: boolean;
  data: T;
  user: User;
}

export interface EventShape<T> {
  id: string;
  timestamp: moment.Moment;
  action: string;
  data: T;
}
