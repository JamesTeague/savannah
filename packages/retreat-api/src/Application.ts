import { User } from '@penguinhouse/retreat-es';
import { PubSub } from 'apollo-server';
import { ILogger } from 'stoolie';
import {
  ICityRepository,
  ICityWarehouse,
  IMemberRepository,
  IMemberWarehouse,
  IProposalRepository,
  IProposalWarehouse,
  IRetreatRepository,
  IRetreatWarehouse,
  IVoteRepository,
  IVoteWarehouse,
} from './persistence/interfaces';

export type ApolloServerContext = {
  logger: ILogger;
  memberWarehouse: IMemberWarehouse;
  cityWarehouse: ICityWarehouse;
  proposalWarehouse: IProposalWarehouse;
  retreatWarehouse: IRetreatWarehouse;
  voteWarehouse: IVoteWarehouse;
  user: User;
  pubSub: PubSub;
};

export type SavannahContext = {
  memberRepository: IMemberRepository;
  memberWarehouse: IMemberWarehouse;
  cityRepository: ICityRepository;
  cityWarehouse: ICityWarehouse;
  proposalRepository: IProposalRepository;
  proposalWarehouse: IProposalWarehouse;
  retreatRepository: IRetreatRepository;
  retreatWarehouse: IRetreatWarehouse;
  voteRepository: IVoteRepository;
  voteWarehouse: IVoteWarehouse;
  pubSub: PubSub;
};
