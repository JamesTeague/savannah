import { ILogger } from 'stoolie';
import { Base, IOTypes } from '@penguinhouse/savannah-io';
import Bluebird from 'bluebird';
import Knex from 'knex';
import * as t from 'io-ts';
import KnexRepositoryBase from './KnexRepositoryBase';
import { IProposalRepository, IProposalWarehouse } from './interfaces';
import { Proposal } from '../types';

const PgProposal = t.type({
  id: t.string,
  member_id: t.string,
  city_id: t.string,
  retreat_id: t.string,
});

const ProposalTransformers = {
  fromPgProposal(pgProposal: t.TypeOf<typeof PgProposal>): Proposal {
    return {
      cityId: pgProposal.city_id,
      id: pgProposal.id,
      memberId: pgProposal.member_id,
      retreatId: pgProposal.retreat_id,
    };
  },

  toPgProposal(proposal: Proposal): t.TypeOf<typeof PgProposal> {
    return {
      id: proposal.id,
      member_id: proposal.memberId,
      city_id: (proposal.cityId as unknown) as string,
      retreat_id: proposal.retreatId,
    };
  },
  fromProposalMadeEvent(event: IOTypes.ProposalMade): t.TypeOf<typeof PgProposal> {
    return {
      id: event.id,
      member_id: event.memberId,
      city_id: event.city.id,
      retreat_id: event.retreatId,
    };
  },
};

function parseProposalIfExists(pgProposal: Base.Nullable<t.TypeOf<typeof PgProposal>>) {
  if (!pgProposal) {
    return null;
  }

  return ProposalTransformers.fromPgProposal(pgProposal);
}

function appendRetreatFilterToQuery<T>(
  retreatId: Base.Nullable<string>,
  builder: Knex.QueryBuilder<T>
) {
  return retreatId ? builder.where({ retreat_id: retreatId }) : builder;
}

export default class KnexProposalWarehouse extends KnexRepositoryBase<t.InterfaceType<any>>
  implements IProposalWarehouse, IProposalRepository {
  entry: ILogger;

  constructor(knex: Knex, logger: ILogger) {
    super(knex, logger, PgProposal);

    this.entry = logger.withCategory('warehouse').withFields({ type: 'proposal-warehouse' });
  }

  addProposal(event: IOTypes.ProposalMade) {
    this.entry.withFields({ event }).verbose('Adding proposal.');

    const pgProposal = ProposalTransformers.fromProposalMadeEvent(event);

    const query = this.knex('proposals').insert(pgProposal);

    return this.runQuery(query).thenReturn();
  }

  getProposalByProposalId(proposalId: string): Bluebird<Base.Nullable<Proposal>> {
    this.entry.withFields({ proposalId }).verbose('Getting proposal by id.');

    const query = this.knex('proposals')
      .where({ id: proposalId })
      .first(this.columns);

    return this.runQuery(query).then(parseProposalIfExists);
  }

  getProposalsByMemberId(memberId: string): Bluebird<Proposal[]> {
    this.entry.withFields({ memberId }).verbose('Getting proposals by member id.');

    const query = this.knex('proposals')
      .where({ member_id: memberId })
      .select(this.columns);

    return this.runQuery(query).map(ProposalTransformers.fromPgProposal);
  }

  getProposalsByRetreatId(retreatId: Base.Nullable<string>): Bluebird<Proposal[]> {
    this.entry.withFields({ retreatId }).verbose('Getting proposals by retreat id.');

    const query = this.knex('proposals').select(this.columns);

    const retreatWrapper = appendRetreatFilterToQuery(retreatId, query);

    return this.runQuery(retreatWrapper).map(ProposalTransformers.fromPgProposal);
  }
}
