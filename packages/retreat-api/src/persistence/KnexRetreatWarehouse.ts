import * as t from 'io-ts';
import KnexRepositoryBase from './KnexRepositoryBase';
import { IRetreatRepository, IRetreatWarehouse } from './interfaces';
import { ILogger } from 'stoolie';
import Knex from 'knex';
import { IOTypes } from '@penguinhouse/savannah-io';

const PgRetreat = t.type({
  id: t.string,
  name: t.string,
  phase: t.string,
});

export default class KnexRetreatWarehouse extends KnexRepositoryBase<t.InterfaceType<any>>
  implements IRetreatRepository, IRetreatWarehouse {
  entry: ILogger;

  constructor(knex: Knex, logger: ILogger) {
    super(knex, logger, PgRetreat);

    this.entry = logger.withCategory('warehouse').withFields({ type: 'retreat-warehouse' });
  }

  registerRetreat(event: IOTypes.RetreatRegistered) {
    this.entry.withFields({ event }).verbose('Registering retreat.');

    const retreat = {
      id: event.id,
      name: event.name,
      phase: IOTypes.Phase.Proposal,
    };

    const query = this.knex('retreats').insert(retreat);

    return this.runQuery(query).thenReturn();
  }

  getRetreatByRetreatId(retreatId: string) {
    this.entry.withFields({ retreatId }).verbose('Getting retreat by id.');

    const query = this.knex('retreats')
      .where({ id: retreatId })
      .first(this.columns);

    return this.runQuery(query);
  }
}
