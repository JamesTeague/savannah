import Knex from 'knex';
import mockDb from 'mock-knex';
import KnexRetreatWarehouse from './KnexRetreatWarehouse';
import { NullLog } from 'stoolie';
import Bluebird from 'bluebird';
import { IOTypes } from '@penguinhouse/savannah-io';

describe('KnexRetreatWarehouse', () => {
  let warehouse: KnexRetreatWarehouse;
  let runQueryOriginal;
  let withFieldsSpy;
  let verboseSpy;
  let runQuerySpy;

  const knex = Knex({ client: 'pg' });
  const event = {
    id: '0',
    name: 'test-retreat',
    phase: IOTypes.Phase.Proposal,
  };

  beforeEach(() => {
    mockDb.mock(knex);
    warehouse = new KnexRetreatWarehouse(knex, NullLog);
    runQueryOriginal = warehouse.runQuery;
    withFieldsSpy = jest.spyOn(NullLog, 'withFields');
    verboseSpy = jest.spyOn(NullLog, 'verbose');
    runQuerySpy = jest.spyOn(warehouse, 'runQuery');
  });

  afterEach(() => {
    warehouse.runQuery = runQueryOriginal;
    mockDb.unmock(knex);
  });

  it('registers a retreat', async () => {
    const registerQuery = knex('retreats').insert(event);

    await warehouse.registerRetreat(event);

    expect(withFieldsSpy).toHaveBeenCalledWith({ event });
    expect(verboseSpy).toHaveBeenCalledWith('Registering retreat.');
    expect(runQuerySpy).toHaveBeenCalledWith(registerQuery);
  });

  it('gets a retreat by retreatId', async () => {
    warehouse.runQuery = jest.fn().mockReturnValue(Bluebird.resolve(event));
    const spy = jest.spyOn(warehouse, 'runQuery');
    const query = knex('retreats')
      .where({ id: event.id })
      .first(['id', 'name', 'phase']);

    const result = await warehouse.getRetreatByRetreatId(event.id);

    expect(result).toMatchObject(event);
    expect(withFieldsSpy).toHaveBeenCalledWith({ retreatId: event.id });
    expect(verboseSpy).toHaveBeenCalledWith('Getting retreat by id.');
    expect(spy).toHaveBeenCalledWith(query);
  });

  it('returns null if no retreat', async () => {
    warehouse.runQuery = jest.fn().mockReturnValue(Bluebird.resolve());

    const result = await warehouse.getRetreatByRetreatId(event.id);

    expect(result).toBeUndefined();
  });
});
