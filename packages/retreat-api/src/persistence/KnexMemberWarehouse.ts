import { Base, IOTypes } from '@penguinhouse/savannah-io';
import { ILogger } from 'stoolie';
import Bluebird from 'bluebird';
import Knex from 'knex';
import * as t from 'io-ts';
import KnexRepositoryBase from './KnexRepositoryBase';
import { IMemberRepository, IMemberWarehouse } from './interfaces';
import { Member } from '../types';

const PgMember = t.type({
  id: t.string,
  display_name: t.string,
  email: t.string,
  points: t.number,
});

const MemberTransformers = {
  fromPgMember(pgMember: t.TypeOf<typeof PgMember>): Member {
    return {
      id: pgMember.id,
      displayName: pgMember.display_name,
      email: pgMember.email,
      points: pgMember.points,
    };
  },

  toPgMember(member: Member | IOTypes.MemberRegistered): t.TypeOf<typeof PgMember> {
    return {
      id: member.id,
      display_name: member.displayName,
      email: member.email,
      points: member.points,
    };
  },
};

function parseMemberIfExists(pgMember: Base.Nullable<t.TypeOf<typeof PgMember>>) {
  if (!pgMember) {
    return null;
  }

  return MemberTransformers.fromPgMember(pgMember);
}

function appendRetreatFilterToQuery<T>(
  retreatId: Base.Nullable<string>,
  builder: Knex.QueryBuilder<T>
) {
  return retreatId
    ? builder
        .leftJoin('retreat_member_xref', 'members.id', 'retreat_member_xref.member_id')
        .where({ retreat_id: retreatId })
    : builder;
}

export default class KnexMemberWarehouse extends KnexRepositoryBase<t.InterfaceType<any>>
  implements IMemberWarehouse, IMemberRepository {
  entry: ILogger;

  constructor(knex: Knex, logger: ILogger) {
    super(knex, logger, PgMember);

    this.entry = logger.withCategory('warehouse').withFields({ type: 'member-warehouse' });
  }

  registerMember(event: IOTypes.MemberRegistered) {
    this.entry.withFields({ event }).verbose('Registering member by id.');

    const pgMember = MemberTransformers.toPgMember(event);

    const query = this.knex('members').insert(pgMember);
    const xrefQuery = this.knex('retreat_member_xref').insert({
      retreat_id: event.retreatId,
      member_id: event.id,
    });

    return this.runQuery(query)
      .then(() => this.runQuery(xrefQuery))
      .thenReturn();
  }

  setPoints(event: IOTypes.VoteCast): Bluebird<void> {
    this.entry.withFields({ event }).verbose('Setting points for member.');

    const query = this.knex('members')
      .where({ id: event.memberId })
      .update({ points: event.memberPoints });

    return this.runQuery(query).thenReturn();
  }

  getMemberByMemberId(memberId: string): Bluebird<Base.Nullable<Member>> {
    this.entry.withFields({ memberId }).verbose('Getting member by id.');

    const query = this.knex('members')
      .where({ id: memberId })
      .first(this.columns);

    return this.runQuery(query).then(parseMemberIfExists);
  }

  getMembersByRetreatId(retreatId: Base.Nullable<string>): Bluebird<Member[]> {
    this.entry.withFields({ retreatId }).verbose('Getting member by retreat id.');

    const query = this.knex('members').select(this.columns);

    const retreatWrapper = appendRetreatFilterToQuery(retreatId, query);

    return this.runQuery(retreatWrapper).map(MemberTransformers.fromPgMember);
  }
}
