import Knex from 'knex';
import mockDb from 'mock-knex';
import KnexMemberWarehouse from './KnexMemberWarehouse';
import { NullLog } from 'stoolie';
import Bluebird from 'bluebird';

describe('KnexMemberWarehouse', () => {
  let warehouse: KnexMemberWarehouse;
  let runQueryOriginal;
  let withFieldsSpy;
  let verboseSpy;
  let runQuerySpy;

  const knex = Knex({ client: 'pg' });
  const event = {
    id: '0',
    displayName: 'test',
    email: 'test@gmail.com',
    points: 10,
    retreatId: '1',
  };

  const pgMember = {
    id: '0',
    display_name: 'test',
    email: 'test@gmail.com',
    points: 10,
  };

  const member = {
    id: pgMember.id,
    displayName: pgMember.display_name,
    email: pgMember.email,
    points: pgMember.points,
  };

  beforeEach(() => {
    mockDb.mock(knex);
    warehouse = new KnexMemberWarehouse(knex, NullLog);
    runQueryOriginal = warehouse.runQuery;
    withFieldsSpy = jest.spyOn(NullLog, 'withFields');
    verboseSpy = jest.spyOn(NullLog, 'verbose');
    runQuerySpy = jest.spyOn(warehouse, 'runQuery');
  });

  afterEach(() => {
    warehouse.runQuery = runQueryOriginal;
    mockDb.unmock(knex);
  });

  it('registers a member', async () => {
    const registerQuery = knex('members').insert(pgMember);
    const xrefQuery = knex('retreat_member_xref').insert({
      retreat_id: event.retreatId,
      member_id: event.id,
    });

    await warehouse.registerMember(event);

    expect(withFieldsSpy).toHaveBeenCalledWith({ event });
    expect(verboseSpy).toHaveBeenCalledWith('Registering member by id.');
    expect(runQuerySpy).toHaveBeenCalledTimes(2);
    expect(runQuerySpy).toHaveBeenCalledWith(registerQuery);
    expect(runQuerySpy).toHaveBeenCalledWith(xrefQuery);
  });

  it('set points on a member', async () => {
    const voteCast = {
      id: '0',
      proposalId: '0',
      memberId: '0',
      retreatId: '0',
      points: 5,
      memberPoints: 10,
    };

    const query = knex('members')
      .where({ id: voteCast.memberId })
      .update({ points: voteCast.memberPoints });

    await warehouse.setPoints(voteCast);

    expect(withFieldsSpy).toHaveBeenCalledWith({ event: voteCast });
    expect(verboseSpy).toHaveBeenCalledWith('Setting points for member.');
    expect(runQuerySpy).toHaveBeenCalledWith(query);
  });

  it('gets a member by memberId', async () => {
    warehouse.runQuery = jest.fn().mockReturnValue(Bluebird.resolve(pgMember));
    const spy = jest.spyOn(warehouse, 'runQuery');
    const query = knex('members')
      .where({ id: pgMember.id })
      .first(['id', 'display_name', 'email', 'points']);

    const result = await warehouse.getMemberByMemberId(pgMember.id);

    expect(result).toMatchObject(member);
    expect(withFieldsSpy).toHaveBeenCalledWith({ memberId: pgMember.id });
    expect(verboseSpy).toHaveBeenCalledWith('Getting member by id.');
    expect(spy).toHaveBeenCalledWith(query);
  });

  it('returns null if no member', async () => {
    warehouse.runQuery = jest.fn().mockReturnValue(Bluebird.resolve());

    const result = await warehouse.getMemberByMemberId(member.id);

    expect(result).toBeNull();
  });

  it('gets members by retreatId', async () => {
    warehouse.runQuery = jest.fn().mockReturnValue(Bluebird.resolve([pgMember, pgMember]));
    const spy = jest.spyOn(warehouse, 'runQuery');
    const queryWithRetreat = knex('members')
      .select(['id', 'display_name', 'email', 'points'])
      .leftJoin('retreat_member_xref', 'members.id', 'retreat_member_xref.member_id')
      .where({ retreat_id: '0' });

    const result = await warehouse.getMembersByRetreatId('0');

    expect(result).toMatchObject([member, member]);
    expect(withFieldsSpy).toHaveBeenCalledWith({ retreatId: '0' });
    expect(verboseSpy).toHaveBeenCalledWith('Getting member by retreat id.');
    expect(spy).toHaveBeenCalledWith(queryWithRetreat);
  });

  it('returns an empty array if no member by retreatId', async () => {
    warehouse.runQuery = jest.fn().mockReturnValue(Bluebird.resolve([]));
    const spy = jest.spyOn(warehouse, 'runQuery');

    const queryWithoutRetreat = knex('members').select(['id', 'display_name', 'email', 'points']);

    const result = await warehouse.getMembersByRetreatId(null);

    expect(result).toMatchObject([]);
    expect(withFieldsSpy).toHaveBeenCalledWith({ retreatId: null });
    expect(verboseSpy).toHaveBeenCalledWith('Getting member by retreat id.');
    expect(spy).toHaveBeenCalledWith(queryWithoutRetreat);
  });
});
