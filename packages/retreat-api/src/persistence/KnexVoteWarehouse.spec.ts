import Knex from 'knex';
import mockDb from 'mock-knex';
import KnexVoteWarehouse from './KnexVoteWarehouse';
import { NullLog } from 'stoolie';
import Bluebird from 'bluebird';

describe('KnexVoteWarehouse', () => {
  let warehouse: KnexVoteWarehouse;
  let runQueryOriginal;
  let withFieldsSpy;
  let verboseSpy;
  let runQuerySpy;

  const knex = Knex({ client: 'pg' });
  const pgVote = {
    id: '0',
    proposal_id: '0',
    member_id: '0',
    points: 4,
    retreat_id: '0',
  };
  const vote = {
    id: pgVote.id,
    proposalId: pgVote.proposal_id,
    memberId: pgVote.member_id,
    points: pgVote.points,
    retreatId: pgVote.retreat_id,
  };
  const columns = ['id', 'proposal_id', 'member_id', 'points', 'retreat_id'];

  beforeEach(() => {
    mockDb.mock(knex);
    warehouse = new KnexVoteWarehouse(knex, NullLog);
    runQueryOriginal = warehouse.runQuery;
    withFieldsSpy = jest.spyOn(NullLog, 'withFields');
    verboseSpy = jest.spyOn(NullLog, 'verbose');
    runQuerySpy = jest.spyOn(warehouse, 'runQuery');
  });

  afterEach(() => {
    warehouse.runQuery = runQueryOriginal;
    mockDb.unmock(knex);
  });

  it('adds a vote', async () => {
    const event = { ...vote, memberPoints: 6 };
    const query = knex('votes').insert(pgVote);

    await warehouse.addVote(event);

    expect(withFieldsSpy).toHaveBeenCalledWith({ event });
    expect(verboseSpy).toHaveBeenCalledWith('Adding vote to member.');
    expect(runQuerySpy).toHaveBeenCalledWith(query);
  });

  it('gets vote by id', async () => {
    warehouse.runQuery = jest.fn().mockReturnValue(Bluebird.resolve(pgVote));
    const spy = jest.spyOn(warehouse, 'runQuery');
    const query = knex('votes')
      .where({ id: pgVote.id })
      .first(columns);

    const result = await warehouse.getVoteByVoteId(pgVote.id);

    expect(result).toMatchObject(vote);
    expect(verboseSpy).toHaveBeenCalledWith('Getting vote by id.');
    expect(withFieldsSpy).toHaveBeenCalledWith({ voteId: pgVote.id });
    expect(spy).toHaveBeenCalledWith(query);
  });

  it('gets votes by member id', async () => {
    warehouse.runQuery = jest.fn().mockReturnValue(Bluebird.resolve([pgVote, pgVote]));
    const spy = jest.spyOn(warehouse, 'runQuery');
    const query = knex('votes')
      .where({ member_id: pgVote.member_id })
      .select(columns);

    const result = await warehouse.getVotesByMemberId(pgVote.member_id);

    expect(result).toMatchObject([vote, vote]);
    expect(verboseSpy).toHaveBeenCalledWith('Getting votes by member id.');
    expect(withFieldsSpy).toHaveBeenCalledWith({ memberId: pgVote.member_id });
    expect(spy).toHaveBeenCalledWith(query);
  });
});
