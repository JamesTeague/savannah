import { Base, IOTypes } from '@penguinhouse/savannah-io';
import Knex from 'knex';
import { ILogger } from 'stoolie';
import * as t from 'io-ts';
import { ICityRepository, ICityWarehouse } from './interfaces';
import KnexRepositoryBase from './KnexRepositoryBase';
import { City } from '../types';

const PgCity = t.type({
  id: t.string,
  address: t.string,
  country: t.string,
  name: t.string,
  state: t.string,
  url: t.string,
  center: t.array(t.number),
});

export default class KnexCityWarehouse extends KnexRepositoryBase<t.InterfaceType<any>>
  implements ICityRepository, ICityWarehouse {
  entry: ILogger;

  constructor(knex: Knex, logger: ILogger) {
    super(knex, logger, PgCity);

    this.entry = logger.withCategory('warehouse').withFields({ type: 'city-warehouse' });
  }

  addCity(city: IOTypes.City) {
    this.entry.withFields({ city }).verbose('Adding city.');

    const query = this.knex('cities').insert({
      ...city,
      center: JSON.stringify(city.center),
    });

    return this.runQuery(query).thenReturn();
  }

  getCityByCityId(cityId: string) {
    this.entry.withFields({ cityId }).verbose('Getting city by id.');

    const query = this.knex('cities')
      .where({ id: cityId })
      .first(this.columns);

    return this.runQuery(query) as Promise<Base.Nullable<City>>;
  }
}
