import { ILogger } from 'stoolie';
import { Base, IOTypes } from '@penguinhouse/savannah-io';
import Bluebird from 'bluebird';
import Knex from 'knex';
import * as t from 'io-ts';
import KnexRepositoryBase from './KnexRepositoryBase';
import { IVoteRepository, IVoteWarehouse } from './interfaces';
import { Vote } from '../types';

const PgVote = t.type({
  id: t.string,
  proposal_id: t.string,
  member_id: t.string,
  points: t.number,
  retreat_id: t.string,
});

const VoteTransformers = {
  toPgVote(vote: Vote): t.TypeOf<typeof PgVote> {
    return {
      id: vote.id,
      proposal_id: vote.proposalId,
      member_id: vote.memberId,
      retreat_id: vote.retreatId,
      points: vote.points,
    };
  },
  fromPgVote(pgVote: t.TypeOf<typeof PgVote>): Vote {
    return {
      id: pgVote.id,
      proposalId: pgVote.proposal_id,
      memberId: pgVote.member_id,
      retreatId: pgVote.retreat_id,
      points: pgVote.points,
    };
  },
  fromVoteCastEvent(event: IOTypes.VoteCast): t.TypeOf<typeof PgVote> {
    return {
      id: event.id,
      proposal_id: event.proposalId,
      member_id: event.memberId,
      retreat_id: event.retreatId,
      points: event.points,
    };
  },
};

function parseVoteIfExists(pgVote: Base.Nullable<t.TypeOf<typeof PgVote>>) {
  if (!pgVote) {
    return null;
  }

  return VoteTransformers.fromPgVote(pgVote);
}

export default class KnexVoteWarehouse extends KnexRepositoryBase<t.InterfaceType<any>>
  implements IVoteRepository, IVoteWarehouse {
  entry: ILogger;

  constructor(knex: Knex, logger: ILogger) {
    super(knex, logger, PgVote);

    this.entry = logger.withCategory('warehouse').withFields({ type: 'vote-warehouse' });
  }

  addVote(event: IOTypes.VoteCast) {
    this.entry.withFields({ event }).verbose('Adding vote to member.');

    const pgVote = VoteTransformers.fromVoteCastEvent(event);

    const query = this.knex('votes').insert(pgVote);

    return this.runQuery(query).thenReturn();
  }

  getVoteByVoteId(voteId: string) {
    this.entry.withFields({ voteId }).verbose('Getting vote by id.');

    const query = this.knex('votes')
      .where({ id: voteId })
      .first(this.columns);

    return this.runQuery(query).then(parseVoteIfExists);
  }

  getVotesByMemberId(memberId: string): Bluebird<Vote[]> {
    this.entry.withFields({ memberId }).verbose('Getting votes by member id.');

    const query = this.knex('votes')
      .where({ member_id: memberId })
      .select(this.columns);

    return this.runQuery(query).map(VoteTransformers.fromPgVote);
  }
}
