import { City } from '../../types';
import { Base } from '@penguinhouse/savannah-io';

export interface ICityWarehouse {
  getCityByCityId(cityId: string): Promise<Base.Nullable<City>>;
}
