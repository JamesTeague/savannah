import { Base } from '@penguinhouse/savannah-io';
import { Member } from '../../types';

export interface IMemberWarehouse {
  getMemberByMemberId(memberId: string): Promise<Base.Nullable<Member>>;
  getMembersByRetreatId(retreatId: Base.Nullable<string>): Promise<Member[]>;
}
