import { Proposal } from '../../types';
import { Base } from '@penguinhouse/savannah-io';

export interface IProposalWarehouse {
  getProposalByProposalId(proposalId: string): Promise<Base.Nullable<Proposal>>;
  getProposalsByMemberId(memberId: string): Promise<Proposal[]>;
  getProposalsByRetreatId(retreatId: string): Promise<Proposal[]>;
}
