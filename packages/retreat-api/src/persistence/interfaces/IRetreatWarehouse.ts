import { Base } from '@penguinhouse/savannah-io';
import { Retreat } from '../../types';

export interface IRetreatWarehouse {
  getRetreatByRetreatId(retreatId: string): Promise<Base.Nullable<Retreat>>;
}
