import { IOTypes } from '@penguinhouse/savannah-io';

export interface IRetreatRepository {
  registerRetreat(event: IOTypes.RetreatRegistered): Promise<void>;
}
