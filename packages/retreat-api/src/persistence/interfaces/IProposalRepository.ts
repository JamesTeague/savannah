import { IOTypes } from '@penguinhouse/savannah-io';

export interface IProposalRepository {
  addProposal(event: IOTypes.ProposalMade): Promise<void>;
}
