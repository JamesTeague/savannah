import { IOTypes } from '@penguinhouse/savannah-io';

export interface ICityRepository {
  addCity(city: IOTypes.City): Promise<void>;
}
