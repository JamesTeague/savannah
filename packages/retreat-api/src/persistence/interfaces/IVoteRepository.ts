import { IOTypes } from '@penguinhouse/savannah-io';

export interface IVoteRepository {
  addVote(event: IOTypes.VoteCast): Promise<void>;
}
