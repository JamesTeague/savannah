import { Vote } from '../../types';
import { Base } from '@penguinhouse/savannah-io';

export interface IVoteWarehouse {
  getVoteByVoteId(voteId: string): Promise<Base.Nullable<Vote>>;
  getVotesByMemberId(memberId: string): Promise<Vote[]>;
}
