import { IOTypes } from '@penguinhouse/savannah-io';

export interface IMemberRepository {
  registerMember(event: IOTypes.MemberRegistered): Promise<void>;
  setPoints(event: IOTypes.VoteCast): Promise<void>;
}
