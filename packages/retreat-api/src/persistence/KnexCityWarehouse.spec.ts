import Knex from 'knex';
import mockDb from 'mock-knex';
import KnexCityWarehouse from './KnexCityWarehouse';
import { NullLog } from 'stoolie';
import cities from '../knex/resources/cities';

describe('KnexCityWarehouse', () => {
  let warehouse;
  let withFieldsSpy;
  let verboseSpy;
  let runQuerySpy;

  const knex = Knex({ client: 'pg' });

  beforeEach(() => {
    mockDb.mock(knex);
    warehouse = new KnexCityWarehouse(knex, NullLog);
    withFieldsSpy = jest.spyOn(NullLog, 'withFields');
    verboseSpy = jest.spyOn(NullLog, 'verbose');
    runQuerySpy = jest.spyOn(warehouse, 'runQuery');
  });

  afterEach(() => {
    mockDb.unmock(knex);
  });

  it('adds a city', async () => {
    const query = knex('cities').insert({
      ...cities[0],
      center: JSON.stringify(cities[0].center),
    });

    await warehouse.addCity(cities[0]);

    expect(withFieldsSpy).toHaveBeenCalledWith({ city: cities[0] });
    expect(verboseSpy).toHaveBeenCalledWith('Adding city.');
    expect(runQuerySpy).toHaveBeenCalledWith(query);
  });

  it('gets a city by id', async () => {
    const query = knex('cities')
      .where({ id: cities[0].id })
      .first(['id', 'address', 'country', 'name', 'state', 'url', 'center']);

    await warehouse.getCityByCityId(cities[0].id);

    expect(withFieldsSpy).toHaveBeenCalledWith({ cityId: cities[0].id });
    expect(verboseSpy).toHaveBeenCalledWith('Getting city by id.');
    expect(runQuerySpy).toHaveBeenCalledWith(query);
  });
});
