import KnexRepositoryBase from './KnexRepositoryBase';
import Knex from 'knex';
import mockDb from 'mock-knex';
import * as t from 'io-ts';
import { NullLog } from 'stoolie';

describe('KnexRepositoryBase', () => {
  const knex = Knex({ client: 'pg' });
  const pgVote = {
    id: '0',
    proposal_id: '0',
    member_id: '0',
    points: 4,
    retreat_id: '0',
  };
  const addQuery = knex('votes').insert(pgVote);

  const repositoryBase = new KnexRepositoryBase(knex, NullLog, t.type({}));
  let withFieldsSpy;
  let debugSpy;

  beforeEach(() => {
    mockDb.mock(knex);
    withFieldsSpy = jest.spyOn(NullLog, 'withFields');
    debugSpy = jest.spyOn(NullLog, 'debug');
  });

  afterEach(() => {
    mockDb.unmock(knex);
  });

  it('runs given query', async () => {
    const queryString = addQuery.toString();

    await repositoryBase.runQuery(addQuery);

    expect(withFieldsSpy).toHaveBeenCalledWith({ query: queryString });
    expect(debugSpy).toHaveBeenCalledWith('Running query.');
    expect(debugSpy).toHaveBeenCalledWith('Query result.');
  });

  it('gets first or default input', async () => {
    const result = await repositoryBase.firstOrDefault([1, 2, 3]);
    const result2 = await repositoryBase.firstOrDefault([]);

    expect(result).toBe(1);
    expect(result2).toBeNull();
  });

  it('checks results are deterministic', async () => {
    const spy = jest.spyOn(repositoryBase, 'firstOrDefault');
    const result = await repositoryBase.checkResultsDeterministicOrThrow([1]);

    expect(result).toBe(1);
    expect(spy).toHaveBeenCalledWith([1]);
    expect(() => {
      repositoryBase.checkResultsDeterministicOrThrow([1, 2]);
    }).toThrow('Non-deterministic find detected.');
  });
});
