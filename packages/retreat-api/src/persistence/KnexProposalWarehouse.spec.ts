import Knex from 'knex';
import mockDb from 'mock-knex';
import cities from '../knex/resources/cities';
import { NullLog } from 'stoolie';
import KnexProposalWarehouse from './KnexProposalWarehouse';
import Bluebird from 'bluebird';

describe('KnexProposalWarehouse', () => {
  let warehouse: KnexProposalWarehouse;
  let runQueryOriginal;
  let withFieldsSpy;
  let verboseSpy;
  let runQuerySpy;

  const knex = Knex({ client: 'pg' });
  const pgProposal = {
    id: '0',
    member_id: '0',
    city_id: cities[0].id,
    retreat_id: '0',
  };
  const proposal = {
    id: pgProposal.id,
    memberId: pgProposal.member_id,
    cityId: pgProposal.city_id,
    retreatId: pgProposal.retreat_id,
  };
  const columns = ['id', 'member_id', 'city_id', 'retreat_id'];

  beforeEach(() => {
    mockDb.mock(knex);
    warehouse = new KnexProposalWarehouse(knex, NullLog);
    runQueryOriginal = warehouse.runQuery;
    withFieldsSpy = jest.spyOn(NullLog, 'withFields');
    verboseSpy = jest.spyOn(NullLog, 'verbose');
    runQuerySpy = jest.spyOn(warehouse, 'runQuery');
  });

  afterEach(() => {
    warehouse.runQuery = runQueryOriginal;
    mockDb.unmock(knex);
  });

  it('adds a proposal', async () => {
    const event = {
      id: '0',
      city: cities[0],
      memberId: '0',
      retreatId: '0',
    };
    const query = knex('proposals').insert(pgProposal);

    await warehouse.addProposal(event);

    expect(verboseSpy).toHaveBeenCalledWith('Adding proposal.');
    expect(withFieldsSpy).toHaveBeenCalledWith({ event });
    expect(runQuerySpy).toHaveBeenCalledWith(query);
  });

  it('gets proposal by id', async () => {
    warehouse.runQuery = jest.fn().mockReturnValue(Bluebird.resolve(pgProposal));
    const spy = jest.spyOn(warehouse, 'runQuery');
    const query = knex('proposals')
      .where({ id: pgProposal.id })
      .first(columns);

    const result = await warehouse.getProposalByProposalId(pgProposal.id);

    expect(result).toMatchObject(proposal);
    expect(verboseSpy).toHaveBeenCalledWith('Getting proposal by id.');
    expect(withFieldsSpy).toHaveBeenCalledWith({ proposalId: '0' });
    expect(spy).toHaveBeenCalledWith(query);
  });

  it('gets proposals by member id', async () => {
    warehouse.runQuery = jest.fn().mockReturnValue(Bluebird.resolve([pgProposal, pgProposal]));
    const spy = jest.spyOn(warehouse, 'runQuery');
    const query = knex('proposals')
      .where({ member_id: pgProposal.member_id })
      .select(columns);

    const result = await warehouse.getProposalsByMemberId(pgProposal.member_id);

    expect(result).toMatchObject([proposal, proposal]);
    expect(verboseSpy).toHaveBeenCalledWith('Getting proposals by member id.');
    expect(withFieldsSpy).toHaveBeenCalledWith({ memberId: pgProposal.member_id });
    expect(spy).toHaveBeenCalledWith(query);
  });

  it('gets proposals by retreat id', async () => {
    warehouse.runQuery = jest.fn().mockReturnValue(Bluebird.resolve([pgProposal, pgProposal]));
    const spy = jest.spyOn(warehouse, 'runQuery');
    const getProposalByRetreatIdQuery = knex('proposals')
      .select(columns)
      .where({ retreat_id: pgProposal.retreat_id });

    const result = await warehouse.getProposalsByRetreatId(pgProposal.retreat_id);

    expect(result).toMatchObject([proposal, proposal]);
    expect(verboseSpy).toHaveBeenCalledWith('Getting proposals by retreat id.');
    expect(withFieldsSpy).toHaveBeenCalledWith({
      retreatId: pgProposal.retreat_id,
    });
    expect(spy).toHaveBeenCalledWith(getProposalByRetreatIdQuery);
  });

  it('return empty array if no proposals by retreat id', async () => {
    warehouse.runQuery = jest.fn().mockReturnValue(Bluebird.resolve([]));
    const spy = jest.spyOn(warehouse, 'runQuery');
    const query = knex('proposals').select(columns);

    const result = await warehouse.getProposalsByRetreatId(null);

    expect(result).toMatchObject([]);
    expect(withFieldsSpy).toHaveBeenCalledWith({ retreatId: null });
    expect(spy).toHaveBeenCalledWith(query);
  });
});
