export type Proposal = {
  id: string;
  memberId: string;
  cityId: string;
  retreatId: string;
};
