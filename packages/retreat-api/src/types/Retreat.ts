import { IOTypes } from '@penguinhouse/savannah-io';

export type Retreat = {
  id: string;
  name: string;
  phase: IOTypes.Phase;
};
