export type Vote = {
  id: string;
  memberId: string;
  proposalId: string;
  points: number;
  retreatId: string;
};
