import { IOTypes } from '@penguinhouse/savannah-io';

export type City = {
  id: string;
  address: string;
  country: IOTypes.Location;
  name: string;
  state: IOTypes.Location;
  url: string;
  center: number[];
};
