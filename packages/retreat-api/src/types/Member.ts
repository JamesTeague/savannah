export type Member = {
  id: string;
  displayName: string;
  email: string;
  points: number;
};
