import { SavannahPlatform, User } from '@penguinhouse/retreat-es';
import stoolie from 'stoolie';
import { ApolloServer, PubSub } from 'apollo-server';
import Knex from 'knex';
import * as Path from 'path';
import * as Rx from 'rxjs';
import * as RxOp from 'rxjs/operators';
import { ApolloServerContext, SavannahContext } from './Application';
import ConfigureApplication from './ApplicationConfiguration';
import typeDefs from './gql/typeDefs';
import resolvers from './gql/resolvers';
import {
  KnexCityWarehouse,
  KnexMemberWarehouse,
  KnexProposalWarehouse,
  KnexVoteWarehouse,
} from './persistence';
import ConfigureSavannahTransport from './transport';
import KnexRetreatWarehouse from './persistence/KnexRetreatWarehouse';

const config = ConfigureApplication(process.env);

/** Knex */
const KNEX_DIR = Path.resolve(__dirname, 'knex');
const knex = Knex({
  client: 'pg',
  connection: config.db,
  migrations: {
    directory: Path.resolve(KNEX_DIR, 'migrations'),
  },
  seeds: {
    directory: Path.resolve(KNEX_DIR, 'seeds'),
  },
});

let transport;
let apolloServer;
const logger = stoolie('retreat-api', config.logLevel);
const apolloContextLogger = logger.withCategory('apollo');
const savannah = new SavannahPlatform({
  appName: 'retreat-api',
  ...config.savannah,
  logLevel: config.logLevel,
});
const memberWarehouse = new KnexMemberWarehouse(knex, logger);
const proposalWarehouse = new KnexProposalWarehouse(knex, logger);
const cityWarehouse = new KnexCityWarehouse(knex, logger);
const voteWarehouse = new KnexVoteWarehouse(knex, logger);
const retreatWarehouse = new KnexRetreatWarehouse(knex, logger);
const pubSub = new PubSub();

const context = (): ApolloServerContext => ({
  logger: apolloContextLogger,
  memberWarehouse,
  proposalWarehouse,
  cityWarehouse,
  retreatWarehouse,
  pubSub,
  voteWarehouse,
  // TODO - Use real user
  user: User.dummy(),
});

const main = (): Rx.Observable<any> => {
  const state: SavannahContext = {
    memberRepository: memberWarehouse,
    memberWarehouse,
    proposalRepository: proposalWarehouse,
    proposalWarehouse,
    cityRepository: cityWarehouse,
    cityWarehouse,
    retreatRepository: retreatWarehouse,
    retreatWarehouse,
    voteRepository: voteWarehouse,
    voteWarehouse,
    pubSub,
  };

  savannah.connectNotifier().then(success => {
    if (!success) {
      throw new Error('Failed to connect to platform.');
    }

    transport = ConfigureSavannahTransport({
      type: 'savannah',
      context: state,
      connection: { savannah },
    });

    const apollo = new ApolloServer({
      typeDefs,
      resolvers,
      context,
      cors: {
        origin: 'http://localhost:3000',
        credentials: true,
      },
      cacheControl: {
        defaultMaxAge: 120,
      },
      subscriptions: {
        onConnect() {
          // if (!connectionParams.authToken) {
          //   throw new Error('No authentication data found.');
          // }
          logger.debug('connecting to subscriptions');
          return new Promise(resolve => {
            return resolve(User.dummy());
            // jwt.verify(
            //   connectionParams.authToken,
            //   getKey,
            //   {
            //     audience: config.api.jwtOptions.audience,
            //     algorithms: ['RS256', 'RS384', 'RS512'],
            //   },
            //   (err, decoded) => {
            //     if (err) {
            //       return reject(err);
            //     }
            //
            //     return resolve(decoded);
            //   },
            // );
          })
            .then(user => {
              // const user = userParser(decoded);

              return {
                user,
                ...state,
              };
            })
            .catch((error: Error) => {
              logger.withError(error).error('Failed to authenticate websocket connection');

              throw error;
            });
        },
      },
    });

    savannah.connect();

    apollo.listen(config.api.port).then(({ server, url, subscriptionsUrl }) => {
      logger.info(`Server ready at ${url}`);
      logger.info(`Subscriptions ready at ${subscriptionsUrl}`);
      apolloServer = server;
    });
  });

  return Rx.NEVER;
};

const exit = new Rx.Subject();
['SIGINT', 'SIGTERM', 'SIGHUP'].forEach((signal: any) => {
  process.on(signal, () => exit.next(true));
});

const cleanup = () => {
  return savannah.disconnect().then(() => {
    transport.unbind();
    apolloServer.close();
    logger.info('Platform disconnected and server closed.');
  });
};

Rx.of(knex.migrate.latest())
  .pipe(
    RxOp.mergeMap(() => main()),
    RxOp.takeUntil(exit)
  )
  .subscribe({
    next() {},
    error(error: Error) {
      cleanup().then(() => {
        logger.withError(error).error('Fatal application error.');
        process.exit(1);
      });
    },
    complete() {
      cleanup().then(() => {
        logger.info('Application exited normally.');
        process.exit(0);
      });
    },
  });
