import Bluebird from 'bluebird';
import { GraphQLDate, GraphQLTime, GraphQLDateTime } from 'graphql-iso-date';
import { withFilter } from 'apollo-server-express';
import GraphQLJSON from 'graphql-type-json';
import { ApolloServerContext } from '../../Application';
import { Member, Proposal, Retreat, Vote } from '../../types';
import { Base, IOTypes } from '@penguinhouse/savannah-io';

const resolvers = {
  Date: GraphQLDate,
  Time: GraphQLTime,
  DateTime: GraphQLDateTime,
  JSON: GraphQLJSON,
  RootQuery: {
    getMemberByMemberId(
      _: any,
      args: { memberId: string },
      context: ApolloServerContext
    ): Bluebird<Base.Nullable<Member>> {
      return Bluebird.try(() => {
        const { memberId } = args;

        const {
          logger,
          // user,
        } = context;

        const entry = logger
          .withCategory('get-member-by-id-resolver')
          .withFields({ memberId /*, userId: user.id*/ });

        entry.info('Requested member by ID.');
        // if (!user.can('read:client')) {
        //   const error = new ForbiddenError('User does not have access rights.');
        //
        //   entry.withError(error)
        //     .error('Failed to read member by ID.');
        //
        //   throw error;
        // }

        return context.memberWarehouse.getMemberByMemberId(memberId).then(member => {
          if (!member) {
            return null;
          }

          // if (!user.is('client-administrator')
          // && user.clientId && user.clientId !== client.id) {
          //   const error = new ForbiddenError('User cannot access client object.');
          //
          //   entry.withError(error)
          //     .error('Failed to resolve client object for user.');
          //
          //   throw error;
          // }

          return member;
        });
      }).catch((error: Error) => {
        const { logger } = context;

        logger.withError(error).error('Resolver failed');

        throw error;
      });
    },

    getMembersByRetreatId(
      _: any,
      args: { retreatId: string },
      context: ApolloServerContext
    ): Bluebird<Member[]> {
      return Bluebird.try(() => {
        const { retreatId } = args;

        const {
          logger,
          // user,
          memberWarehouse,
        } = context;

        const entry = logger
          .withCategory('get-member-by-retreat-id-resolver')
          .withFields({ retreatId /*, userId: user.id*/ });

        entry.info('Requested members by retreat ID.');

        return memberWarehouse.getMembersByRetreatId(retreatId);
      }).catch((error: Error) => {
        const { logger } = context;

        logger.withError(error).error('Resolver failed');

        throw error;
      });
    },

    getProposalByProposalId(
      _: any,
      args: { proposalId: string },
      context: ApolloServerContext
    ): Bluebird<Base.Nullable<Proposal>> {
      return Bluebird.try(() => {
        const { proposalId } = args;

        const {
          logger,
          // user,
          proposalWarehouse,
        } = context;

        const entry = logger
          .withCategory('get-proposal-by-proposal-id-resolver')
          .withFields({ proposalId /*, userId: user.id*/ });

        entry.info('Requested proposal by proposal ID.');

        return proposalWarehouse.getProposalByProposalId(proposalId);
      }).catch((error: Error) => {
        const { logger } = context;

        logger.withError(error).error('Resolver failed');

        throw error;
      });
    },

    getProposalsByRetreatId(
      _: any,
      args: { retreatId: string },
      context: ApolloServerContext
    ): Promise<Proposal[]> {
      return Bluebird.try(() => {
        const { retreatId } = args;

        const {
          logger,
          // user,
          proposalWarehouse,
        } = context;

        const entry = logger
          .withCategory('get-proposal-by-retreat-id-resolver')
          .withFields({ retreatId /*, userId: user.id*/ });

        entry.info('Requested proposals by retreat ID.');

        return proposalWarehouse.getProposalsByRetreatId(retreatId);
      }).catch((error: Error) => {
        const { logger } = context;

        logger.withError(error).error('Resolver failed');

        throw error;
      });
    },

    getRetreatByRetreatId(
      _: any,
      args: { retreatId: string },
      context: ApolloServerContext
    ): Promise<Retreat> {
      return Bluebird.try(() => {
        const { retreatId } = args;

        const {
          logger,
          //user,
          retreatWarehouse,
        } = context;

        const entry = logger
          .withCategory('get-proposal-by-retreat-id-resolver')
          .withFields({ retreatId /*, userId: user.id*/ });

        entry.info('Requested retreat by retreat ID.');

        return retreatWarehouse.getRetreatByRetreatId(retreatId);
      }).catch(error => {
        const { logger } = context;

        logger.withError(error).error('Resolver failed');

        throw error;
      });
    },

    getVoteByVoteId(
      _: any,
      args: { voteId: string },
      context: ApolloServerContext
    ): Bluebird<Base.Nullable<Vote>> {
      return Bluebird.try(() => {
        const { voteId } = args;

        const { logger, user, voteWarehouse } = context;

        const entry = logger
          .withCategory('get-vote-by-vote-id-resolver')
          .withFields({ voteId, userId: user.id });

        entry.info('Requested vote by vote ID.');

        return voteWarehouse.getVoteByVoteId(voteId);
      }).catch((error: Error) => {
        const { logger } = context;

        logger.withError(error).error('Resolver failed');

        throw error;
      });
    },

    getVotesByMemberId(
      _: any,
      args: { memberId: string },
      context: ApolloServerContext
    ): Bluebird<Vote[]> {
      return Bluebird.try(() => {
        const { memberId } = args;

        const {
          logger,
          // user,
          voteWarehouse,
        } = context;

        const entry = logger
          .withCategory('get-vote-by-member-id-resolver')
          .withFields({ memberId /*, userId: user.id*/ });

        entry.info('Requested votes by member ID.');

        return voteWarehouse.getVotesByMemberId(memberId);
      }).catch((error: Error) => {
        const { logger } = context;

        logger.withError(error).error('Resolver failed');

        throw error;
      });
    },
  },
  Member: {
    proposals(root: Member, _: any, context: ApolloServerContext) {
      return context.proposalWarehouse.getProposalsByMemberId(root.id);
    },
    votes(root: Member, _: any, context: ApolloServerContext) {
      return context.voteWarehouse.getVotesByMemberId(root.id);
    },
  },
  Proposal: {
    city(root: Proposal, _: any, context: ApolloServerContext) {
      return context.cityWarehouse.getCityByCityId(root.cityId);
    },
  },
  Vote: {
    proposal(root: Vote, _: any, context: ApolloServerContext) {
      return context.proposalWarehouse.getProposalByProposalId(root.proposalId);
    },
  },
  Subscription: {
    memberUpdated: {
      subscribe: withFilter(
        (_: any, __: any, context: ApolloServerContext) => {
          context.logger.info('Started member update subscription');
          return context.pubSub.asyncIterator([
            IOTypes.SubscriptionActionEnum.MemberRegistered,
            IOTypes.SubscriptionActionEnum.MemberUpdated,
          ]);
        },
        (payload, variables) => payload.memberUpdated.id === variables.id
      ),
    },
    proposalAdded: {
      subscribe: withFilter(
        (_: any, __: any, context: ApolloServerContext) => {
          context.logger.info('Started proposal added subscription');
          return context.pubSub.asyncIterator([IOTypes.SubscriptionActionEnum.ProposalMade]);
        },
        (payload, variables) => {
          console.log('[proposalAddedSubFilter (index.ts)]:282');
          console.dir(payload, { depth: null });
          console.dir(payload.proposalAdded.retreatId === variables.retreatId, { depth: null });
          return payload.proposalAdded.retreatId === variables.retreatId;
        }
      ),
    },
    proposalUpdated: {
      subscribe: withFilter(
        (_: any, __: any, context: ApolloServerContext) => {
          context.logger.info('Started proposal update subscription');
          // TODO - Change this enum
          return context.pubSub.asyncIterator([IOTypes.SubscriptionActionEnum.ProposalRemoved]);
        },
        (payload, variables) => payload.proposalUpdated.id === variables.id
      ),
    },
    voteUpdated: {
      subscribe: withFilter(
        (_: any, __: any, context: ApolloServerContext) => {
          context.logger.info('Started vote update subscription');
          return context.pubSub.asyncIterator([IOTypes.SubscriptionActionEnum.VoteCast]);
        },
        (payload, variables) => payload.voteUpdated.id === variables.id
      ),
    },
    retreatUpdated: {
      subscribe: withFilter(
        (_: any, __: any, context: ApolloServerContext) => {
          context.logger.info('Started retreat update subscription');
          return context.pubSub.asyncIterator([IOTypes.SubscriptionActionEnum.RetreatRegistered]);
        },
        (payload, variables) => payload.retreatUpdated.id === variables.id
      ),
    },
  },
};

export default resolvers;
