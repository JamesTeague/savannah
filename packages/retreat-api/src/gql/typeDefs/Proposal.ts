import { gql } from 'apollo-server';
import City from './City';

const Proposal = gql`
  type Proposal {
    id: ID!
    city: City!
    retreatId: ID!
    memberId: ID!
  }
`;

export default [Proposal, ...City];
