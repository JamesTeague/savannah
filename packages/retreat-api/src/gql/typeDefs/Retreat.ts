import { gql } from 'apollo-server';

const Phase = gql`
  enum Phase {
    PROPOSAL
    VOTING
    RETREAT
  }
`;

const Retreat = gql`
  type Retreat {
    id: ID!
    name: String!
    phase: Phase!
  }
`;

export default [Retreat, Phase];
