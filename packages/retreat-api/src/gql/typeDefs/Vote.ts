import { gql } from 'apollo-server';
import Proposal from './Proposal';

const Vote = gql`
  type Vote {
    id: ID!
    proposal: Proposal!
    retreatId: ID!
    memberId: ID!
    points: Int!
  }
`;

export default [Vote, ...Proposal];
