import { gql } from 'apollo-server';
import Member from './Member';
import Retreat from './Retreat';

const Subscription = gql`
  type Subscription {
    memberAdded: Member!
    memberUpdated: Member!
    proposalAdded(retreatId: ID!): Proposal!
    proposalUpdated: Proposal!
    voteAdded: Vote!
    voteUpdated: Vote!
    retreatUpdated: Retreat!
  }
`;

export default [Subscription, ...Member, ...Retreat];
