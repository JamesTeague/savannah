import { gql } from 'apollo-server';
import Member from './Member';
import Retreat from './Retreat';

const RootQuery = gql`
  scalar Date
  scalar Time
  scalar DateTime
  scalar JSON

  type RootQuery {
    getMemberByMemberId(memberId: ID!): Member
    getMembersByRetreatId(retreatId: ID!): [Member]!
    getProposalByProposalId(proposalId: ID!): Proposal
    getProposalsByRetreatId(retreatId: ID!): [Proposal]!
    getRetreatByRetreatId(retreatId: ID!): Retreat
    getVoteByVoteId(voteId: ID!): Vote
    getVotesByMemberId(memberId: ID!): [Vote]!
  }
`;

// TODO - Fold Retreat into Member and remove mentions of retreat here
export default [RootQuery, ...Member, ...Retreat];
