import { gql } from 'apollo-server';
import Location from './Location';

const City = gql`
  type City {
    id: ID!
    address: String!
    country: Location!
    name: String!
    state: Location!
    url: String
    center: [Float]!
  }
`;

export default [City, ...Location];
