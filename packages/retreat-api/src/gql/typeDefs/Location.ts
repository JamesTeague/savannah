import { gql } from 'apollo-server';

const Location = gql`
  type Location {
    abbreviation: String!
    name: String!
  }
`;

export default [Location];
