import { gql } from 'apollo-server';
import Proposal from './Proposal';
import Vote from './Vote';

const Member = gql`
  type Member {
    id: ID!
    displayName: String!
    email: String!
    points: Int!
    proposals: [Proposal]!
    votes: [Vote]!
  }
`;

export default [Member, ...Proposal, ...Vote];
