import { gql } from 'apollo-server';
import RootQuery from './RootQuery';
import Subscription from './Subscription';

const Schema = gql`
  schema {
    query: RootQuery
    subscription: Subscription
  }
`;

export default [Schema, ...RootQuery, ...Subscription];
