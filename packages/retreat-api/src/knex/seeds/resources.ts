import Bluebird from 'bluebird';
import Knex from 'knex';
import { proposals, cities, members, votes, retreats } from '../resources';

export const seed = (knex: Knex): Bluebird<any> => {
  return Bluebird.try(async () => {
    await Bluebird.all([
      knex('retreats').delete(),
      knex('retreat_member_xref').delete(),
      knex('members').delete(),
      knex('cities').delete(),
      knex('proposals').delete(),
      knex('votes').delete(),
    ]);

    await Bluebird.all([...retreats.map(retreat => knex('retreats').insert(retreat))]);
    await Bluebird.all([...members.map(member => knex('members').insert(member))]);
    await Bluebird.all([
      ...members.map(member =>
        knex('retreat_member_xref').insert({
          member_id: member.id,
          retreat_id: retreats[0].id,
        })
      ),
    ]);
    await Bluebird.all([
      ...cities.map(city =>
        knex('cities').insert({
          ...city,
          center: JSON.stringify(city.center),
        })
      ),
    ]);
    await Bluebird.all([...proposals.map(proposal => knex('proposals').insert(proposal))]);
    await Bluebird.all([...votes.map(vote => knex('votes').insert(vote))]);
  });
};

export default {
  seed,
};
