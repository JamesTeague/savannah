import proposals from './proposals';
import members from './members';
import retreats from './retreats';

const vote1 = {
  proposal_id: proposals[4].id,
  id: '107f41de-6fd8-4a80-8c59-fca1bd3a3ad3',
  points: 4,
  member_id: members[0].id,
  retreat_id: retreats[0].id,
};

const vote2 = {
  id: '8183c412-fc96-4a55-8242-ef666ae3405a',
  proposal_id: proposals[0].id,
  points: 4,
  member_id: members[1].id,
  retreat_id: retreats[0].id,
};

const vote3 = {
  id: 'd3bcc5c4-6078-4efd-aea0-4afeb89ed08d',
  proposal_id: proposals[0].id,
  points: 2,
  member_id: members[0].id,
  retreat_id: retreats[0].id,
};

const vote4 = {
  id: '5ec51047-5c38-425d-bcec-390c08435cfb',
  proposal_id: proposals[3].id,
  points: 4,
  member_id: members[0].id,
  retreat_id: retreats[0].id,
};

export default [vote1, vote2, vote3, vote4];
