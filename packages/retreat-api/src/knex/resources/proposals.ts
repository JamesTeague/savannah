import cities from './cities';
import members from './members';
import retreats from './retreats';

const proposal1 = {
  id: '75410980-20ff-4022-9565-f18a36691529',
  city_id: cities[0].id,
  member_id: members[1].id,
  retreat_id: retreats[0].id,
};

const proposal2 = {
  id: '8bae8c0f-8e89-4b2f-b4c9-bc7f08beea3f',
  city_id: cities[1].id,
  member_id: members[1].id,
  retreat_id: retreats[0].id,
};

const proposal3 = {
  id: 'cc0089d8-e3ed-4b99-99c2-bb0c8cc98e32',
  city_id: cities[2].id,
  member_id: members[0].id,
  retreat_id: retreats[0].id,
};

const proposal4 = {
  id: 'd9b381c3-4579-4c5f-8929-9a9e6139fc94',
  city_id: cities[3].id,
  member_id: members[0].id,
  retreat_id: retreats[0].id,
};

const proposal5 = {
  id: 'ddbfc43b-cd57-41b1-a25f-a92283c93263',
  city_id: cities[4].id,
  member_id: members[0].id,
  retreat_id: retreats[0].id,
};

export default [proposal1, proposal2, proposal3, proposal4, proposal5];
