import Knex from 'knex';

// const ON_UPDATE_TIMESTAMP_FUNCTION = `
//   CREATE OR REPLACE FUNCTION on_update_timestamp()
//   RETURNS trigger AS $$
//   BEGIN
//     NEW.updated_at = now();
//     RETURN NEW;
//   END;
// $$ language 'plpgsql';
// `;
//
// const DROP_ON_UPDATE_TIMESTAMP_FUNCTION = `DROP FUNCTION on_update_timestamp`;
//
// const onUpdateTrigger = table => `
//     CREATE TRIGGER ${table}_updated_at
//     BEFORE UPDATE ON ${table}
//     FOR EACH ROW
//     EXECUTE PROCEDURE on_update_timestamp();
//   `;
//
// const dropTrigger = table => `DROP TRIGGER IF EXISTS ${table}_updated_at ON ${table}`;

export function up(knex: Knex): Promise<any> {
  return knex.schema
    .createTable('cities', tableBuilder => {
      tableBuilder.text('id').primary();
      tableBuilder.text('address').notNullable();
      tableBuilder.jsonb('country').notNullable();
      tableBuilder.text('name').notNullable();
      tableBuilder.jsonb('state').notNullable();
      tableBuilder.jsonb('center').notNullable();
      tableBuilder.text('url').nullable();
      tableBuilder.timestamp('created_at').defaultTo(knex.fn.now());
      tableBuilder.timestamp('updated_at').defaultTo(knex.fn.now());
    })
    .createTable('members', tableBuilder => {
      tableBuilder.uuid('id').primary();
      tableBuilder.text('display_name').notNullable();
      tableBuilder.text('email');
      tableBuilder.integer('points').notNullable();
      tableBuilder.timestamp('created_at').defaultTo(knex.fn.now());
      tableBuilder.timestamp('updated_at').defaultTo(knex.fn.now());
    })
    .createTable('proposals', tableBuilder => {
      tableBuilder.uuid('id').primary();
      tableBuilder.text('member_id').notNullable();
      tableBuilder.text('city_id').notNullable();
      tableBuilder.text('retreat_id').notNullable();
      tableBuilder.timestamp('created_at').defaultTo(knex.fn.now());
      tableBuilder.timestamp('updated_at').defaultTo(knex.fn.now());
    })
    .createTable('votes', tableBuilder => {
      tableBuilder.uuid('id').primary();
      tableBuilder.uuid('proposal_id').notNullable();
      tableBuilder.text('member_id').notNullable();
      tableBuilder.integer('points').notNullable();
      tableBuilder.text('retreat_id').notNullable();
      tableBuilder.timestamp('created_at').defaultTo(knex.fn.now());
      tableBuilder.timestamp('updated_at').defaultTo(knex.fn.now());
    })
    .createTable('retreats', tableBuilder => {
      tableBuilder.uuid('id').primary();
      tableBuilder.text('name').notNullable();
      tableBuilder.text('phase').notNullable();
    })
    .createTable('retreat_member_xref', tableBuilder => {
      tableBuilder.increments('increment').primary();
      tableBuilder.uuid('retreat_id').notNullable();
      tableBuilder.uuid('member_id').notNullable();
    });
}

export function down(knex: Knex): Promise<any> {
  return knex.schema
    .dropTableIfExists('retreats')
    .dropTableIfExists('retreat_member_xref')
    .dropTableIfExists('cities')
    .dropTableIfExists('members')
    .dropTableIfExists('proposals')
    .dropTableIfExists('votes');
}
