import { IEventExecutionContext } from '@penguinhouse/retreat-es';
import { IOTypes } from '@penguinhouse/savannah-io';
import { SavannahContext } from '../Application';
import { CreateSavannahEventTransport } from './SavannahTransporter';

async function RetreatRegisteredTransportHandler(
  _: IEventExecutionContext,
  event: IOTypes.RetreatRegistered,
  state: SavannahContext
): Promise<void> {
  await state.retreatRepository.registerRetreat(event);

  const retreat = state.retreatWarehouse.getRetreatByRetreatId(event.id);

  await state.pubSub.publish(IOTypes.SubscriptionActionEnum.RetreatRegistered, {
    retreatAdded: retreat,
  });
}

const RetreatRegisteredTransport = CreateSavannahEventTransport({
  action: IOTypes.EventActionEnum.RetreatRegistered,
  payloadType: null,
  handler: RetreatRegisteredTransportHandler,
});

export default RetreatRegisteredTransport;
