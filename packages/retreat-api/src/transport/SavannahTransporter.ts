import { IEventExecutionContext, ISavannahPlatform } from '@penguinhouse/retreat-es';
import { Base } from '@penguinhouse/savannah-io';

// TODO - experiment with returning something from savannah (Rx.Subscription)
export interface ISavannahTransporter<TState> {
  bind(anna: ISavannahPlatform, state: TState): void;
}

export type EventHandler<TPayload, TResult, TState> = (
  context: IEventExecutionContext,
  payloadType: TPayload,
  state: TState
) => Promise<TResult>;

type CreateSavannahEventTransportArgs<TPayload, TResult, TState> = {
  action: string;
  payloadType: Base.Nullable<TPayload>;
  handler: EventHandler<TPayload, TResult, TState>;
};

function CreateSavannahEventTransport<TPayload, TResult, TState>(
  args: CreateSavannahEventTransportArgs<TPayload, TResult, TState>
): ISavannahTransporter<TState> {
  return {
    bind(savannah: ISavannahPlatform, state: TState) {
      const consumeArgs = {
        action: args.action,
        payloadType: args.payloadType,
        handler: (context, payloadType) => args.handler(context, payloadType, state),
      };

      // TODO - consider subscribing here in the app not inside call
      savannah.onEvent(consumeArgs);
    },
  };
}

export { CreateSavannahEventTransport };
