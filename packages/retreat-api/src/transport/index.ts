import { ISavannahPlatform } from '@penguinhouse/retreat-es';
import * as Rx from 'rxjs';
import MemberRegisteredTransport from './MemberRegisteredTransport';
import ProposalMadeTransport from './ProposalMadeTransport';
import VoteCastTransport from './VoteCastTransport';
import { SavannahContext } from '../Application';
import RetreatRegisteredTransport from './RetreatRegisteredTransport';

const SavannahTransports = [
  MemberRegisteredTransport,
  ProposalMadeTransport,
  RetreatRegisteredTransport,
  VoteCastTransport,
];

export type SavannahTransportConfiguration<TState> = {
  type: 'savannah';
  context: TState;
  connection: {
    savannah: ISavannahPlatform;
  };
};

function ConfigureSavannahTransport<TState>(config: SavannahTransportConfiguration<TState>) {
  const { connection, context } = config;
  const { savannah } = connection;

  const subscription: Rx.Subscription = SavannahTransports.reduce(
    (acc, transport) =>
      <Rx.Subscription>acc.add(transport.bind(savannah, (context as unknown) as SavannahContext)),
    new Rx.Subscription()
  );

  return {
    unbind() {
      subscription.unsubscribe();
    },
  };
}

export default ConfigureSavannahTransport;
