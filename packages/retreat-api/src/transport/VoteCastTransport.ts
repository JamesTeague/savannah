import Bluebird from 'bluebird';
import { IEventExecutionContext } from '@penguinhouse/retreat-es';
import { SavannahContext } from '../Application';
import { CreateSavannahEventTransport } from './SavannahTransporter';
import { IOTypes } from '@penguinhouse/savannah-io';

async function VoteCastTransportHandler(
  _: IEventExecutionContext,
  event: IOTypes.VoteCast,
  state: SavannahContext
): Promise<void> {
  await Bluebird.all([
    state.voteRepository.addVote(event),
    state.memberRepository.setPoints(event),
  ]);

  const [vote, member] = await Bluebird.all([
    state.voteWarehouse.getVoteByVoteId(event.id),
    state.memberWarehouse.getMemberByMemberId(event.memberId),
  ]);

  await state.pubSub.publish(IOTypes.SubscriptionActionEnum.VoteCast, { voteAdded: vote });
  await state.pubSub.publish(IOTypes.SubscriptionActionEnum.MemberUpdated, {
    memberUpdate: member,
  });
}

const VoteCastTransport = CreateSavannahEventTransport({
  action: IOTypes.EventActionEnum.VoteCast,
  payloadType: null,
  handler: VoteCastTransportHandler,
});

export default VoteCastTransport;
