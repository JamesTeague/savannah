import { SavannahPlatform } from '@penguinhouse/retreat-es';
import { CreateSavannahEventTransport } from './SavannahTransporter';
jest.mock('@penguinhouse/retreat-es');

describe('Transports', () => {
  it('creates an event transport', () => {
    const { bind } = CreateSavannahEventTransport({
      action: 'test-action',
      payloadType: null,
      handler: jest.fn().mockResolvedValue({}),
    });

    expect(bind).toBeDefined();
  });

  it('bind calls onEvent', () => {
    const args = {
      action: 'test-action',
      payloadType: null,
      handler: jest.fn().mockResolvedValue({}),
    };
    const { bind } = CreateSavannahEventTransport(args);
    const platform = new SavannahPlatform({
      appName: 'index.spec',
      password: '',
      database: '',
      user: '',
      host: '',
    });

    const spy = jest.spyOn(platform, 'onEvent');

    bind(platform, {});

    expect(spy).toHaveBeenCalledWith({
      ...args,
      handler: expect.any(Function),
    });
  });
});
