import { IEventExecutionContext } from '@penguinhouse/retreat-es';
import { IOTypes } from '@penguinhouse/savannah-io';
import Bluebird from 'bluebird';
import { SavannahContext } from '../Application';
import { CreateSavannahEventTransport } from './SavannahTransporter';

async function ProposalMadeTransportHandler(
  _: IEventExecutionContext,
  event: IOTypes.ProposalMade,
  state: SavannahContext
): Promise<void> {
  await Bluebird.all([
    state.proposalRepository.addProposal(event),
    state.cityRepository.addCity(event.city),
  ]);

  const [proposal, city] = await Bluebird.all([
    state.proposalWarehouse.getProposalByProposalId(event.id),
    state.cityWarehouse.getCityByCityId(event.city.id),
  ]);

  await state.pubSub.publish(IOTypes.SubscriptionActionEnum.ProposalMade, {
    proposalAdded: {
      ...proposal,
      city: { ...city },
      memberId: event.memberId,
    },
  });
}

const ProposalMadeTransport = CreateSavannahEventTransport({
  action: IOTypes.EventActionEnum.ProposalMade,
  payloadType: null,
  handler: ProposalMadeTransportHandler,
});

export default ProposalMadeTransport;
