import { IEventExecutionContext } from '@penguinhouse/retreat-es';
import { IOTypes } from '@penguinhouse/savannah-io';
import { SavannahContext } from '../Application';
import { CreateSavannahEventTransport } from './SavannahTransporter';

async function MemberRegisteredTransportHandler(
  _: IEventExecutionContext,
  event: IOTypes.MemberRegistered,
  state: SavannahContext
): Promise<void> {
  await state.memberRepository.registerMember(event);

  const member = await state.memberWarehouse.getMemberByMemberId(event.id);

  return state.pubSub.publish(IOTypes.SubscriptionActionEnum.MemberRegistered, {
    memberAdded: member,
  });
}

const MemberRegisteredTransport = CreateSavannahEventTransport({
  action: IOTypes.EventActionEnum.MemberRegistered,
  payloadType: null,
  handler: MemberRegisteredTransportHandler,
});

export default MemberRegisteredTransport;
