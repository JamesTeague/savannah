import Knex from 'knex';
import * as Path from 'path';

/** Default database name */
const DEFAULT_DB_NAME = 'retreat_api';

/** Default database username */
const DEFAULT_DB_USER = 'postgres';

/** Default database password */
const DEFAULT_DB_PASS = 'postgres';

/** Default database port */
const DEFAULT_DB_PORT = 5432;

/** Default database host */
const DEFAULT_DB_HOST = 'localhost';

const dbConfig = {
  host: process.env.PG_DB_HOST || DEFAULT_DB_HOST,
  port: Number(process.env.PG_DB_PORT || DEFAULT_DB_PORT),
  database: process.env.PG_DB_NAME || DEFAULT_DB_NAME,
  user: process.env.PG_USER_NAME || DEFAULT_DB_USER,
  password: process.env.PG_PASSWORD || DEFAULT_DB_PASS,
};

const KNEX_DIR = Path.resolve(__dirname, 'src/knex');

// TODO - async/await or thenables not both
async function main() {
  const knex = await Knex({
    client: 'pg',
    connection: dbConfig,
    seeds: {
      directory: Path.resolve(KNEX_DIR, 'seeds'),
    },
  });

  console.log('Seeding start');
  return knex.seed
    .run()
    .then(() => {
      knex.destroy(() => {
        console.log('Seeding end');
      });
    })
    .catch(error => {
      console.error('Error while seeding the data', error);
    });
}

main().then(() => {
  console.log('done');
});
