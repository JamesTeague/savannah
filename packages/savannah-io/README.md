@penguinhouse/savannah-io
---
![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg) ![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)

Input/Output types and enumerations for microservices in project savannah and common types shared throughout the platform.
