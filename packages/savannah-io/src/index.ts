import * as moment from 'moment';

export namespace IOTypes {
  export interface Location {
    abbreviation: string;
    name: string;
  }

  export interface City {
    address: string;
    country: Location;
    id: string;
    name: string;
    state: Location;
    url: string;
    center: number[];
  }

  interface UserName {
    first: string;
    last: string;
  }

  export interface User {
    id: string;
    retreatIds: string[];
    email: string;
    name: UserName;
    groups: string[];
    roles: string[];
    permissions: string[];
  }

  export interface Command<T> {
    id: string;
    timestamp: moment.Moment;
    action: string;
    user: User;
    data: T;
  }

  export interface CommandResult<T> {
    id: string;
    timestamp: moment.Moment;
    success: boolean;
    data: T;
    user: User;
  }

  export interface Event<T> {
    id: string;
    timestamp: moment.Moment;
    action: string;
    data: T;
  }

  export interface Member {
    id: string;
    displayName: string;
    email: string;
    points: number;
    proposals: Proposal[];
    votes: Vote[];
    retreatIds: string[];
  }

  export interface Proposal {
    id: string;
    city: City;
    retreatId: string;
    memberId: string;
  }

  export enum Phase {
    Proposal = 'PROPOSAL',
    Voting = 'VOTING',
    Retreat = 'RETREAT',
  }

  export enum ResponseCode {
    Success = 'G200',
    NotFound = 'G404',
    FailedValidation = 'V400',
  }

  export enum CommandActionEnum {
    MakeProposal = 'make-proposal',
    RegisterMember = 'register-member',
    RegisterRetreat = 'register-retreat',
    RemoveProposal = 'remove-proposal',
    SetRetreatPhase = 'set-retreat-phase',
    CastVote = 'cast-vote',
    RescindVote = 'rescind-vote',
  }

  export enum EventActionEnum {
    MemberRegistered = 'member-registered',
    ProposalMade = 'proposal-made',
    ProposalRemoved = 'proposal-removed',
    RetreatNameUpdated = 'retreat-name-updated',
    RetreatPhaseChanged = 'retreat-phase-changed',
    RetreatRegistered = 'retreat-registered',
    VoteCast = 'vote-cast',
    VoteRescinded = 'vote-rescinded',
  }

  export enum SubscriptionActionEnum {
    MemberRegistered = 'memberRegistered',
    MemberUpdated = 'memberUpdated',
    ProposalMade = 'proposalMade',
    ProposalRemoved = 'proposalRemoved',
    RetreatNameUpdated = 'retreatNameUpdated',
    RetreatPhaseChanged = 'retreatPhaseChanged',
    RetreatRegistered = 'retreatRegistered',
    VoteCast = 'voteCast',
    VoteRescinded = 'voteRescinded',
  }

  export interface Retreat {
    id: string;
    name: string;
    phase: Phase;
  }

  export interface Vote {
    proposalId: string;
    memberId: string;
    points: number;
    id: string;
    retreatId: string;
  }

  export interface CastVote {
    memberId: string;
    retreatId: string;
    proposalId: string;
    points: number;
  }

  export interface MakeProposal {
    memberId: string;
    city: City;
    retreatId: string;
  }

  export interface RegisterMember {
    displayName: string;
    email: string;
    retreatId: string;
  }

  export interface RegisterRetreat {
    retreatId: string;
    name: string;
  }

  export interface RemoveProposal {
    memberId: string;
    proposalId: string;
  }

  export interface RescindVote {
    memberId: string;
    voteId: string;
  }

  export interface SetRetreatPhase {
    retreatId: string;
    phase: Phase;
  }

  export interface MemberRegistered {
    id: string;
    displayName: string;
    email: string;
    points: number;
    retreatId: string;
  }

  export interface ProposalMade {
    id: string;
    city: City;
    memberId: string;
    retreatId: string;
  }

  export interface RetreatRegistered {
    id: string;
    name: string;
    phase: Phase;
  }

  export interface VoteCast {
    id: string;
    proposalId: string;
    memberId: string;
    retreatId: string;
    points: number;
    memberPoints: number;
  }
}

export namespace Base {
  export interface ISerializable<T> {
    toJSON(): T;
  }
  export type Nullable<T> = T | null;
}
