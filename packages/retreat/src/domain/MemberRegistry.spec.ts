import Knex from 'knex';
import mockDb from 'mock-knex';
import { NullLog } from 'stoolie';
import { KnexMemberRepository } from '../persistence';
import MemberRegistry from './MemberRegistry';
import { Command, User } from '@penguinhouse/retreat-es';
import moment from 'moment';
import Member from './Member';
import { IOTypes } from '@penguinhouse/savannah-io';

describe('MemberRegistry', () => {
  let repository;
  let context;
  const knex = Knex({ client: 'pg' });

  beforeEach(() => {
    mockDb.mock(knex);
    context = Command.fromJSON({
      id: '0',
      timestamp: moment(),
      action: 'test-action',
      user: User.dummy(),
      data: { testField: 'test' },
    });
    repository = new KnexMemberRepository(knex, NullLog);
  });

  afterEach(() => {
    mockDb.unmock(knex);
  });

  it('registers a member', async () => {
    const repositorySpy = jest.spyOn(repository, 'addMember');
    const contextSpy = jest.spyOn(context, 'createChildEvent');
    const registry = new MemberRegistry(repository);
    const member = Member.fromInitial('0', 'displayName', 'email', '0');

    await registry.registerMember(context, member);

    expect(repositorySpy).toHaveBeenCalledWith(member);
    expect(contextSpy).toHaveBeenCalledWith(IOTypes.EventActionEnum.MemberRegistered, {
      ...member.toJSON(),
      retreatId: member.retreatIds[0],
    });
  });
});
