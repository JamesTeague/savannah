import Knex from 'knex';
import mockDb from 'mock-knex';
import { NullLog } from 'stoolie';
import { KnexRetreatRepository } from '../persistence';
import RetreatRegistry from './RetreatRegistry';
import { Command, User } from '@penguinhouse/retreat-es';
import moment from 'moment';
import Retreat from './Retreat';
import { IOTypes } from '@penguinhouse/savannah-io';

describe('RetreatRegistry', () => {
  let repository;
  let context;
  const knex = Knex({ client: 'pg' });

  beforeEach(() => {
    mockDb.mock(knex);
    context = Command.fromJSON({
      id: '0',
      timestamp: moment(),
      action: 'test-action',
      user: User.dummy(),
      data: { testField: 'test' },
    });
    repository = new KnexRetreatRepository(knex, NullLog);
  });

  afterEach(() => {
    mockDb.unmock(knex);
  });

  it('registers a retreat', async () => {
    const repositorySpy = jest.spyOn(repository, 'registerRetreat');
    const contextSpy = jest.spyOn(context, 'createChildEvent');
    const registry = new RetreatRegistry(repository);
    const retreat = Retreat.fromInitial('0', 'displayName');

    await registry.registerRetreat(context, retreat);

    expect(repositorySpy).toHaveBeenCalledWith(retreat);
    expect(contextSpy).toHaveBeenCalledWith(IOTypes.EventActionEnum.RetreatRegistered, {
      ...retreat.toJSON(),
    });
  });
});
