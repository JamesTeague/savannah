import uuid from 'uuid/v4';
import { Base, IOTypes } from '@penguinhouse/savannah-io';
import { ICreateChildEvents } from '@penguinhouse/retreat-es';
import { IHandleProposal, IHandleVote } from './interfaces';

export default class Member
  implements IHandleProposal, IHandleVote, Base.ISerializable<IOTypes.Member> {
  private points: number;
  private readonly displayName: string;
  private readonly email: string;
  private readonly _id: string;
  private readonly proposals: IOTypes.Proposal[];
  private readonly votes: IOTypes.Vote[];
  private readonly _retreatIds: string[];

  constructor(
    displayName: string,
    email: string,
    points: number,
    id: string,
    proposals: IOTypes.Proposal[],
    votes: IOTypes.Vote[],
    retreatIds: string[]
  ) {
    this.displayName = displayName;
    this.email = email;
    this.points = points;
    this._id = id;
    this.proposals = proposals;
    this.votes = votes;
    this._retreatIds = retreatIds;
  }

  get id() {
    return this._id;
  }

  get retreatIds() {
    return this._retreatIds;
  }

  makeProposal(context: ICreateChildEvents, city: IOTypes.City, retreatId: string) {
    if (this.proposals.length < 5) {
      const proposal = {
        id: uuid(),
        city,
        retreatId,
        memberId: this._id,
      };

      this.proposals.push(proposal);
      context.createChildEvent(IOTypes.EventActionEnum.ProposalMade, {
        ...proposal,
        memberId: this._id,
        retreatId,
      });
    }
  }

  removeProposal(context: ICreateChildEvents, proposalId: string) {
    const proposalIdIndex = this.proposals.findIndex(proposal => proposal.id === proposalId);

    if (proposalIdIndex >= 0) {
      this.proposals.splice(proposalIdIndex, 1);
      context.createChildEvent(IOTypes.EventActionEnum.ProposalRemoved, proposalId);
    }
  }

  castVote(context: ICreateChildEvents, proposalId: string, votePoints: number, retreatId: string) {
    if (votePoints <= this.points) {
      const vote = {
        id: uuid(),
        proposalId,
        points: votePoints,
        retreatId,
        memberId: this._id,
      };

      this.points -= votePoints;

      this.votes.push(vote);
      context.createChildEvent(IOTypes.EventActionEnum.VoteCast, {
        ...vote,
        retreatId,
        memberId: this._id,
        memberPoints: this.points,
      });
    }
  }

  rescindVote(context: ICreateChildEvents, voteId: string) {
    const voteIdIndex = this.votes.findIndex(vote => vote.id === voteId);
    if (voteIdIndex >= 0) {
      this.votes.splice(voteIdIndex, 1);
      context.createChildEvent(IOTypes.EventActionEnum.VoteRescinded, voteId);
    }
  }

  toJSON(): IOTypes.Member {
    return {
      displayName: this.displayName,
      email: this.email,
      points: this.points,
      id: this._id,
      proposals: this.proposals,
      votes: this.votes,
      retreatIds: this.retreatIds,
    };
  }

  static fromInitial(id: string, displayName: string, email: string, retreatId: string) {
    return new Member(displayName, email, 10, id, [], [], [retreatId]);
  }

  static fromJson(json: IOTypes.Member) {
    return new Member(
      json.displayName,
      json.email,
      json.points,
      json.id,
      json.proposals,
      json.votes,
      json.retreatIds
    );
  }
}
