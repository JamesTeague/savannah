import Retreat from './Retreat';
import { Command, User } from '@penguinhouse/retreat-es';
import moment from 'moment';
import { IOTypes } from '@penguinhouse/savannah-io';

describe('Retreat', () => {
  let context;
  let retreat;

  beforeEach(() => {
    retreat = Retreat.fromInitial('0', 'test-retreat');
    context = Command.fromJSON({
      id: '0',
      timestamp: moment(),
      action: 'test-action',
      user: User.dummy(),
      data: { testField: 'test' },
    });
  });

  it('sets name', () => {
    const spy = jest.spyOn(context, 'createChildEvent');

    retreat.setName(context, 'test-name-change');

    expect(spy).toHaveBeenCalledWith(IOTypes.EventActionEnum.RetreatNameUpdated, {
      retreatId: '0',
      old: 'test-retreat',
      new: 'test-name-change',
    });
  });

  it('does not set name if name is the same', () => {
    const spy = jest.spyOn(context, 'createChildEvent');

    expect(() => retreat.setName(context, 'test-retreat')).toThrow(
      'Retreat already has the given name.'
    );

    expect(spy).not.toHaveBeenCalled();
  });

  it('sets phase', () => {
    const spy = jest.spyOn(context, 'createChildEvent');

    retreat.setPhase(context, IOTypes.Phase.Voting);

    expect(spy).toHaveBeenCalledWith(IOTypes.EventActionEnum.RetreatPhaseChanged, {
      retreatId: '0',
      old: IOTypes.Phase.Proposal,
      new: IOTypes.Phase.Voting,
    });
  });

  it('does not set phase if phase is the same', () => {
    const spy = jest.spyOn(context, 'createChildEvent');

    expect(() => retreat.setPhase(context, IOTypes.Phase.Proposal)).toThrow(
      'Retreat is already in the given phase.'
    );

    expect(spy).not.toHaveBeenCalled();
  });
});
