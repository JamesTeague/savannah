import Member from './Member';
import { Command, User } from '@penguinhouse/retreat-es';
import moment from 'moment';
import { IOTypes } from '@penguinhouse/savannah-io';

describe('Member', () => {
  let member: Member;
  let command;
  const city = {
    id: '0',
    address: '',
    country: {
      abbreviation: '',
      name: '',
    },
    name: '',
    state: {
      abbreviation: '',
      name: '',
    },
    url: '',
  };

  beforeEach(() => {
    member = Member.fromInitial('0', 'displayName', 'email', '0');
    command = Command.fromJSON({
      id: '0',
      timestamp: moment(),
      action: 'test-action',
      user: User.dummy(),
      data: { testField: 'test' },
    });
  });

  it('makes a proposal', () => {
    const spy = jest.spyOn(command, 'createChildEvent');

    member.makeProposal(command, city, '0');

    expect(spy).toHaveBeenCalledWith(IOTypes.EventActionEnum.ProposalMade, {
      id: expect.any(String),
      city,
      retreatId: '0',
      memberId: '0',
    });
  });

  it('does not make a proposal', () => {
    const proposal = {
      id: '0',
      retreatId: '0',
      city,
      memberId: '0',
    };
    member = Member.fromJson({
      id: '0',
      displayName: 'displayName',
      email: 'email',
      retreatIds: ['0'],
      points: 10,
      proposals: [proposal, proposal, proposal, proposal, proposal],
      votes: [],
    });
    const spy = jest.spyOn(command, 'createChildEvent');

    member.makeProposal(command, city, '0');

    expect(spy).not.toHaveBeenCalled();
  });

  it('removes a proposal', () => {
    member = Member.fromJson({
      id: '0',
      displayName: 'displayName',
      email: 'email',
      retreatIds: ['0'],
      points: 10,
      proposals: [
        {
          id: '0',
          retreatId: '0',
          city,
          memberId: '0',
        },
      ],
      votes: [],
    });
    const spy = jest.spyOn(command, 'createChildEvent');

    member.removeProposal(command, '0');

    expect(spy).toHaveBeenCalledWith(IOTypes.EventActionEnum.ProposalRemoved, '0');
  });

  it('does not a proposal if does not exist', () => {
    const spy = jest.spyOn(command, 'createChildEvent');

    member.removeProposal(command, '0');

    expect(spy).not.toHaveBeenCalled();
  });

  it('casts a vote', () => {
    const spy = jest.spyOn(command, 'createChildEvent');

    member.castVote(command, '0', 10, '0');

    expect(spy).toHaveBeenCalledWith(IOTypes.EventActionEnum.VoteCast, {
      id: expect.any(String),
      proposalId: '0',
      points: 10,
      retreatId: '0',
      memberId: '0',
      memberPoints: 0,
    });
  });

  it('does not cast a vote', () => {
    const spy = jest.spyOn(command, 'createChildEvent');

    member.castVote(command, '0', 11, '0');

    expect(spy).not.toHaveBeenCalled();
  });

  it('rescinds a vote', () => {
    const vote = {
      id: '0',
      proposalId: '0',
      points: 10,
      retreatId: '0',
      memberId: '0',
    };

    member = Member.fromJson({
      id: '0',
      displayName: 'displayName',
      email: 'email',
      retreatIds: ['0'],
      points: 10,
      proposals: [],
      votes: [vote],
    });
    const spy = jest.spyOn(command, 'createChildEvent');

    member.rescindVote(command, '0');

    expect(spy).toHaveBeenCalledWith(IOTypes.EventActionEnum.VoteRescinded, '0');
  });

  it('does not rescind a vote if it does not exist', () => {
    const spy = jest.spyOn(command, 'createChildEvent');

    member.castVote(command, '0', 11, '0');

    expect(spy).not.toHaveBeenCalled();
  });

  it('serializes correctly', () => {
    const json = member.toJSON();

    expect(json).toMatchObject({
      id: '0',
      displayName: 'displayName',
      email: 'email',
      retreatIds: ['0'],
      points: 10,
      proposals: [],
      votes: [],
    });
  });
});
