import { ICreateChildEvents } from '@penguinhouse/retreat-es';
import { Base, IOTypes } from '@penguinhouse/savannah-io';
import { IChangeName, IChangePhase } from './interfaces';

export default class Retreat
  implements IChangeName, IChangePhase, Base.ISerializable<IOTypes.Retreat> {
  private readonly _id: string;
  private name: string;
  private phase: IOTypes.Phase;

  constructor(id: string, name: string, phase: IOTypes.Phase) {
    this._id = id;
    this.name = name;
    this.phase = phase;
  }

  get id() {
    return this._id;
  }

  setName(context: ICreateChildEvents, name: string) {
    if (this.name === name) {
      throw new Error('Retreat already has the given name.');
    }

    const old = this.name;
    this.name = name;

    context.createChildEvent(IOTypes.EventActionEnum.RetreatNameUpdated, {
      retreatId: this._id,
      old,
      new: name,
    });
  }

  setPhase(context: ICreateChildEvents, phase: IOTypes.Phase) {
    if (this.phase === phase) {
      throw new Error('Retreat is already in the given phase.');
    }

    const old = this.phase;
    this.phase = phase;

    context.createChildEvent(IOTypes.EventActionEnum.RetreatPhaseChanged, {
      retreatId: this._id,
      old,
      new: phase,
    });
  }

  toJSON(): IOTypes.Retreat {
    return {
      id: this._id,
      name: this.name,
      phase: this.phase,
    };
  }

  static fromInitial(id: string, name: string) {
    return new Retreat(id, name, IOTypes.Phase.Proposal);
  }

  static fromJson(json: IOTypes.Retreat) {
    return new Retreat(json.id, json.name, json.phase);
  }
}
