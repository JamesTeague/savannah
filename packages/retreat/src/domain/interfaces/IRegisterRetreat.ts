import Bluebird from 'bluebird';
import { ICreateChildEvents } from '@penguinhouse/retreat-es';
import Retreat from '../Retreat';

export interface IRegisterRetreat {
  registerRetreat(context: ICreateChildEvents, retreat: Retreat): Bluebird<void>;
}
