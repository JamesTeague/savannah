import Bluebird from 'bluebird';
import { ICommandExecutionContext } from '@penguinhouse/retreat-es';

export interface IProcessable {
  process(context: ICommandExecutionContext): Bluebird<void>;
}
