import Bluebird from 'bluebird';
import { ICreateChildEvents } from '@penguinhouse/retreat-es';
import Member from '../Member';

export interface IRegisterMember {
  registerMember(context: ICreateChildEvents, member: Member): Bluebird<void>;
}
