export interface ICheckEquality<T> {
  isEqual(id: any): boolean;
  isDeepEqual(obj: T): boolean;
}
