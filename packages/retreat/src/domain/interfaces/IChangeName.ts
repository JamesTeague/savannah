import { ICreateChildEvents } from '@penguinhouse/retreat-es';

export interface IChangeName {
  setName(context: ICreateChildEvents, name: string): void;
}
