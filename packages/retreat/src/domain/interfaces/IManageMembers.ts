import Bluebird from 'bluebird';
import { ICreateChildEvents } from '@penguinhouse/retreat-es';
import Member from '../Member';
import { IMemberRepository } from '../../persistence/interfaces';

export interface IManageMembers {
  registerMember(
    context: ICreateChildEvents,
    repository: IMemberRepository,
    member: Member
  ): Bluebird<void>;
}
