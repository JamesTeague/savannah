import { IOTypes } from '@penguinhouse/savannah-io';
import { ICreateChildEvents } from '@penguinhouse/retreat-es';

export interface IChangePhase {
  setPhase(context: ICreateChildEvents, phase: IOTypes.Phase): void;
}
