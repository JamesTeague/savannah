import { ICreateChildEvents } from '@penguinhouse/retreat-es';

export interface IHandleVote {
  castVote(
    context: ICreateChildEvents,
    proposalId: string,
    points: number,
    retreatId: string
  ): void;
  rescindVote(context: ICreateChildEvents, voteId: string): void;
}
