import { IOTypes } from '@penguinhouse/savannah-io';
import { ICommandExecutionContext } from '@penguinhouse/retreat-es';

export interface IHandleProposal {
  makeProposal(context: ICommandExecutionContext, city: IOTypes.City, retreatId: string): void;
  removeProposal(context: ICommandExecutionContext, id: string): void;
}
