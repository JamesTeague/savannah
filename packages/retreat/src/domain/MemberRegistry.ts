import { IOTypes } from '@penguinhouse/savannah-io';
import { ICreateChildEvents } from '@penguinhouse/retreat-es';
import { IMemberRepository } from '../persistence/interfaces';
import { IRegisterMember } from './interfaces';
import Member from './Member';

export default class MemberRegistry implements IRegisterMember {
  repository: IMemberRepository;

  constructor(repository: IMemberRepository) {
    this.repository = repository;
  }

  registerMember(context: ICreateChildEvents, member: Member) {
    return this.repository.addMember(member).tap(() => {
      context.createChildEvent(IOTypes.EventActionEnum.MemberRegistered, {
        ...member.toJSON(),
        retreatId: member.retreatIds[0],
      });
    });
  }
}
