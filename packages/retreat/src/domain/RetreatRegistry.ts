import { IOTypes } from '@penguinhouse/savannah-io';
import { ICreateChildEvents } from '@penguinhouse/retreat-es';
import { IRetreatRepository } from '../persistence/interfaces';
import Retreat from './Retreat';
import { IRegisterRetreat } from './interfaces';

export default class RetreatRegistry implements IRegisterRetreat {
  repository: IRetreatRepository;

  constructor(repository: IRetreatRepository) {
    this.repository = repository;
  }

  registerRetreat(context: ICreateChildEvents, retreat: Retreat) {
    return this.repository.registerRetreat(retreat).tap(() => {
      context.createChildEvent(IOTypes.EventActionEnum.RetreatRegistered, retreat.toJSON());
    });
  }
}
