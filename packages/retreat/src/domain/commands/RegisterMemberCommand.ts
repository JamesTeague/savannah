import { IOTypes } from '@penguinhouse/savannah-io';
import { ICommandExecutionContext } from '@penguinhouse/retreat-es';
import { IProcessable, IRegisterMember } from '../interfaces';
import Member from '../Member';

export default class RegisterMemberCommand implements IProcessable {
  private readonly id: string;
  private readonly displayName: string;
  private readonly email: string;
  private readonly retreatId: string;
  private registry: IRegisterMember;

  constructor(command: IOTypes.RegisterMember, id: string, registry: IRegisterMember) {
    this.id = id;
    this.displayName = command.displayName;
    this.email = command.email;
    this.retreatId = command.retreatId;
    this.registry = registry;
  }

  process(context: ICommandExecutionContext) {
    const member = Member.fromInitial(this.id, this.displayName, this.email, this.retreatId);

    return this.registry.registerMember(context, member).tap(() => {
      context.createCommandResult(true, {
        code: IOTypes.ResponseCode.Success,
        resourceId: member.id,
      });
    });
  }
}
