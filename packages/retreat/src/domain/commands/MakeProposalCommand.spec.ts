import { Command, User } from '@penguinhouse/retreat-es';
import { IOTypes } from '@penguinhouse/savannah-io';
import uuid from 'uuid/v4';
import moment from 'moment';
import MakeProposalCommand from './MakeProposalCommand';
import Member from '../Member';
jest.mock('@penguinhouse/retreat-es');

describe('MakeProposalCommand', () => {
  it('processes the command', async () => {
    const command = new Command({
      id: uuid(),
      timestamp: moment(),
      action: 'test-action',
      user: User.dummy(),
      data: { testField: 'test' },
    });
    const city = {
      address: '',
      country: {
        abbreviation: '',
        name: '',
      },
      id: '',
      name: '',
      state: {
        abbreviation: '',
        name: '',
      },
      url: '',
    };
    const member = Member.fromInitial('0', 'testName', 'testEmail', '0');
    const makeProposalCommand = new MakeProposalCommand(member, city, '0');
    const commandSpy = jest.spyOn(command, 'createCommandResult');
    const memberSpy = jest.spyOn(member, 'makeProposal');

    await makeProposalCommand.process(command);

    expect(commandSpy).toHaveBeenCalledWith(true, {
      code: IOTypes.ResponseCode.Success,
      resourceId: member.id,
    });
    expect(memberSpy).toHaveBeenCalledWith(command, city, '0');
  });
});
