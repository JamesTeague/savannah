import { IOTypes } from '@penguinhouse/savannah-io';
import { ICommandExecutionContext } from '@penguinhouse/retreat-es';
import { IProcessable, IRegisterRetreat } from '../interfaces';
import Retreat from '../Retreat';

export default class RegisterRetreatCommand implements IProcessable {
  private readonly id: string;
  private readonly name: string;
  private registry: IRegisterRetreat;

  constructor(command: IOTypes.RegisterRetreat, registry: IRegisterRetreat) {
    this.id = command.retreatId;
    this.name = command.name;
    this.registry = registry;
  }

  process(context: ICommandExecutionContext) {
    const retreat = Retreat.fromInitial(this.id, this.name);

    return this.registry.registerRetreat(context, retreat).tap(() =>
      context.createCommandResult(true, {
        code: IOTypes.ResponseCode.Success,
        resourceId: retreat.id,
      })
    );
  }
}
