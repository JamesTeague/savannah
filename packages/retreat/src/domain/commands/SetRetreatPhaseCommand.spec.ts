import { Command, User } from '@penguinhouse/retreat-es';
import { IOTypes } from '@penguinhouse/savannah-io';
import uuid from 'uuid/v4';
import moment from 'moment';
import SetRetreatPhaseCommand from './SetRetreatPhaseCommand';
import Retreat from '../Retreat';
jest.mock('@penguinhouse/retreat-es');

describe('SetRetreatPhaseCommand', () => {
  const context = new Command({
    id: uuid(),
    timestamp: moment(),
    action: 'test-action',
    user: User.dummy(),
    data: { testField: 'test' },
  });

  it('processes the command', async () => {
    const command = {
      retreatId: '0',
      phase: IOTypes.Phase.Voting,
    };

    const retreat = Retreat.fromInitial('0', 'retreat');

    const rescindVoteCommand = new SetRetreatPhaseCommand(command, retreat);
    const retreatSpy = jest.spyOn(retreat, 'setPhase');
    const contextSpy = jest.spyOn(context, 'createCommandResult');

    await rescindVoteCommand.process(context);

    expect(contextSpy).toHaveBeenCalledWith(true, {
      code: IOTypes.ResponseCode.Success,
      resourceId: command.retreatId,
    });
    expect(retreatSpy).toHaveBeenCalledWith(context, command.phase);
  });
});
