import Bluebird from 'bluebird';
import { ICommandExecutionContext } from '@penguinhouse/retreat-es';
import { IProcessable } from '../interfaces';
import Member from '../Member';
import { IOTypes } from '@penguinhouse/savannah-io';

export default class CastVoteCommand implements IProcessable {
  private member: Member;
  private readonly proposalId: string;
  private readonly retreatId: string;
  private readonly points: number;

  constructor(member: Member, proposalId: string, points: number, retreatId: string) {
    this.member = member;
    this.proposalId = proposalId;
    this.points = points;
    this.retreatId = retreatId;
  }

  process(context: ICommandExecutionContext) {
    this.member.castVote(context, this.proposalId, this.points, this.retreatId);

    context.createCommandResult(true, {
      code: IOTypes.ResponseCode.Success,
      resourceId: this.member.id,
    });

    return Bluebird.resolve();
  }
}
