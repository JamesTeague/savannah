import { Command, User } from '@penguinhouse/retreat-es';
import { IOTypes } from '@penguinhouse/savannah-io';
import Knex from 'knex';
import mockDb from 'mock-knex';
import { NullLog } from 'stoolie';
import uuid from 'uuid/v4';
import MemberRegistry from '../MemberRegistry';
import { KnexMemberRepository } from '../../persistence';
import RegisterMemberCommand from './RegisterMemberCommand';
import moment from 'moment';
import Member from '../Member';
jest.mock('@penguinhouse/retreat-es');

describe('RegisterMemberCommand', () => {
  const knex = Knex({ client: 'pg' });
  const context = new Command({
    id: uuid(),
    timestamp: moment(),
    action: 'test-action',
    user: User.dummy(),
    data: { testField: 'test' },
  });
  beforeEach(() => {
    mockDb.mock(knex);
  });

  afterEach(() => {
    mockDb.unmock(knex);
  });

  it('processes the command', async () => {
    const command = {
      displayName: '',
      email: '',
      retreatId: '0',
    };
    const member = Member.fromInitial('0', command.displayName, command.email, command.retreatId);

    const registry = new MemberRegistry(new KnexMemberRepository(knex, NullLog));
    const registerMemberCommand = new RegisterMemberCommand(command, '0', registry);
    const registrySpy = jest.spyOn(registry, 'registerMember');
    const contextSpy = jest.spyOn(context, 'createCommandResult');

    await registerMemberCommand.process(context);

    expect(contextSpy).toHaveBeenCalledWith(true, {
      code: IOTypes.ResponseCode.Success,
      resourceId: '0',
    });
    expect(registrySpy).toHaveBeenCalledWith(context, member);
  });
});
