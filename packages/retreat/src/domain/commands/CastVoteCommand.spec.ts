import { Command, User } from '@penguinhouse/retreat-es';
import { IOTypes } from '@penguinhouse/savannah-io';
import uuid from 'uuid/v4';
import moment from 'moment';
import CastVoteCommand from './CastVoteCommand';
import Member from '../Member';
jest.mock('@penguinhouse/retreat-es');

describe('CastVoteCommand', () => {
  it('processes the command', async () => {
    const command = new Command({
      id: uuid(),
      timestamp: moment(),
      action: 'test-action',
      user: User.dummy(),
      data: { testField: 'test' },
    });
    const member = Member.fromInitial('0', 'testName', 'testEmail', '0');
    const castVoteCommand = new CastVoteCommand(member, '0', 0, '0');
    const commandSpy = jest.spyOn(command, 'createCommandResult');
    const memberSpy = jest.spyOn(member, 'castVote');

    await castVoteCommand.process(command);

    expect(commandSpy).toHaveBeenCalledWith(true, {
      code: IOTypes.ResponseCode.Success,
      resourceId: '0',
    });
    expect(memberSpy).toHaveBeenCalledWith(command, '0', 0, '0');
  });
});
