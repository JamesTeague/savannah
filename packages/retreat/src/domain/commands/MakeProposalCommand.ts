import Bluebird from 'bluebird';
import { IOTypes } from '@penguinhouse/savannah-io';
import { ICommandExecutionContext } from '@penguinhouse/retreat-es';
import { IProcessable } from '../interfaces';
import Member from '../Member';

export default class MakeProposalCommand implements IProcessable {
  private member: Member;
  private readonly city: IOTypes.City;
  private readonly retreatId: string;

  constructor(member: Member, city: IOTypes.City, retreatId: string) {
    this.member = member;
    this.city = city;
    this.retreatId = retreatId;
  }

  process(context: ICommandExecutionContext) {
    this.member.makeProposal(context, this.city, this.retreatId);

    context.createCommandResult(true, {
      code: IOTypes.ResponseCode.Success,
      resourceId: this.member.id,
    });

    return Bluebird.resolve();
  }
}
