import Bluebird from 'bluebird';
import { IOTypes } from '@penguinhouse/savannah-io';
import { ICommandExecutionContext } from '@penguinhouse/retreat-es';
import { IProcessable, IChangePhase } from '../interfaces';

export default class SetRetreatPhaseCommand implements IProcessable {
  id: string;
  phase: IOTypes.Phase;
  retreat: IChangePhase;

  constructor(command: IOTypes.SetRetreatPhase, retreat: IChangePhase) {
    this.id = command.retreatId;
    this.phase = command.phase;
    this.retreat = retreat;
  }

  process(context: ICommandExecutionContext) {
    this.retreat.setPhase(context, this.phase);

    context.createCommandResult(true, {
      code: IOTypes.ResponseCode.Success,
      resourceId: this.id,
    });

    return Bluebird.resolve();
  }
}
