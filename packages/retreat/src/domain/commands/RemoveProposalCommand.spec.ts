import { Command, User } from '@penguinhouse/retreat-es';
import { IOTypes } from '@penguinhouse/savannah-io';
import uuid from 'uuid/v4';
import moment from 'moment';
import Member from '../Member';
import RemoveProposalCommand from './RemoveProposalCommand';
jest.mock('@penguinhouse/retreat-es');

describe('RemoveProposalCommand', () => {
  const context = new Command({
    id: uuid(),
    timestamp: moment(),
    action: 'test-action',
    user: User.dummy(),
    data: { testField: 'test' },
  });

  it('processes the command', async () => {
    const member = Member.fromInitial('0', 'testName', 'testEmail', '0');

    const removeProposalCommand = new RemoveProposalCommand(member, '0');
    const memberSpy = jest.spyOn(member, 'removeProposal');
    const contextSpy = jest.spyOn(context, 'createCommandResult');

    await removeProposalCommand.process(context);

    expect(contextSpy).toHaveBeenCalledWith(true, {
      code: IOTypes.ResponseCode.Success,
      resourceId: member.id,
    });
    expect(memberSpy).toHaveBeenCalledWith(context, '0');
  });
});
