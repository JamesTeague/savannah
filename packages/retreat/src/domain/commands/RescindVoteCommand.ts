import Bluebird from 'bluebird';
import { IOTypes } from '@penguinhouse/savannah-io';
import { ICommandExecutionContext } from '@penguinhouse/retreat-es';
import { IProcessable } from '../interfaces';
import Member from '../Member';

export default class RescindVoteCommand implements IProcessable {
  private member: Member;
  private readonly voteId: string;

  constructor(member: Member, voteId: string) {
    this.member = member;
    this.voteId = voteId;
  }

  process(context: ICommandExecutionContext) {
    this.member.rescindVote(context, this.voteId);

    context.createCommandResult(true, {
      code: IOTypes.ResponseCode.Success,
      resourceId: this.member.id,
    });

    return Bluebird.resolve();
  }
}
