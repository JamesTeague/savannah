import Bluebird from 'bluebird';
import { IOTypes } from '@penguinhouse/savannah-io';
import { ICommandExecutionContext } from '@penguinhouse/retreat-es';
import { IProcessable } from '../interfaces';
import Member from '../Member';

export default class RemoveProposalCommand implements IProcessable {
  private member: Member;
  private readonly proposalId: string;

  constructor(member: Member, proposalId: string) {
    this.member = member;
    this.proposalId = proposalId;
  }

  process(context: ICommandExecutionContext) {
    this.member.removeProposal(context, this.proposalId);

    context.createCommandResult(true, {
      code: IOTypes.ResponseCode.Success,
      resourceId: this.member.id,
    });

    return Bluebird.resolve();
  }
}
