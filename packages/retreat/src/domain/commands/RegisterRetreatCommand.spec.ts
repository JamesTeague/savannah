import { Command, User } from '@penguinhouse/retreat-es';
import { IOTypes } from '@penguinhouse/savannah-io';
import Knex from 'knex';
import mockDb from 'mock-knex';
import { NullLog } from 'stoolie';
import uuid from 'uuid/v4';
import { KnexRetreatRepository } from '../../persistence';
import moment from 'moment';
import RetreatRegistry from '../RetreatRegistry';
import RegisterRetreatCommand from './RegisterRetreatCommand';
import Retreat from '../Retreat';
jest.mock('@penguinhouse/retreat-es');

describe('RegisterRetreatCommand', () => {
  const knex = Knex({ client: 'pg' });
  const context = new Command({
    id: uuid(),
    timestamp: moment(),
    action: 'test-action',
    user: User.dummy(),
    data: { testField: 'test' },
  });
  beforeEach(() => {
    mockDb.mock(knex);
  });

  afterEach(() => {
    mockDb.unmock(knex);
  });

  it('processes the command', async () => {
    const command = {
      name: '',
      retreatId: '0',
    };
    const retreat = Retreat.fromInitial(command.retreatId, command.name);

    const registry = new RetreatRegistry(new KnexRetreatRepository(knex, NullLog));
    const registerRetreatCommand = new RegisterRetreatCommand(command, registry);
    const registrySpy = jest.spyOn(registry, 'registerRetreat');
    const contextSpy = jest.spyOn(context, 'createCommandResult');

    await registerRetreatCommand.process(context);

    expect(contextSpy).toHaveBeenCalledWith(true, {
      code: IOTypes.ResponseCode.Success,
      resourceId: command.retreatId,
    });
    expect(registrySpy).toHaveBeenCalledWith(context, retreat);
  });
});
