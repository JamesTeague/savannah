import { ICommandExecutionContext, User } from '@penguinhouse/retreat-es';
import { IOTypes } from '@penguinhouse/savannah-io';

export interface IRetreatService {
  handleRegisterRetreatCommand(
    context: ICommandExecutionContext,
    user: User,
    command: IOTypes.RegisterRetreat
  ): Promise<void>;
  handleSetRetreatPhaseCommand(
    context: ICommandExecutionContext,
    user: User,
    command: IOTypes.SetRetreatPhase
  ): Promise<void>;
}
