import { User, ICommandExecutionContext } from '@penguinhouse/retreat-es';
import { IOTypes } from '@penguinhouse/savannah-io';

export interface IMemberService {
  handleCastVoteCommand(
    context: ICommandExecutionContext,
    user: User,
    command: IOTypes.CastVote
  ): Promise<void>;
  handleRescindVoteCommand(
    context: ICommandExecutionContext,
    user: User,
    command: IOTypes.RescindVote
  ): Promise<void>;
  handleMakeProposalCommand(
    context: ICommandExecutionContext,
    user: User,
    command: IOTypes.MakeProposal
  ): Promise<void>;
  handleRemoveProposalCommand(
    context: ICommandExecutionContext,
    user: User,
    command: IOTypes.RemoveProposal
  ): Promise<void>;
  handleRegisterMember(
    context: ICommandExecutionContext,
    user: User,
    command: IOTypes.RegisterMember
  ): Promise<void>;
}
