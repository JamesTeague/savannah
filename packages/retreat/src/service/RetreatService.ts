// TODO - remove check below
// @ts-nocheck
import { ILogger } from 'stoolie';
import { ICommandExecutionContext, User } from '@penguinhouse/retreat-es';
import { IOTypes } from '@penguinhouse/savannah-io';
import { IRetreatService } from './interfaces';
import { IRetreatRepository } from '../persistence/interfaces';
import RetreatRegistry from '../domain/RetreatRegistry';
import RegisterRetreatCommand from '../domain/commands/RegisterRetreatCommand';
import { SetRetreatPhaseCommand } from '../domain/commands';

export default class RetreatService implements IRetreatService {
  repository: IRetreatRepository;
  entry: ILogger;

  constructor(repository: IRetreatRepository, logger: ILogger) {
    this.repository = repository;
    this.entry = logger.withCategory('service').withFields({ type: 'retreat-service' });

    this.entry.debug('Retreat service created.');
  }

  handleRegisterRetreatCommand(
    context: ICommandExecutionContext,
    user: User,
    command: IOTypes.RegisterRetreat
  ) {
    this.entry.withFields({ command }).verbose('Handling registering retreat.');

    const registry = new RetreatRegistry(this.repository);

    const registerRetreatCommand = new RegisterRetreatCommand(command, registry);

    return registerRetreatCommand.process(context);
  }

  async handleSetRetreatPhaseCommand(
    context: ICommandExecutionContext,
    user: User,
    command: IOTypes.SetRetreatPhase
  ) {
    this.entry.withFields({ command }).verbose('Handling setting retreat phase.');

    const retreat = await this.repository.getRetreatByRetreatId(command.retreatId);

    if (!retreat) {
      throw new Error('Retreat was not found with given id.');
    }

    const setRetreatPhaseCommand = new SetRetreatPhaseCommand(command, retreat);

    return setRetreatPhaseCommand.process(context);
  }
}
