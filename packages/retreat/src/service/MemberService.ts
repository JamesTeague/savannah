// TODO - remove check below
// @ts-nocheck
import Bluebird from 'bluebird';
import uuid from 'uuid/v4';
import { User, ICommandExecutionContext } from '@penguinhouse/retreat-es';
import { IOTypes } from '@penguinhouse/savannah-io';
import { ILogger } from 'stoolie';
import { IMemberService } from './interfaces';
import { IMemberRepository } from '../persistence/interfaces';
import { CastVoteCommand, MakeProposalCommand } from '../domain';
import RegisterMemberCommand from '../domain/commands/RegisterMemberCommand';
import MemberRegistry from '../domain/MemberRegistry';

export default class MemberService implements IMemberService {
  repository: IMemberRepository;
  entry: ILogger;

  constructor(repository: IMemberRepository, logger: ILogger) {
    this.repository = repository;
    this.entry = logger.withCategory('service').withFields({ type: 'member-service' });

    this.entry.debug('Member service created.');
  }

  async handleMakeProposalCommand(
    context: ICommandExecutionContext,
    user: User,
    command: IOTypes.MakeProposal
  ) {
    this.entry.withFields({ command }).verbose('Handling making a proposal.');

    const { memberId, city, retreatId } = command;

    const member = await this.repository.getMemberByMemberId(memberId);

    if (!member) {
      throw new Error('Member was not found with given id.');
    }

    const makeProposalCommand = new MakeProposalCommand(member, city, retreatId);

    await makeProposalCommand.process(context);

    return this.repository.updateMember(member);
  }

  async handleCastVoteCommand(
    context: ICommandExecutionContext,
    user: User,
    command: IOTypes.CastVote
  ) {
    this.entry.withFields({ command }).verbose('Handling casting a vote.');

    const { memberId, proposalId, points, retreatId } = command;
    const member = await this.repository.getMemberByMemberId(memberId);

    if (!member) {
      throw new Error('Member was not found with given id.');
    }

    const castVoteCommand = new CastVoteCommand(member, proposalId, points, retreatId);

    await castVoteCommand.process(context);

    return this.repository.updateMember(member);
  }

  async handleRegisterMember(
    context: ICommandExecutionContext,
    user: User,
    command: IOTypes.RegisterMember
  ) {
    this.entry.withFields({ command }).verbose('Handling registering member.');

    const registry = new MemberRegistry(this.repository);

    const registerMemberCommand = new RegisterMemberCommand(command, uuid(), registry);

    return registerMemberCommand.process(context);
  }

  handleRemoveProposalCommand(
    context: ICommandExecutionContext,
    user: User,
    command: IOTypes.RemoveProposal
  ) {
    this.entry.warn('Removing proposals have not been implemented.');
    return Bluebird.resolve();
  }

  handleRescindVoteCommand(
    context: ICommandExecutionContext,
    user: User,
    command: IOTypes.RescindVote
  ) {
    this.entry.warn('Rescinding votes have not been implemented.');
    return Bluebird.resolve();
  }
}
