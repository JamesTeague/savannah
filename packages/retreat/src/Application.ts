import { IMemberService, IRetreatService } from './service/interfaces';

export type AppState = {
  memberService: IMemberService;
  retreatService: IRetreatService;
};
