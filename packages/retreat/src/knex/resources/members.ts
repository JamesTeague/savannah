import retreats from './retreats';

const james = {
  displayName: 'James Teague II',
  email: 'jtteague13@gmail.com',
  points: 10,
  id: '773c6fd3-f5f5-485c-94b1-2eb24de0eb41',
  proposals: [],
  votes: [],
  retreatIds: [retreats[0].id],
};

const testUser = {
  displayName: 'Test User',
  email: 'user@gmail.com',
  points: 10,
  id: 'c11d0281-2fc2-49e8-80f8-c43aaf93ae7a',
  proposals: [],
  votes: [],
  retreatIds: [retreats[0].id],
};

export default [james, testUser];
