const whatCheer = {
  address: 'What Cheer, IA, United States of America',
  country: {
    abbreviation: 'US',
    name: 'United States of America',
  },
  id: 'a940f03d-8116-4777-86c8-1142debb6a26',
  name: 'What Cheer',
  state: {
    abbreviation: 'IA',
    name: 'Iowa',
  },
  center: [-92.3546, 41.4014],
};

const stateCenter = {
  address: 'State Center, IA, United States of America',
  country: {
    abbreviation: 'US',
    name: 'United States of America',
  },
  id: '40866b6f-4959-496d-b245-d99d86413ca2',
  name: 'State Center',
  state: {
    abbreviation: 'IA',
    name: 'Iowa',
  },
  center: [-93.1634, 42.0168],
};

const portland = {
  address: 'Portland, ME, United States of America',
  country: {
    abbreviation: 'US',
    name: 'United States of America',
  },
  id: '64b9f890-a1cb-446b-a09e-c20d78948e99',
  name: 'Portland',
  state: {
    abbreviation: 'ME',
    name: 'Maine',
  },
  center: [-70.2549, 43.661],
};

const hilo = {
  address: 'Hilo, HI, United States of America',
  country: {
    abbreviation: 'US',
    name: 'United States of America',
  },
  id: 'ecbc68d2-b24c-449a-b0ef-aa7a82f18198',
  name: 'Hilo',
  state: {
    abbreviation: 'HI',
    name: 'Hawaii',
  },
  center: [-155.0814, 19.7068],
};

const dallas = {
  address: 'Dallas, TX, United States of America',
  country: {
    abbreviation: 'US',
    name: 'United States of America',
  },
  id: '1c8b8032-16f1-418a-a88b-04734a5330fa',
  name: 'Dallas',
  state: {
    abbreviation: 'TX',
    name: 'Texas',
  },
  center: [-96.7969, 32.7763],
};

export default [whatCheer, stateCenter, portland, hilo, dallas];
