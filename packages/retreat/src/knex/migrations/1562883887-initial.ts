import Knex from 'knex';

const ON_UPDATE_TIMESTAMP_FUNCTION = `
  CREATE OR REPLACE FUNCTION on_update_timestamp()
  RETURNS trigger AS $$
  BEGIN
    NEW.updated_at = now();
    RETURN NEW;
  END;
$$ language 'plpgsql';
`;

const DROP_ON_UPDATE_TIMESTAMP_FUNCTION = `DROP FUNCTION on_update_timestamp`;

const onUpdateTrigger = table => `
    CREATE TRIGGER ${table}_updated_at
    BEFORE UPDATE ON ${table}
    FOR EACH ROW
    EXECUTE PROCEDURE on_update_timestamp();
  `;

const dropTrigger = table => `DROP TRIGGER IF EXISTS ${table}_updated_at ON ${table}`;

export function up(knex: Knex): Promise<any> {
  return knex.raw(ON_UPDATE_TIMESTAMP_FUNCTION).then(() =>
    knex.schema
      .createTable('members', tableBuilder => {
        tableBuilder
          .text('id')
          .notNullable()
          .primary();
        tableBuilder.jsonb('body').notNullable();
        tableBuilder.timestamp('created_at').defaultTo(knex.fn.now());
        tableBuilder.timestamp('updated_at').defaultTo(knex.fn.now());
      })
      .createTable('retreats', tableBuilder => {
        tableBuilder
          .uuid('id')
          .notNullable()
          .primary();
        tableBuilder.jsonb('body').notNullable();
        tableBuilder.timestamp('created_at').defaultTo(knex.fn.now());
        tableBuilder.timestamp('updated_at').defaultTo(knex.fn.now());
      })
      .then(() => {
        knex.raw(onUpdateTrigger('members'));
        knex.raw(onUpdateTrigger('retreats'));
      })
  );
}

export function down(knex: Knex): Promise<any> {
  return knex.raw(DROP_ON_UPDATE_TIMESTAMP_FUNCTION).then(() =>
    knex.schema
      .dropTableIfExists('members')
      .dropTableIfExists('retreats')
      .then(() => {
        knex.raw(dropTrigger('members'));
        knex.raw(dropTrigger('retreats'));
      })
  );
}
