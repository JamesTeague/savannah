import Bluebird from 'bluebird';
import Knex from 'knex';
import { proposals, members, votes, retreats } from '../resources';

export const seed = (knex: Knex): Bluebird<any> => {
  return Bluebird.try(async () => {
    await Bluebird.all([knex('members').delete(), knex('retreats').delete()]);

    const membersDb: any[] = [];
    members.forEach(member => membersDb.push({ ...member }));

    proposals.forEach(proposal => {
      const matchingMember = membersDb.filter(member => member.id === proposal.memberId)[0];
      matchingMember.proposals.push({ ...proposal });
    });

    votes.forEach(vote => {
      const matchingMember = membersDb.filter(member => member.id === vote.memberId)[0];
      matchingMember.votes.push({ ...vote });
    });

    return Bluebird.all([
      ...membersDb.map(member =>
        knex('members').insert({
          id: member.id,
          body: { ...member },
        })
      ),
      ...retreats.map(retreat =>
        knex('retreats').insert({
          id: retreat.id,
          body: { ...retreat },
        })
      ),
    ]);
  });
};

export default {
  seed,
};
