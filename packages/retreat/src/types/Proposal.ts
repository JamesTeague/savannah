import { IOTypes } from '@penguinhouse/savannah-io';

export type Proposal = {
  readonly id: string;
  readonly city: IOTypes.City;
};
