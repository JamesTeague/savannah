export type Vote = {
  id: string;
  proposalId: string;
  points: number;
};
