import { User, ICommandExecutionContext } from '@penguinhouse/retreat-es';
import { IOTypes } from '@penguinhouse/savannah-io';
import { AppState } from '../Application';
import { CreateSavannahCommandTransport } from './SavannahTransporter';
import { ISavannahTransporter } from './SavannahTransporter';

function RescindVoteTransportHandler(
  context: ICommandExecutionContext,
  user: User,
  payload: IOTypes.RescindVote,
  state: AppState
): Promise<void> {
  return state.memberService.handleRescindVoteCommand(context, user, payload);
}

const RescindVoteTransport: ISavannahTransporter<AppState> = CreateSavannahCommandTransport({
  action: IOTypes.CommandActionEnum.RescindVote,
  payloadType: null,
  handler: RescindVoteTransportHandler,
});

export default RescindVoteTransport;
