import { User, ICommandExecutionContext } from '@penguinhouse/retreat-es';
import { IOTypes } from '@penguinhouse/savannah-io';
import { AppState } from '../Application';
import { CreateSavannahCommandTransport } from './SavannahTransporter';
import { ISavannahTransporter } from './SavannahTransporter';

function RegisterMemberTransportHandler(
  context: ICommandExecutionContext,
  user: User,
  payload: IOTypes.RegisterMember,
  state: AppState
): Promise<void> {
  return state.memberService.handleRegisterMember(context, user, payload);
}

const RegisterMemberTransport: ISavannahTransporter<AppState> = CreateSavannahCommandTransport({
  action: IOTypes.CommandActionEnum.RegisterMember,
  payloadType: null,
  handler: RegisterMemberTransportHandler,
});

export default RegisterMemberTransport;
