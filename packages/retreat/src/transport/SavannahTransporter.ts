import { User, ICommandExecutionContext, ISavannahPlatform } from '@penguinhouse/retreat-es';
import { Base } from '@penguinhouse/savannah-io';

// TODO - experiment with returning something from savannah (Rx.Subscription)
export interface ISavannahTransporter<TState> {
  bind(anna: ISavannahPlatform, state: TState): void;
}

type CommandHandler<TPayload, TResult, TState> = (
  context: ICommandExecutionContext,
  user: User,
  payloadType: TPayload,
  state: TState
) => Promise<TResult>;

type CreateSavannahCommandTransportArgs<TPayload, TResult, TState> = {
  action: string;
  payloadType: Base.Nullable<TPayload>;
  handler: CommandHandler<TPayload, TResult, TState>;
};

function CreateSavannahCommandTransport<TPayload, TResult, TState>(
  args: CreateSavannahCommandTransportArgs<TPayload, TResult, TState>
): ISavannahTransporter<TState> {
  return {
    bind(savannah: ISavannahPlatform, state: TState) {
      const consumeArgs = {
        action: args.action,
        payloadType: args.payloadType,
        handler: (context, user, payloadType) => args.handler(context, user, payloadType, state),
      };

      // TODO - consider subscribing here in the app not inside call
      savannah.onCommand(consumeArgs);
    },
  };
}

export { CreateSavannahCommandTransport };
