import * as Rx from 'rxjs';
import { ISavannahPlatform } from '@penguinhouse/retreat-es';
import CastVoteTransport from './CastVoteTransport';
import MakeProposalTransport from './MakeProposalTransport';
import RegisterMemberTransport from './RegisterMemberTransport';
import RegisterRetreatTransport from './RegisterRetreatTransport';
import RemoveProposalTransport from './RemoveProposalTransport';
import RescindVoteTransport from './RescindVoteTransport';
import SetRetreatPhaseTransport from './SetRetreatPhaseTransport';
import { AppState } from '../Application';

const SavannahTransports = [
  CastVoteTransport,
  MakeProposalTransport,
  RegisterMemberTransport,
  RegisterRetreatTransport,
  RemoveProposalTransport,
  RescindVoteTransport,
  SetRetreatPhaseTransport,
];

export type SavannahTransportConfiguration<TState> = {
  type: 'savannah';
  context: TState;
  connection: {
    savannah: ISavannahPlatform;
  };
};

function ConfigureSavannahTransport<TState extends AppState>(
  config: SavannahTransportConfiguration<TState>
) {
  const { connection, context } = config;
  const { savannah } = connection;

  const subscription: Rx.Subscription = SavannahTransports.reduce(
    (acc, transport) => acc.add(transport.bind(savannah, context)),
    new Rx.Subscription()
  );

  return {
    unbind() {
      subscription.unsubscribe();
    },
  };
}

export default ConfigureSavannahTransport;
