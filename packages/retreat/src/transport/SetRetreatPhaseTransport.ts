import { User, ICommandExecutionContext } from '@penguinhouse/retreat-es';
import { IOTypes } from '@penguinhouse/savannah-io';
import { AppState } from '../Application';
import { CreateSavannahCommandTransport } from './SavannahTransporter';
import { ISavannahTransporter } from './SavannahTransporter';

function SetRetreatPhaseTransportHandler(
  context: ICommandExecutionContext,
  user: User,
  payload: IOTypes.SetRetreatPhase,
  state: AppState
): Promise<void> {
  return state.retreatService.handleSetRetreatPhaseCommand(context, user, payload);
}

const SetRetreatPhaseTransport: ISavannahTransporter<AppState> = CreateSavannahCommandTransport({
  action: IOTypes.CommandActionEnum.SetRetreatPhase,
  payloadType: null,
  handler: SetRetreatPhaseTransportHandler,
});

export default SetRetreatPhaseTransport;
