import { User, ICommandExecutionContext } from '@penguinhouse/retreat-es';
import { IOTypes } from '@penguinhouse/savannah-io';
import { AppState } from '../Application';
import { CreateSavannahCommandTransport } from './SavannahTransporter';
import { ISavannahTransporter } from './SavannahTransporter';

function RemoveProposalTransportHandler(
  context: ICommandExecutionContext,
  user: User,
  payload: IOTypes.RemoveProposal,
  state: AppState
): Promise<void> {
  return state.memberService.handleRemoveProposalCommand(context, user, payload);
}

const RemoveProposalTransport: ISavannahTransporter<AppState> = CreateSavannahCommandTransport({
  action: IOTypes.CommandActionEnum.RemoveProposal,
  payloadType: null,
  handler: RemoveProposalTransportHandler,
});

export default RemoveProposalTransport;
