import { User, ICommandExecutionContext } from '@penguinhouse/retreat-es';
import { IOTypes } from '@penguinhouse/savannah-io';
import { AppState } from '../Application';
import { CreateSavannahCommandTransport } from './SavannahTransporter';
import { ISavannahTransporter } from './SavannahTransporter';

function RegisterRetreatTransportHandler(
  context: ICommandExecutionContext,
  user: User,
  payload: IOTypes.RegisterRetreat,
  state: AppState
): Promise<void> {
  return state.retreatService.handleRegisterRetreatCommand(context, user, payload);
}

const RegisterRetreatTransport: ISavannahTransporter<AppState> = CreateSavannahCommandTransport({
  action: IOTypes.CommandActionEnum.RegisterRetreat,
  payloadType: null,
  handler: RegisterRetreatTransportHandler,
});

export default RegisterRetreatTransport;
