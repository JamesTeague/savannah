import { User, ICommandExecutionContext } from '@penguinhouse/retreat-es';
import { IOTypes } from '@penguinhouse/savannah-io';
import { AppState } from '../Application';
import { CreateSavannahCommandTransport } from './SavannahTransporter';
import { ISavannahTransporter } from './SavannahTransporter';

function CastVoteTransportHandler(
  context: ICommandExecutionContext,
  user: User,
  payload: IOTypes.CastVote,
  state: AppState
): Promise<void> {
  return state.memberService.handleCastVoteCommand(context, user, payload);
}

const CastVoteTransport: ISavannahTransporter<AppState> = CreateSavannahCommandTransport({
  action: IOTypes.CommandActionEnum.CastVote,
  payloadType: null,
  handler: CastVoteTransportHandler,
});

export default CastVoteTransport;
