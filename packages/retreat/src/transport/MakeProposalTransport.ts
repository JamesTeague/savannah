import { User, ICommandExecutionContext } from '@penguinhouse/retreat-es';
import { IOTypes } from '@penguinhouse/savannah-io';
import { AppState } from '../Application';
import { CreateSavannahCommandTransport } from './SavannahTransporter';
import { ISavannahTransporter } from './SavannahTransporter';

function MakeProposalTransportHandler(
  context: ICommandExecutionContext,
  user: User,
  payload: IOTypes.MakeProposal,
  state: AppState
): Promise<void> {
  return state.memberService.handleMakeProposalCommand(context, user, payload);
}

const MakeProposalTransport: ISavannahTransporter<AppState> = CreateSavannahCommandTransport({
  action: IOTypes.CommandActionEnum.MakeProposal,
  payloadType: null,
  handler: MakeProposalTransportHandler,
});

export default MakeProposalTransport;
