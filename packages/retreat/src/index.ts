import Knex from 'knex';
import * as Rx from 'rxjs';
import * as RxOp from 'rxjs/operators';
import * as Path from 'path';
import stoolie from 'stoolie';
import { SavannahPlatform } from '@penguinhouse/retreat-es';
import ConfigureApplication from './ApplicationConfiguration';
import ConfigureSavannahTransport from './transport';
import { KnexMemberRepository, KnexRetreatRepository } from './persistence';
import { MemberService, RetreatService } from './service';
import { AppState } from './Application';

const config = ConfigureApplication(process.env);

/** Knex */
const KNEX_DIR = Path.resolve(__dirname, 'knex');
const knex = Knex({
  client: 'pg',
  connection: config.db,
  migrations: {
    directory: Path.resolve(KNEX_DIR, 'migrations'),
  },
  seeds: {
    directory: Path.resolve(KNEX_DIR, 'seeds'),
  },
});

const logger = stoolie('retreat', config.logLevel);
const savannah = new SavannahPlatform({
  appName: 'retreat',
  ...config.db,
  logLevel: config.logLevel,
});
let transport;

const main = (): Rx.Observable<any> => {
  savannah.connectNotifier().then(success => {
    if (!success) {
      throw new Error('Failed to connect to platform.');
    }

    const memberRepository = new KnexMemberRepository(knex, logger);
    const retreatRepository = new KnexRetreatRepository(knex, logger);
    const memberService = new MemberService(memberRepository, logger);
    const retreatService = new RetreatService(retreatRepository, logger);

    const state: AppState = {
      memberService,
      retreatService,
    };

    transport = ConfigureSavannahTransport({
      type: 'savannah',
      context: state,
      connection: { savannah },
    });

    savannah.connect();
    logger.info('Application is ready.');
  });

  return Rx.NEVER;
};

const exit = new Rx.Subject();
['SIGINT', 'SIGTERM', 'SIGHUP'].forEach((signal: any) => {
  process.on(signal, () => exit.next(true));
});

const cleanup = () => {
  return savannah.disconnect().then(() => {
    transport.unbind();
  });
};

Rx.of(knex.migrate.latest())
  .pipe(
    RxOp.mergeMap(() => main()),
    RxOp.takeUntil(exit)
  )
  .subscribe({
    next() {},
    error(error: Error) {
      cleanup().then(() => {
        logger.withError(error).error('Fatal application error.');
        process.exit(1);
      });
    },
    complete() {
      cleanup().then(() => {
        logger.info('Application exited normally.');
        process.exit(0);
      });
    },
  });
