import Knex from 'knex';
import { ILogger } from 'stoolie';
import { Base, IOTypes } from '@penguinhouse/savannah-io';
import { Member } from '../domain';
import KnexRepositoryBase from './KnexRepositoryBase';
import { IMemberRepository } from './interfaces';

type PgMember = {
  readonly id: string;
  readonly body: IOTypes.Member;
};

const pgMemberColumnObject = {
  id: '',
  body: {},
};

export default class KnexMemberRepository extends KnexRepositoryBase<PgMember>
  implements IMemberRepository {
  entry: ILogger;

  constructor(knex: Knex, logger: ILogger) {
    super(knex, logger, Object.keys(pgMemberColumnObject));

    this.entry = logger.withCategory('repository').withFields({ type: 'member-repository' });
  }

  getMemberByMemberId(memberId: string) {
    this.entry.withFields({ memberId }).verbose('Fetching user by member id.');

    const query = this.knex('members')
      .select(this.columns)
      .where({ id: memberId })
      .first();

    return this.runQuery(query).then((maybeMember: Base.Nullable<PgMember>) => {
      if (!maybeMember) {
        return null;
      }

      return Member.fromJson(maybeMember.body);
    });
  }

  addMember(member: Member) {
    this.entry.withFields({ member: member.toJSON() }).verbose('Adding member.');

    const dto = member.toJSON();
    const row = {
      id: dto.id,
      body: dto,
    };

    const query = this.knex('members').insert(row);

    return this.runQuery(query);
  }

  updateMember(member: Member) {
    this.entry.withFields({ member: member.toJSON() }).verbose('Updating member.');

    const dto = member.toJSON();
    const row = {
      id: dto.id,
      body: dto,
    };

    const query = this.knex('members')
      .where({ id: row.id })
      .update(row);

    return this.runQuery(query);
  }
}
