import Knex from 'knex';
import { ILogger } from 'stoolie';
import { Base, IOTypes } from '@penguinhouse/savannah-io';
import { IRetreatRepository } from './interfaces';
import KnexRepositoryBase from './KnexRepositoryBase';
import Retreat from '../domain/Retreat';

type PgRetreat = {
  readonly id: string;
  readonly body: IOTypes.Retreat;
};

const pgRetreatColumnObject = {
  id: '',
  body: {},
};

export default class KnexRetreatRepository extends KnexRepositoryBase<PgRetreat>
  implements IRetreatRepository {
  entry: ILogger;

  constructor(knex: Knex, logger: ILogger) {
    super(knex, logger, Object.keys(pgRetreatColumnObject));

    this.entry = logger.withCategory('repository').withFields({ type: 'retreat-repository' });
  }

  getRetreatByRetreatId(retreatId: string) {
    this.entry.withFields({ retreatId }).verbose('Retrieving retreat.');

    const query = this.knex('retreats')
      .where({ id: retreatId })
      .select(this.columns)
      .first();

    return this.runQuery(query).then((maybeRetreat: Base.Nullable<PgRetreat>) => {
      if (!maybeRetreat) {
        return null;
      }

      return Retreat.fromJson(maybeRetreat.body);
    });
  }

  registerRetreat(retreat: Retreat) {
    this.entry.withFields({ retreat: retreat.toJSON() }).verbose('Registering retreat.');

    const dto = retreat.toJSON();

    const row = {
      id: dto.id,
      body: dto,
    };

    const query = this.knex('retreats').insert(row);

    return this.runQuery(query);
  }

  updateRetreat(retreat: Retreat) {
    this.entry.withFields({ retreat: retreat.toJSON() }).verbose('Updating retreat.');

    const query = this.knex('retreats')
      .where({ id: retreat.id })
      .update(retreat.toJSON());

    return this.runQuery(query);
  }
}
