import Bluebird from 'bluebird';
import Retreat from '../../domain/Retreat';
import { Base } from '@penguinhouse/savannah-io';

export interface IRetreatRepository {
  getRetreatByRetreatId(retreatId: string): Bluebird<Base.Nullable<Retreat>>;
  registerRetreat(retreat: Retreat): Bluebird<void>;
  updateRetreat(retreat: Retreat): Bluebird<void>;
}
