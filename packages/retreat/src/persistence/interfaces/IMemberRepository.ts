import Bluebird from 'bluebird';
import Member from '../../domain/Member';
import { Base } from '@penguinhouse/savannah-io';

export interface IMemberRepository {
  getMemberByMemberId(memberId: string): Bluebird<Base.Nullable<Member>>;
  addMember(member: Member): Bluebird<void>;
  updateMember(member: Member): Bluebird<void>;
}
