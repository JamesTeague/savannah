import Knex from 'knex';
import { ILogger } from 'stoolie';
import Bluebird from 'bluebird';
import { Base } from '@penguinhouse/savannah-io';

export default class KnexRepositoryBase<T> {
  knex: Knex;
  logger: ILogger;
  columns: string[];

  constructor(knex: Knex, logger: ILogger, columns: string[]) {
    this.knex = knex;
    this.logger = logger;
    this.columns = columns;
  }

  runQuery<R>(query: Knex.QueryBuilder<R, any>): Bluebird<R> {
    const entry = this.logger.withFields({ query: query.toString() });

    entry.debug('Running query.');

    return Bluebird.resolve(query).tap((result: any) =>
      entry.withFields({ result }).debug('Query result.')
    );
  }

  firstOrDefault(input: T[], projector: (T) => boolean = () => true): Base.Nullable<T> {
    for (let i = 0; i < input.length; i += 1) {
      if (projector(input[i])) {
        return input[i];
      }
    }

    return null;
  }

  checkResultsDeterministicOrThrow(input: T[], max: number = 1): Base.Nullable<T> {
    if (input.length > max) {
      throw new Error('Non-deterministic find detected.');
    }

    return this.firstOrDefault(input);
  }
}
