import Knex from 'knex';
import mockDb from 'mock-knex';
import KnexRetreatRepository from './KnexRetreatRepository';
import { NullLog } from 'stoolie';
import { IOTypes } from '@penguinhouse/savannah-io';
import { Retreat } from '../domain';

describe('KnexRetreatRepository', () => {
  let repository: KnexRetreatRepository;
  let runQueryOriginal;
  let withFieldsSpy;
  let verboseSpy;
  let runQuerySpy;

  const columns = ['id', 'body'];
  const knex = Knex({ client: 'pg' });
  const pgRetreat = {
    id: '0',
    body: {
      name: 'test',
      phase: IOTypes.Phase.Proposal,
      id: '0',
    },
  };

  beforeEach(() => {
    mockDb.mock(knex);
    repository = new KnexRetreatRepository(knex, NullLog);
    runQueryOriginal = repository.runQuery;
    withFieldsSpy = jest.spyOn(NullLog, 'withFields');
    verboseSpy = jest.spyOn(NullLog, 'verbose');
    runQuerySpy = jest.spyOn(repository, 'runQuery');
  });

  afterEach(() => {
    repository.runQuery = runQueryOriginal;
    mockDb.unmock(knex);
  });

  it('gets a retreat by id', async () => {
    repository.runQuery = jest.fn().mockResolvedValueOnce(pgRetreat);
    runQuerySpy = jest.spyOn(repository, 'runQuery');
    const query = knex('retreats')
      .where({ id: '0' })
      .select(columns)
      .first();

    const result = await repository.getRetreatByRetreatId('0');

    expect(withFieldsSpy).toHaveBeenCalledWith({ retreatId: '0' });
    expect(verboseSpy).toHaveBeenCalledWith('Retrieving retreat.');
    expect(runQuerySpy).toHaveBeenCalledWith(query);
    expect(result).toMatchObject(Retreat.fromJson(pgRetreat.body));
  });

  it('returns null if no retreat is found', async () => {
    repository.runQuery = jest.fn().mockResolvedValueOnce(null);

    const result = await repository.getRetreatByRetreatId('0');

    expect(result).toBeNull();
  });

  it('registers a Retreat', async () => {
    const retreat = Retreat.fromInitial(pgRetreat.id, pgRetreat.body.name);
    const query = knex('retreats').insert(pgRetreat);

    await repository.registerRetreat(retreat);

    expect(withFieldsSpy).toHaveBeenCalledWith({ retreat: retreat.toJSON() });
    expect(verboseSpy).toHaveBeenCalledWith('Registering retreat.');
    expect(runQuerySpy).toHaveBeenCalledWith(query);
  });

  it('updates a retreat', async () => {
    const retreat = Retreat.fromInitial(pgRetreat.id, pgRetreat.body.name);
    const query = knex('retreats')
      .where({ id: retreat.id })
      .update(retreat.toJSON());

    await repository.updateRetreat(retreat);

    expect(withFieldsSpy).toHaveBeenCalledWith({ retreat: retreat.toJSON() });
    expect(verboseSpy).toHaveBeenCalledWith('Updating retreat.');
    expect(runQuerySpy).toHaveBeenCalledWith(query);
  });
});
