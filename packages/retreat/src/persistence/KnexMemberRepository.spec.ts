import Knex from 'knex';
import mockDb from 'mock-knex';
import KnexMemberRepository from './KnexMemberRepository';
import { NullLog } from 'stoolie';
import { Member } from '../domain';

describe('KnexMemberRepository', () => {
  let repository: KnexMemberRepository;
  let runQueryOriginal;
  let withFieldsSpy;
  let verboseSpy;
  let runQuerySpy;

  const columns = ['id', 'body'];
  const knex = Knex({ client: 'pg' });
  const pgMember = {
    id: '0',
    body: {
      displayName: 'test',
      email: 'test-email',
      points: 10,
      id: '0',
      proposals: [],
      votes: [],
      retreatIds: ['0'],
    },
  };

  beforeEach(() => {
    mockDb.mock(knex);
    repository = new KnexMemberRepository(knex, NullLog);
    runQueryOriginal = repository.runQuery;
    withFieldsSpy = jest.spyOn(NullLog, 'withFields');
    verboseSpy = jest.spyOn(NullLog, 'verbose');
    runQuerySpy = jest.spyOn(repository, 'runQuery');
  });

  afterEach(() => {
    repository.runQuery = runQueryOriginal;
    mockDb.unmock(knex);
  });

  it('gets a member by id', async () => {
    repository.runQuery = jest.fn().mockResolvedValueOnce(pgMember);
    runQuerySpy = jest.spyOn(repository, 'runQuery');
    const query = knex('members')
      .select(columns)
      .where({ id: '0' })
      .first();

    const result = await repository.getMemberByMemberId('0');

    expect(withFieldsSpy).toHaveBeenCalledWith({ memberId: '0' });
    expect(verboseSpy).toHaveBeenCalledWith('Fetching user by member id.');
    expect(runQuerySpy).toHaveBeenCalledWith(query);
    expect(result).toMatchObject(Member.fromJson(pgMember.body));
  });

  it('returns null if no member is found', async () => {
    repository.runQuery = jest.fn().mockResolvedValueOnce(null);

    const result = await repository.getMemberByMemberId('0');

    expect(result).toBeNull();
  });

  it('adds a member', async () => {
    const member = Member.fromInitial(
      pgMember.id,
      pgMember.body.displayName,
      pgMember.body.email,
      pgMember.body.retreatIds[0]
    );
    const query = knex('members').insert(pgMember);

    await repository.addMember(member);

    expect(withFieldsSpy).toHaveBeenCalledWith({ member: member.toJSON() });
    expect(verboseSpy).toHaveBeenCalledWith('Adding member.');
    expect(runQuerySpy).toHaveBeenCalledWith(query);
  });

  it('updates a member', async () => {
    const member = Member.fromInitial(
      pgMember.id,
      pgMember.body.displayName,
      pgMember.body.email,
      pgMember.body.retreatIds[0]
    );
    const query = knex('members')
      .where({ id: pgMember.id })
      .update(pgMember);

    await repository.updateMember(member);

    expect(withFieldsSpy).toHaveBeenCalledWith({ member: member.toJSON() });
    expect(verboseSpy).toHaveBeenCalledWith('Updating member.');
    expect(runQuerySpy).toHaveBeenCalledWith(query);
  });
});
