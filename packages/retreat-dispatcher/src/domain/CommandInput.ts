export type CommandInput = {
  action: string;
  key?: string;
  data: Object;
};
