import { gql } from 'apollo-server-express';

/**
 * Represents the GraphQL Root query that we use.
 * @type string
 */
const RootQuery = gql`
  type RootQuery {
    dummy: String
  }
`;

export default [RootQuery];
