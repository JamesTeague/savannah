import { gql } from 'apollo-server-express';
import JSON from './JSON';

/**
 * Represents the GraphQL command resource type.
 * @type string
 */
const Command = gql`
  type Command {
    id: String!
    timestamp: String!
    action: String!
    data: JSON
  }
`;

export default [Command, ...JSON];
