import { gql } from 'apollo-server-express';
import JSON from './JSON';

/**
 * Represents the GraphQL input for executing a command.
 *
 * @type string
 */
const CommandInput = gql`
  input CommandInput {
    action: String!
    data: JSON!
  }
`;

export default [CommandInput, ...JSON];
