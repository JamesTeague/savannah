import { gql } from 'apollo-server-express';
import RootMutation from './RootMutation';
import RootQuery from './RootQuery';

const Schema = gql`
  schema {
    mutation: RootMutation
    query: RootQuery
  }
`;

export default [Schema, ...RootMutation, ...RootQuery];
