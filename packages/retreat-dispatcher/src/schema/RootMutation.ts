import { gql } from 'apollo-server-express';
import Command from './Command';
import CommandInput from './CommandInput';

/**
 * Represents the GraphQL Root mutation that we use.
 * @type string
 */
const RootMutation = gql`
  type RootMutation {
    executeCommand(input: CommandInput): Command!
  }
`;

export default [RootMutation, ...CommandInput, ...Command];
