import { gql } from 'apollo-server-express';

/**
 * Represents the GraphQL JSON type.
 *
 * @type string
 */
const JSON = gql`
  scalar JSON
`;

export default [JSON];
