import { IOTypes } from '@penguinhouse/savannah-io';
import { ForbiddenError } from 'apollo-server-errors';
import Bluebird from 'bluebird';
import { ApolloServerContext } from '../Application';
import { CommandInput } from '../domain';

const executeCommand = <T>(
  _: Object,
  { input }: { input: CommandInput },
  context: ApolloServerContext
): Promise<IOTypes.Command<T>> =>
  Bluebird.try(() => {
    const { commandExecutionService, user, logger } = context;

    const { action, data } = input;

    const entry = logger.withFields({ userId: user.id, action, data });
    entry.info('Command execution requested.');

    if (!user.can('create:command')) {
      const error = new ForbiddenError('User does not have rights to execute commands.');

      entry.withError(error).error('Command execution failed due to access rights.');

      throw error;
    }

    return commandExecutionService
      .executeCommand(user, action, data)
      .then(command => command.toJSON());
  }).catch((error: Error) => {
    const { logger } = context;

    logger.withError(error).error('Error occurred in resolver.');

    throw error;
  });

export default executeCommand;
