import GraphQLJSON from 'graphql-type-json';
import { IResolvers } from 'graphql-tools';
import executeCommand from './ExecuteCommand';

const resolvers: IResolvers = {
  JSON: GraphQLJSON,
  RootMutation: {
    executeCommand,
  },
};

export default resolvers;
