import stoolie from 'stoolie';
import { User, SavannahPlatform } from '@penguinhouse/retreat-es';
import { ApolloServer } from 'apollo-server-express';
import cors from 'cors';
import http from 'http';
import express from 'express';
import * as Rx from 'rxjs';
import * as RxOp from 'rxjs/operators';
import { ApolloServerContext } from './Application';
import ConfigureApplication from './ApplicationConfiguration';
import typeDefs from './schema';
import { CommandExecutionService } from './service';
import resolvers from './resolvers';

const config = ConfigureApplication(process.env);
const logger = stoolie('retreat-dispatcher', config.logLevel);

const savannah = new SavannahPlatform({
  appName: 'retreat-dispatcher',
  ...config.db,
});

const app = express()
  .use(express.json())
  .use(cors());
const server = http.createServer(app);

const commandExecutionService = new CommandExecutionService(savannah, logger);

const context = ({ req, connection }): ApolloServerContext => {
  if (connection) {
    logger.info('Chose existing context.');
    return connection.context;
  }

  logger.info('Creating new context.');

  // TODO - Revisit this when we actually get users from Auth0
  const { user } = req;
  if (!user) {
    logger.warn(
      'No credentials found. Creating dummy user. This behavior will soon be deprecated.'
    );
    req.user = User.dummy();
    // throw new Error('No credentials found.');
  }

  return {
    user: req.user,
    logger,
    commandExecutionService,
  };
};

const apollo = new ApolloServer({
  resolvers,
  typeDefs,
  context,
});

apollo.applyMiddleware({ app });

const exit = new Rx.Subject();
['SIGINT', 'SIGTERM', 'SIGHUP'].forEach((signal: any) => {
  process.on(signal, () => exit.next(true));
});

const cleanup = () => {
  return savannah.disconnect().then(() => {
    server.close();
    logger.info('Platform disconnected and server closed.');
  });
};

const main = (): Rx.Observable<any> => {
  savannah.connectNotifier().then(success => {
    if (!success) {
      throw new Error('Failed to connect to platform.');
    }

    server.listen(config.api.port, config.api.address);

    logger.info('Server is listening.');
  });

  return Rx.NEVER;
};

Rx.of([])
  .pipe(
    RxOp.mergeMap(() => main()),
    RxOp.takeUntil(exit)
  )
  .subscribe({
    next() {},
    error(error: Error) {
      cleanup().then(() => {
        logger.withError(error).error('Fatal application error.');
      });
    },
    complete() {
      cleanup().then(() => {
        logger.info('Application exited normally.');
      });
    },
  });
