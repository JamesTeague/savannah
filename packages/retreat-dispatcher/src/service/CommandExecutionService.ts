import { Command, User, ISavannahPlatform } from '@penguinhouse/retreat-es';
import { ILogger } from 'stoolie';
import moment from 'moment';
import uuid from 'uuid/v4';
import { IExecuteCommands } from './interfaces';

export default class CommandExecutionService implements IExecuteCommands {
  savannah: ISavannahPlatform;
  entry: ILogger;

  constructor(savannah: ISavannahPlatform, logger: ILogger) {
    this.savannah = savannah;
    this.entry = logger.withCategory('command-execution-service');
    this.entry.debug('Configured command execution service.');
  }

  executeCommand<T>(user: User, action: string, data: T): Promise<Command<T>> {
    const id = uuid();
    const timestamp = moment().utc();
    const command = new Command({
      id,
      action,
      timestamp,
      data,
      user,
    });

    return this.savannah.notifyCommand(command).then(() => command);
  }
}
