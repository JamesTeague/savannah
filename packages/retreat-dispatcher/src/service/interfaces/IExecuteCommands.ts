import { User, Command } from '@penguinhouse/retreat-es';

export interface IExecuteCommands {
  executeCommand<T>(user: User, action: string, data: T): Promise<Command<T>>;
}
