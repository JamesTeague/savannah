import { LogLevel } from 'stoolie';

type DbConfig = {
  host: string;
  port: number;
  database: string;
  user: string;
  password: string;
};

type JwtConfig = {
  jwksUri: string;
  audience: string;
};

type RbacConfig = {
  authNamespace: string;
};

type ApiConfig = {
  port: number;
  address: string;
  jwtOptions: JwtConfig;
  rbacOptions: RbacConfig;
};

export type AppConfig = {
  logLevel: LogLevel;
  db: DbConfig;
  api: ApiConfig;
};

export default (env: { [key: string]: string | null | typeof undefined }): AppConfig => ({
  logLevel: (env.LOG_LEVEL as LogLevel) || LogLevel.DEBUG,
  db: {
    host: env.PG_DB_HOST || 'localhost',
    port: Number(env.PG_DB_PORT) || 5432,
    database: env.PG_DB_NAME || 'retreat',
    user: env.PG_USER_NAME || 'postgres',
    password: env.PG_PASSWORD || 'postgres',
  },
  api: {
    port: Number(env.API_PORT) || 4000,
    address: env.API_ADDRESS || '0.0.0.0',
    jwtOptions: {
      jwksUri: env.API_JWKS_URI || 'https://anna-development.auth0.com/.well-known/jwks.json',
      audience: env.API_JWT_AUD || 'https://api.dev.postacute.io',
    },
    rbacOptions: {
      authNamespace: env.API_RBAC_NS || 'https://postacute.io/claims',
    },
  },
});
