import { User } from '@penguinhouse/retreat-es';
import { ILogger } from 'stoolie';
import { IExecuteCommands } from './service';

export type ApolloServerContext = {
  commandExecutionService: IExecuteCommands;
  logger: ILogger;
  user: User;
};
