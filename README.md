# Project Savannah
Project for planning trips with a group that needs to do a little bit of research of the city, before suggesting a place, and ultimately voting on a destination.
## Getting Started
### Prerequisites

---

[*Node.js*](https://nodejs.org/)
- [nvm v0.34+](https://github.com/creationix/nvm#install-script)
    - `curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash`
- [node v12.x](https://nodejs.org/en/)
  - `nvm install 12`

[*PostgreSQL 10*](https://www.postgresql.org/download/)
 - create a user named postgres
 - set postgres password to `postgres`
    - you can set it to something else but then you must update your env variables
 - create a database named `retreat`
 - create a database named `retreat_api`
 - start postgres

[*Docker*](https://www.docker.com/products/docker-desktop)
 - start Docker

### Building the app

---
- Install project dependencies
    - `npm install`
- Install all dependencies and bootstrap packages
    - `npm run bootstrap`
- Compile and build packages
    - `npm run build`

### Starting the app

---
- Start Docker Containers
    - `docker-compose up`

- Start UI
    - `cd packages/retreat-ui && npm start`

### Using the app

---

Navigate to http://localhost:3000 for the user interface.
